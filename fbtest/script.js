$(document).ready(function() {
	
	window.user; // Informacion del usuario
	var ajaxResult;
	var firstResponse = true;
	
	/***** Conexion con Facebook *****/	
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
		// Init App
		FB.init({ 
			appId : '1377547122361431',
			cookie : true,
			version: 'v2.10'
		});  
		// Get login status   
		FB.getLoginStatus(function(response) { 
			
		});
	});
	
	/***** Accion al boton login *****/
	$(".container").on( "click", "#btn_login", function() {
		login();
	});


	/***** Abrir ventana de login o aceptacion de permisos ******/
	function login(){
		FB.login(function(response) {
			if (response.authResponse) { //Logueado correctamente
				shareFacebook();
			} else { // Login cancelado, no dio permisos				
				$(".container #btn_login").fadeIn();
			}
		},{scope: 'email,publish_actions'});
	}
	
	/***** Accion al compartir en Facebook *****/
	function shareFacebook(){
		FB.api('/me/?fields=id,name,email', function(response){
			window.user = response.name;						
		    if (!response || response.error) {
		        alert("Ha ocurrido un error. Por favor intenta más tarde.")
		    } else {
		    	var imgURL = "http://www.mesaparacamaleones.com/wp-content/uploads/2016/09/Ver%C3%B3nica-Orozco-parte-del-elenco-actoral-de-mesa-para-camaleones-serie-web-colombiana-de-humor.jpg";
				FB.api('/photos', 'post', {
				    url:imgURL,
				    message: window.user+" se parece a Veronica. Descubre tu a quien te pareces entrando a http://www.lightsbytena.co/fbtest/"
				}, function(response){
				    if (!response || response.error) {
				        alert("Ha ocurrido un error subiendo tu foto a Facebook, por favor descargala y subela manualmente.")
				    } else {
				    	alert("Compartido Correctamente.")
				    }
				});
		    }
		});		
	};	
});