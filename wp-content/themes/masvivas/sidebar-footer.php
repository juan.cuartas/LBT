<?php
/**
 * The Footer widget areas
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
?>
	  <?php if ( is_active_sidebar( 'first-footer-widget-area' ) || is_active_sidebar( 'second-footer-widget-area' ) || is_active_sidebar( 'third-footer-widget-area' ) ) : ?>
      <div class="container">
        

        

          <div class="ax-footer-logo">

          <div class="row">
          
            <div class="col-sm-12">
                <h2>Más vivas que nunca</h2>
            </div>

            <div class="ax-footer">
            
              <div class="col-md-3">
                  <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
              </div>

              <div class="col-md-7">
                  <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
              </div>
            
              <div class="col-md-2">
                  <div class="ax-iconosredes">
                        <a target="_blank" href="https://www.facebook.com/masvivasquenunca"><i class="fa fa-facebook" aria-hidden="true"></i> </a>
                        <a class="ax-inst" href="https://www.instagram.com/masvivas" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a target="_blank" href="https://twitter.com/masvivas"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                  </div>
              </div>

            </div>
          
             </div><!-- /.row -->
          </div>
       
      
	  </div><!-- /.containereste -->
      <?php endif ; ?>
	  