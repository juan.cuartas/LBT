<?php

include_once('../../../wp-config.php' );
if ( 
	isset( $_POST['my_image_upload_nonce'], $_POST['post_id'] ) 
	&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' )
	&& current_user_can( 'edit_post', $_POST['post_id'] )
) {
	// The nonce was valid and the user has the capabilities, it is safe to continue.

	// These files need to be included as dependencies when on the front end.
	require_once('../../../wp-admin/includes/image.php' );
	require_once('../../../wp-admin/includes/file.php' );
	require_once( '../../../wp-admin/includes/media.php' );

	
	// Let WordPress handle the upload.
	// Remember, 'my_image_upload' is the name of our file input in our form above.

	$attachment_id = media_handle_upload( 'my_image_upload', 9999999 );
	
	if ( is_wp_error( $attachment_id ) ) {

		echo 'Se ha producido un error al subir la imagen.';

		
			


		
	} else {


			    $args = array(
			        'post_type' => 'attachment',
			        'post_mime_type' =>'image',
			        'post_status' => 'inherit',
			        'posts_per_page' => -1,
			    );
			    $query_images = new WP_Query( $args );
			    $images = array();
			    foreach ( $query_images->posts as $image) {
			        $images[]= $image->guid;
			    }
			  echo  print_r($images);
			  echo json_encode($images);
		echo 'La imagen se ha subido correctamente';
	}

} else {
echo json_encode($_POST);
	 echo 'El control de seguridad falló, tal vez mostrar al usuario un error.';

}