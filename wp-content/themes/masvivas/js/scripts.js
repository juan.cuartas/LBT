$(function ($) {
  $(document).ready(function () {
    $('#searchsubmit, #commentform #submit').addClass('btn btn-default');
	$('button, html input[type="button"], input[type="reset"], input[type="submit"]').addClass('btn btn-default');
	$('input:not(button, html input[type="button"], input[type="reset"], input[type="submit"]), input[type="file"], select, textarea').addClass('form-control');
	if ($('label').parent().not('div')) {
	  $('label:not(#searchform label,#commentform label)').wrap('<div></div>');
	}
    $('table').addClass('table table-bordered');
    $('.attachment-thumbnail').addClass('thumbnail');
    $('embed-responsive-item,iframe,embed,object,video').parent().addClass('embed-responsive embed-responsive-16by9');
	$('.navbar-nav').addClass('blog-nav');
	$('.dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:focus').closest('.navbar-nav').removeClass('blog-nav');
  
 $("#holder").jPages({
    containerID : "ax-retos",
     perPage : 4,
     first:false,
     last:false,
     previous:false,
     next:false
  });

$("#holder-videos").jPages({
    containerID : "ax-vid",
     perPage : 4,
     first:false,
     last:false,
     previous:false,
     next:false
  });


$("#frm_solicitar_muestra").submit(function(e) {

    var url = "/wp-content/themes/masvivas/solicitar_envio.php"; // the script where you handle the form input.
    var data = {
    	action:'consultar_muestras',
    	data:$("#frm_solicitar_muestra").serialize()
    };
    $.ajax({
           type: "POST",
           dataType:"json",
           url: url,
           data: $("#frm_solicitar_muestra").serialize(), // serializes the form's elements.
           success: function(data)
           {

            console.log(data);
           	if(data=='')
           	{
           		 $("#frm_solicitar_muestra").remove();

		        $("#mensaje_muestra").fadeIn(3000);

		       
		        $("#mensaje_muestra").fadeOut("slow");
           	}
	           

          
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});



$("#frm_reto").submit(function(e) {

    var url = "/wp-content/themes/masvivas/solicitar_envio.php"; // the script where you handle the form input.
    var data = {
      action:'consultar_muestras',
      data:$("#frm_reto").serialize()
    };
    $.ajax({
           type: "POST",
           dataType:"json",
           url: url,
           data: $("#frm_reto").serialize(), // serializes the form's elements.
           success: function(datos)
           {

            console.log(datos);
            if(datos==true)
            {
               // $("#frm_reto").remove();

            $("#mensaje_reto").fadeIn(3000);

           
            $("#mensaje_reto").fadeOut("slow");
            }

           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});



$("#frm_invitado").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var url = "/wp-content/themes/masvivas/solicitar_envio.php"; // the script where you handle the form input.
    var data = {
      action:'consultar_muestras',
      data:$("#frm_invitado").serialize()
    };
        
    
    //var validate='';
/*    var validate = function(){        
        jQuery.each(jQuery('#frm_invitado').serializeArray(), function(i, field) {
            var input = jQuery('input[name='+field.name+']').data('validate');
            field.value = jQuery.trim(field.value);
            if(field.value){
                validate[i] = validatesForm(input,field.value);

                //if(!validate) return;
            }else{                
                validate[i] = {status:false,message:'Campos requeridos'}
            }
        });
        return validate;
    }*/    
    var validate = validatesForm('email',$("#email_invitado").val());
    
    if($("#email_invitado").val() != '' && validate.status){
        validate = validatesForm('text',$("#nombre_invitado").val());
        if(($("#nombre_invitado").val() != '' && $("#nombre_invitado").val() != '') && validate.status){
            $.ajax({
                type: "POST",
                dataType:"json",
                url: url,
                data: $("#frm_invitado").serialize(), // serializes the form's elements.
                beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                },
                success: function(data) {
                    $(".ax-back-opa").css("display","none");
                    $("#enviar-invitado").prop('disabled', true);
                    $("#agregar-otro").prop('disabled', false);

                    if(data==true) {
                        $("#frm_invitado").trigger('reset');
                        showMessage($("#mensaje_exitoso_invitado"),'Has invitado a tus amigas exitosamente');
                        setTimeout(function(){
                            $("#mensaje_exitoso_invitado").fadeOut('slow');    
                        },7000);
                        
                    } else if (data.sesion) {
                        showMessage($("#mensaje_exitoso_invitado"),'Por favor inicia sesión.');
                    } else {
                        showMessage($("#mensaje_exitoso_invitado"),'No se pudo enviar el correo.');
                    }
                }
             });
        }else{
            showMessage($("#mensaje_exitoso_invitado"),validate.message);
        }    
    }else{
        showMessage($("#mensaje_exitoso_invitado"),validate.message);
    }    
});



// $('#frm_invitado input').on('keypress', function(){

//   console.log("estoy copiando el form");

// });

$('#agregar-otro').on('click', function(){
    $("#mensaje_exitoso_invitado").fadeOut('slow');
     $("#nombre_invitado").val('');

    $("#apellido_invitado").val('');

     $("#email_invitado").val('');

  $("#enviar-invitado").prop('disabled', false);

   $("#agregar-otro").prop('disabled', true);

});

//TEXT CARACTERS
var text_max = 140;
$('#count_message').html(text_max);

$('#descripcion_reto').keyup(function() {
  var text_length = $('#descripcion_reto').val().length;
  var text_remaining = text_max - text_length;
  
  $('#count_message').html(text_remaining);
});
//END TEXT CARACTERS

 // $("#my_image_upload").change(function(){
 //        $(".ax-paso-1 .ax-aproved-imagen").fadeIn("slow");
 //    });
      
// $( "#descripcion_reto" ).keyup(function() {
//     if($( "#descripcion_reto" ).val()=="")
//         {
//             $(".ax-paso-2 .ax-aproved-imagen").fadeOut("slow");
//         }else {
//             $(".ax-paso-2 .ax-aproved-imagen").delay(2000).fadeIn("slow");
//         }
      

//     });    
    
});  

function bloquear_inputs(){

           // bloquear inputs
            $("#my_image_upload").prop('disabled', true);
            $("#descripcion_reto").prop('disabled', true);
            $("#submit_my_image_upload").prop('disabled', true);

            $("#my_image_upload").val("");
            $("#descripcion_reto").val("");
      }
$("#featured_upload").submit(function(e) {

    var url = "/wp-content/themes/masvivas/solicitar_envio.php"; 
    var data = new FormData();
    data.append('my_image_upload', $("#my_image_upload")[0].files[0]);   

    // data.append('post_id',$("#post_id").val());

    data.append('_wp_http_referer',$( "input[name*='_wp_http_referer']" ).val());

    data.append('my_image_upload_nonce',$("#my_image_upload_nonce").val());

    data.append('descripcion_reto',$("#descripcion_reto").val());

    
   
    $.ajax({
        type: 'POST',
        url:url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                },
        success: function(res){

          $('body').animate({scrollTop:$('.ax-paso-3').offset().top},1000);

        $(".ax-back-opa").css("display", "none");
         

          if (res==1) {
            $(".ax-paso-2 .ax-aproved-imagen").fadeIn("slow");
            bloquear_inputs();
          }


          if (res==2) {
             $(".ax-paso-1 .ax-aproved-imagen").fadeIn("slow");
             bloquear_inputs();
          }

          if (res==3) {
             $(".ax-paso-1 .ax-aproved-imagen").fadeIn("slow");
             $(".ax-paso-2 .ax-aproved-imagen").fadeIn("slow");
             bloquear_inputs();
          }

        }
    });


    e.preventDefault(); // avoid to execute the actual submit of the form.
});


$('#finalizar').on('click', function(){

   
            $("#mensaje_reto").fadeIn("slow");
});


  });


function showMessage(input,message){
    input.text(message);
    input.fadeIn();
}

function validatesForm(type,value){
    var re ='',message = '';
    switch(type) {
        case 'text':
            re = /^[a-zA-Z]+$/;
            message = 'Campo nombre o apellido incorrecto';
            break;
        case 'email':
            re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            message = 'Campo email incorrecto';
            break;
    }
    
    return {
        status:re.test(value),
        message:message
    }
}