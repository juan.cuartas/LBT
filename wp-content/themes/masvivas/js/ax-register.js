ax = {
    validateInfo: function() {
        $('.registration').bind('click', function() {
            $("#pixels").empty();
            $("#pixels2").empty();
            $("#pixels").append('<!-- Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home_BotonRegistrate URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 08/17/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixels2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te002;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+'+a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script><noscript> <iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te002;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe> </noscript><!-- End of DoubleClick Floodlight Tag: Please do not remove -->');
            
            $('.ax-form-rp').empty();
            $('.ax-bgfooter').empty();
            if($(window).width() >= 768){
                $('.ax-bgfooter').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificación"><button id="regcontinuar" type="button" onclick="ax.validateUser()" class="btn btn-default ax-button">Continuar</button></form><div id="axmsgregister" class="messagelogin"></div></div></div>').animate({opacity: 1},500);
                }else {
                    $('.ax-form-rp').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificación"><button id="regcontinuar" type="button" onclick="ax.validateUser()" class="btn btn-default ax-button">Continuar</button></form><div id="axmsgregister" class="messagelogin"></div></div></div>').animate({opacity: 1},500);
                }
        });
        $('.userLogin').bind('click', function() {
            $('.ax-form-rp').empty();
            $('.ax-bgfooter').empty();
            if($(window).width() >= 768){
                $('.ax-bgfooter').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificación"><button id="regcontinuar" type="button" onclick="ax.ingresar()" class="btn btn-default ax-button">Continuar</button><button id="actualizar" type="button" onclick="ax.updateUserData()" class="btn btn-default ax-button">Actualizar datos</button></form><div id="axmsgregister" class="messagelogin"></div></div></div>').animate({opacity: 1},500);
                }else {
                    $('.ax-form-rp').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificación"><button id="regcontinuar" type="button" onclick="ax.ingresar()" class="btn btn-default ax-button">Continuar</button><button id="actualizar" type="button" onclick="ax.validateUser()" class="btn btn-default ax-button">Actualizar datos</button></form><div id="axmsgregister" class="messagelogin"></div></div></div>').animate({opacity: 1},500);
                }
        });
    },
    ingresar: function (comuni = false) {
        var numerodocumento = $('#reg_numdocumento').val();
        var correo = $('#reg_email').val();
        var mesg = '';
        $('#axmsgregister').empty();
        
        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo válido</br>';
        }
        
        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificación</br>';
        } 
        
        if (mesg != '') {
            $('#axmsgregister').empty();
            $('#axmsgregister').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento};
            
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: 
                {
                    ingresaruser: data
                },
                beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    $('#axmessager').empty();
                    $('#reg_email').val();
                    $('#reg_numdocumento').val();
                    $("#msghome").empty();
                    $("#redirecthome").empty();
                      
                    if (jsonData.resno != null) {
                        
                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            $("#msghome").append(jsonData.msg);
                            $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                            $("#mdlhome").modal("show");
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');     
                        }    
                    } else if (jsonData.redirect != '') {
                        if (comuni) {
                            window.location.reload();
                        } else {
                            window.location.href = jsonData.redirect;   
                        }
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    console.log(jsonData);
                }
            });
        }
    },
    validateUser: function () {
        var numerodocumento = $('#reg_numdocumento').val();
        var correo = $('#reg_email').val();
        var mesg = '';
        $('#axmsgregister').empty();
        
        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo válido</br>';
        }
        
        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificación</br>';
        } 
        
        if (mesg != '') {
            $('#axmessager').empty();
            $('#axmessager').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento};
            
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: 
                {
                    validate: data
                },
                beforeSend: function () {
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData)
                {
                    $('#axmessager').empty();
                    $('#axmsgregister').empty();
                    $('#reg_email').val();
                    $('#reg_numdocumento').val();
                    $("#msghome").empty();
                    $("#redirecthome").empty();
                    
                    if (jsonData.resno != null) {
                        
                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            $("#msghome").append(jsonData.msg);
                            $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                            $("#mdlhome").modal("show");
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');     
                        }    
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                    } 
                },
                error: function(jsonData)
                {
                    console.log(jsonData);
                }
            });
        }
    },
    updateUserData: function () {
        var numerodocumento = $('#reg_numdocumento').val();
        var correo = $('#reg_email').val();
        var mesg = '';
        $('#axmsgregister').empty();
        
        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo válido</br>';
        }
        
        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificación</br>';
        } 
        
        if (mesg != '') {
            $('#axmsgregister').empty();
            $('#axmsgregister').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento, home: true};
            
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: 
                {
                    updatedatauser: data
                },
                beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    $('#axmessager').empty();
                    $('#reg_email').val();
                    $('#reg_numdocumento').val();
                    $("#msghome").empty();
                    $("#redirecthome").empty();
                      
                    if (jsonData.resno != null) {
                        
                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            $("#msghome").append(jsonData.msg);
                            $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                            $("#mdlhome").modal("show");
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');     
                        }    
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                    } 
                },
                error: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    console.log(jsonData);
                }
            });
        }
    },
    validateEmail: function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);   
    },
    getTypesDocument: function() {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                tipodoc: 1
            },
            beforeSend: function () {
                $('#reg_tipo_documento').empty();
                $('#reg_tipo_documento').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData)
            {
                $('#reg_tipo_documento').empty();
                $('#reg_tipo_documento').removeAttr('disabled');
                $('#reg_tipo_documento').append('<option value="0" selected="selected">Tipo de documento</option>');
                
                if (jsonData.length == 0) {
                    $('#reg_tipo_documento').empty();
                    $('#reg_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
                }
                
                $.each(jsonData,function() {
                    $('#reg_tipo_documento').append('<option value="'+this.id+'">'+this.documento+'</option>');
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#reg_tipo_documento').empty();
                $('#reg_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
            }
        });
    },
    getTypesDocumentUpd: function() {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                tipodoc: 1
            },
            beforeSend: function () {
                $('#upd_tipo_documento').empty();
                $('#upd_tipo_documento').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData)
            {
                $('#upd_tipo_documento').empty();
                $('#upd_tipo_documento').removeAttr('disabled');
                $('#upd_tipo_documento').append('<option value="0" selected="selected">Tipo de documento</option>');
                
                if (jsonData.length == 0) {
                    $('#upd_tipo_documento').empty();
                    $('#upd_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
                }
                
                $.each(jsonData,function() {
                    $('#upd_tipo_documento').append('<option value="'+this.id+'">'+this.documento+'</option>');
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#upd_tipo_documento').empty();
                $('#upd_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
            }
        });
    },
    getDepartments: function(id) {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                idcountry: id
            },
            beforeSend: function () {
                $('#reg_departamento').empty();
                $('#reg_departamento').attr('disabled','disabled');
                $('#reg_departamento').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData)
            {
                $('#reg_departamento').empty();
                $('#reg_departamento').removeAttr('disabled');
                $('#reg_ciudad').empty();
                $('#reg_ciudad').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                $('#reg_departamento').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                
                if (jsonData.length == 0) {
                    $('#reg_departamento').empty();
                    $('#reg_departamento').append('<option value="0" selected="selected">Selecciona tu pais</option>');
                }
                
                $.each(jsonData,function() {
                    $('#reg_departamento').append('<option value="'+this.id+'">'+this.departamento+'</option>');
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#reg_departamento').empty();
                $('#reg_departamento').append('<option value="-1" selected="selected">No hay departamentos disponibles</option>');
            }
        });
    },
    getDepartmentsUpd: function(id) {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                idcountry: id
            },
            beforeSend: function () {
                $('#upd_departamento').empty();
                $('#upd_departamento').attr('disabled','disabled');
                $('#upd_departamento').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData)
            {
                $('#upd_departamento').empty();
                $('#upd_departamento').removeAttr('disabled');
                $('#upd_ciudad').empty();
                $('#upd_ciudad').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                $('#upd_departamento').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                
                if (jsonData.length == 0) {
                    $('#upd_departamento').empty();
                    $('#upd_departamento').append('<option value="0" selected="selected">Selecciona tu pais</option>');
                }
                
                $.each(jsonData,function() {
                    $('#upd_departamento').append('<option value="'+this.id+'">'+this.departamento+'</option>');
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#upd_departamento').empty();
                $('#upd_departamento').append('<option value="-1" selected="selected">No hay departamentos disponibles</option>');
            }
        }); 
    },
    getCities: function(id) {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                iddepart: id
            },
            beforeSend: function () {
                $('#reg_ciudad').empty();
                $('#reg_ciudad').attr('disabled','disabled');
                $('#reg_ciudad').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData)
            {
                $('#reg_ciudad').empty();
                $('#reg_ciudad').removeAttr('disabled');
                $('#reg_ciudad').append('<option value="0" selected="selected">Selecciona tu ciudad</option>');
                
                if (jsonData.length == 0) {
                    $('#reg_ciudad').empty();
                    $('#reg_ciudad').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                }
                
                $.each(jsonData,function() {
                    $('#reg_ciudad').append('<option value="'+this.id+'">'+this.ciudad+'</option>');
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#reg_ciudad').empty();
                $('#reg_ciudad').append('<option value="-1" selected="selected">No hay ciudades disponibles</option>');
            }
        });
    },
    getCitiesUpd: function(id) {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                iddepart: id
            },
            beforeSend: function () {
                $('#upd_ciudad').empty();
                $('#upd_ciudad').attr('disabled','disabled');
                $('#upd_ciudad').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData)
            {
                $('#upd_ciudad').empty();
                $('#upd_ciudad').removeAttr('disabled');
                $('#upd_ciudad').append('<option value="0" selected="selected">Selecciona tu ciudad</option>');
                
                if (jsonData.length == 0) {
                    $('#upd_ciudad').empty();
                    $('#upd_ciudad').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                }
                
                $.each(jsonData,function() {
                    $('#upd_ciudad').append('<option value="'+this.id+'">'+this.ciudad+'</option>');
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#upd_ciudad').empty();
                $('#upd_ciudad').append('<option value="-1" selected="selected">No hay ciudades disponibles</option>');
            }
        });
    },
    getInterest: function() {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                intereses: 'intereses'
            },
            beforeSend: function () {
                $('#int_group').empty();
                $('#int_group').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
            },
            success: function(jsonData)
            {
                $('#int_group').empty();
                
                $.each(jsonData,function() {
                    $('#int_group').append('<div class="checkbox"><label class="custom-control custom-checkbox"><input type="checkbox" class="ax-checkbox ax-temas custom-control-input" value="'+this.name+'"><span class="custom-control-indicator"></span>'+this.name+'</label>');
                });
                
                $('#int_group input').bind('click', function() {
                    if (this.value == 'Otros' && $(this).is(':checked')) {
                        $('#o-temas').append('<input id="txtOtema" type="text" class="ax-input""><button id="btn-ag-tema" type="button">Agregar</button>');
                        
                        $('#btn-ag-tema').bind('click', function () {
                            if ($('#txtOtema').val() != '') {
                                if ($('.nw-tema')) {
                                    var exist = false;
                                    $('.nw-tema strong').each(function() {
                                        if ($(this).text() == ($('#txtOtema').val())) {
                                            exist = true;
                                        }
                                    });
                                    if (!exist) {
                                        $('#o-temas').append('<div class="nw-tema"><strong>'+$('#txtOtema').val()+'</strong><span class="bt-del-tema">X</span></div>');
                                        $('#txtOtema').val('');
                                        $('.bt-del-tema').click(function () {
                                            $(this).parent().remove();
                                        });  
                                    }
                                } else {
                                    $('#o-temas').append('<div class="nw-tema"><strong>'+$('#txtOtema').val()+'</strong><span class="bt-del-tema">X</span></div>');
                                    $('.bt-del-tema').click(function () {
                                        $(this).parent().remove();
                                    });
                                }
                            }
                        });
                    } else if (this.value == 'Otros' && !$(this).is(':checked')) {
                        $('#o-temas').empty();
                    }
                });
            },
            error: function(jsonData)
            {
                console.log(jsonData);
                $('#int_group').empty();
                $('#int_group').append('<p>No hay temas disponibles</p>');
            }
        });
    },
    register: function() {
        
        $("#pixels").empty();
        $("#pixels2").empty();
        $("#pixels").append('<!-- Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Formulario_BotonEnviar URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/registrate/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 08/17/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixels2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te000;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+'+a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script><noscript> <iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te000;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript><!-- End of DoubleClick Floodlight Tag: Please do not remove -->');
        
        var nombre = $('#reg_nombre').val();
        var segnombre = $('#reg_segnombre').val();
        var apellido = $('#reg_apellido').val();
        var segapellido = $('#reg_segapellido').val();
        var tipodocumento = $('#reg_tipo_documento').val();
        var numerodocumento = $('#reg_num_documento').val();
        var dia = $('#reg_dia_nac').val();
        var mes = $('#reg_mes_nac').val();
        var ano = $('#reg_ano_nac').val();
        var genero = $('#reg_sexo').val();
        var pais = $('#reg_pais').val();
        var departamento = $('#reg_departamento').val();
        var ciudad = $('#reg_ciudad').val();
        var direccion = $('#reg_direccion').val();
        var correo = $('#reg_email_register').val();
        var celular = $('#reg_celular').val();
        var barrio = $('#reg_barrio').val();
        
        var selectedtema = false;
        $('.ax-checkbox.ax-temas').each(function() {
          if (this.checked) {
             selectedtema = true;
          }
        });
        
        var urlvid = '';
        if ($("#reg-submit-teq").length > 0) {
            urlvid = $("#reg_url_vid").val();
        }
        
        var contactoresidencia = $('#reg_med_fis:checked').val();
        var contactoemail = $('#reg_med_vir:checked').val();
        var terminos = $('#reg_terms:checked').val();
        var politica = $('#reg_policy:checked').val();
        var expectativa = $('#reg_expectativa').val();
        var mesg = '';

        if (nombre.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Nombre</div>';
        } 
        if (apellido.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Apellido</div>';
        } 
        if (tipodocumento == 0) {
            mesg += '<div class="ax-registro-user">Ingresa Tipo de documento</div>';
        } 
        if (numerodocumento.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Número de documento</div>';
        } 
        if (dia == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Día de nacimiento</div>';
        } 
        if (mes == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Mes de nacimiento</div>';
        } 
        if (ano == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Año de nacimiento</div>';
        } 
        if (genero == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Género</div>';
        } 
        if (pais == '0') {
            mesg += '<div class="ax-registro-user">Ingresa País</div>';
        } 
        if (departamento == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Departamento</div>';
        } 
        if (ciudad == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Ciudad</div>';
        } 
        if (direccion.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Dirección</div>';
        } 
        if (!ax.validateEmail(correo.trim())) {
            mesg += '<div class="ax-registro-user">Ingresa un correo electrónico válido</div>';
        } 
        
        if (celular.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Celular</div>';
        }
        
        if (!selectedtema) {
            mesg += '<div class="ax-registro-user">Por favor selecciona al menos 1 interés</div>';
        } 
        
        if (!terminos) {
            mesg += '<div class="ax-registro-user">Por favor acepta los Términos y condiciones</div>';
        } 
        if (!politica) {
            mesg += '<div class="ax-registro-user">Por favor acepta la Política de privacidad</div>';
        }
        
        if (mesg != '') {
            $('#axmessage').empty();
            $('#axmessage').append(mesg);
            window.scrollTo(0, 250);
        } else {
            var fechanacimiento = ano+'-'+mes+'-'+dia;
            var temas = '';
            var contemas = 0;
            var contacresi = 'S';
            var contavirt = 'S';
            var terms = 'N';
            var policy = 'N';
            var nameciudad = $('#reg_ciudad option[value='+$('#reg_ciudad').val()+']').text();
            var namedepar = $('#reg_departamento option[value='+$('#reg_departamento').val()+']').text();
            var namepais = $('#reg_pais option[value='+$('#reg_pais').val()+']').text();
            $('.ax-checkbox.ax-temas').each(function() {
              if (this.checked) {
                  if (contemas == 0) {
                    temas += this.value;
                  } else {
                    temas += ','+this.value;   
                  }
                  contemas++;
              }
            });
            
            if (contactoresidencia) {
                contacresi = 'N';
            }
            
            if (contactoemail) {
                contavirt = 'N';
            }
            
            if (terminos) {
                terms = 'S';
            }
            
            if (politica) {
                policy = 'S';
            }
            var data = null;
            
            if ($("#reg-submit-teq").length > 0) {
                data = {
                        datauserteq: {
                            nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                            tipodocumento: tipodocumento, numdocumento: numerodocumento, anon: ano, mesn: mes, dian: dia, genero: genero,
                            pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio, correo: correo, 
                            celular: celular, temas: temas,urlvid: urlvid, contactoresidencia: contacresi,
                            contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa                   
                       }
                };
            } else {
                data = {
                        datauser: {
                            nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                            tipodocumento: tipodocumento, numdocumento: numerodocumento, anon: ano, mesn: mes, dian: dia, genero: genero,
                            pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio,
                            correo: correo, celular: celular, temas: temas, contactoresidencia: contacresi,
                            contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa                   
                       }
                };
            }
            
            $("#msgregistro").empty();
            $("#redirectregistro").empty();
            
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: data,
                beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                },
                success: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    
                    if (jsonData.redirect) {
                        if (jsonData.res == 'errordb') {
                            $("#msgregistro").append("Error al registrar el usuario, por favor inténtelo más tarde.");
                        } else {
                            $("#msgregistro").append(jsonData.msg);
                        }
                        
                        $("#redirectregistro").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                        $("#mdlregistro").modal("show");
                    } else {
                        if (jsonData.res != '') {
                            if (jsonData.res == 'errordb') {
                                $("#msgregistro").append("Error al registrar el usuario, por favor inténtelo más tarde.");
                            } else {
                                $("#msgregistro").append(jsonData.msg);
                            }
                            $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            $("#mdlregistro").modal("show");
                        } else { 
                            console.log(jsonData);
                        }   
                    }
                },
                error: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    console.log(jsonData);
                }
            });
        }
    },
    getUserInfo: function(udata) {
        $(".ax-back-opa").css("display", "block");
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: 
            {
                getuserinfo: udata
            },
            beforeSend: function () {
                $(".ax-back-opa").css("display", "block");
            },
            success: function(jsonData)
            {
                $('#ax-preload').empty();
                
                if (jsonData[0].correo != '') {
                    
                   window.setTimeout(function() {
                       $(".ax-back-opa").css("display", "none");
                       
                        $('#upd_nombre').val(jsonData[0].nombre1);
                        $('#upd_segnombre').val(jsonData[0].nombre2);
                        $('#upd_apellido').val(jsonData[0].apellido1);
                        $('#upd_segapellido').val(jsonData[0].apellido2);
                        $('#upd_tipo_documento option[value="'+jsonData[0].tipoidentificacion+'"]').attr("selected", "selected");
                        $('#upd_num_documento').val(jsonData[0].identificacion);
                        $('#upd_num_documento').attr("disabled", "disabled");
                        if (jsonData[0].fechanacimiento != '') {
                            $('#upd_dia_nac option[value="'+jsonData[0].fechanacimiento.substr(6, 2)+'"]').attr("selected","seleted");
                            $('#upd_mes_nac option[value="'+jsonData[0].fechanacimiento.substr(4, 2)+'"]').attr("selected","seleted");
                            $('#upd_ano_nac option[value="'+jsonData[0].fechanacimiento.substr(0, 4)+'"]').attr("selected","seleted");   
                        }
                        $('#upd_sexo option[value="'+jsonData[0].sexo+'"]').attr("selected", "selected");
                       
                        $('#upd_pais option').each(function() {
                            $(this).removeAttr("selected");
                            if (this.value == jsonData[0].idpais) {
                                $(this).attr("selected", "selected");
                            }
                        });
                       
                        $('#upd_departamento').empty();
                        $('#upd_departamento').append('<option value="0" selected="selected">Selecciona tu departamento</option>');
                       
                        for (i=0; i < jsonData.departamentos.length; i++) {
                            $('#upd_departamento').append('<option value="'+jsonData.departamentos[i].id+'">'+jsonData.departamentos[i].departamento+'</option>');
                        }
                       
                        $('#upd_departamento option').each(function() {
                            $(this).removeAttr("selected");
                            if (this.value == jsonData[0].iddepartamento) {
                                $(this).attr("selected", "selected");
                            }
                        });
                       
                        $('#upd_ciudad').empty();
                        $('#upd_ciudad').append('<option value="0" selected="selected">Selecciona tu ciudad</option>');
                       
                        for (i=0; i < jsonData.ciudades.length; i++) {
                            $('#upd_ciudad').append('<option value="'+jsonData.ciudades[i].id+'">'+jsonData.ciudades[i].ciudad+'</option>');
                        }
                       
                        $('#upd_ciudad option').each(function() {
                            $(this).removeAttr("selected");
                            if (this.value == jsonData[0].idciudad) {
                                $(this).attr("selected", "selected");
                            }
                        });
                       
                        $('#upd_direccion').val(jsonData[0].direccion);
                        $('#upd_email').val(jsonData[0].correo);
                        $('#upd_celular').val(jsonData[0].celular);
                        $('#upd_barrio').val(jsonData[0].barrio);

                       if (jsonData.temasinteres) {
                            var temasusr = jsonData.temasinteres;
                            $('#int_group input[type="checkbox"]').each(function() {
                                for (i = 0; i <= temasusr.length; i++) {
                                    if (this.value == temasusr[i]) {
                                        $(this).attr("checked", "checked");
                                    }
                                }
                            });
                        }
                       
                       if (jsonData[0].contactoemail == 'S') {
                            $('#upd_med_vir').attr("checked", "checked");   
                        }

                        if (jsonData[0].contactoresidencia == 'S') {
                            $('#upd_med_fis').attr("checked", "checked");   
                        }
                       
                        if (jsonData[0].terminosYcondiciones == 'S') {
                            $('#upd_terms').attr("checked", "checked");   
                        }

                        if (jsonData[0].politicaPrivacidad == 'S') {
                            $('#upd_policy').attr("checked", "checked");   
                        }
                   },1000);
                }
            },
            error: function(jsonData)
            {
                console.log(jsonData);
            }
        });   
    },
    updateUserInfo: function() {
        var nombre = $('#upd_nombre').val();
        var segnombre = $('#upd_segnombre').val();
        var apellido = $('#upd_apellido').val();
        var segapellido = $('#upd_segapellido').val();
        var tipodocumento = $('#upd_tipo_documento').val();
        var numerodocumento = $('#upd_num_documento').val();
        var dia = $('#upd_dia_nac').val();
        var mes = $('#upd_mes_nac').val();
        var ano = $('#upd_ano_nac').val();
        var genero = $('#upd_sexo').val();
        var pais = $('#upd_pais').val();
        var departamento = $('#upd_departamento').val();
        var ciudad = $('#upd_ciudad').val();
        var direccion = $('#upd_direccion').val();
        var correo = $('#upd_email').val();
        var celular = $('#upd_celular').val();
        var barrio = $('#upd_barrio').val();
        
        var selectedtema = false;
        $('.ax-checkbox.ax-temas').each(function() {
          if (this.checked) {
             selectedtema = true;
          }
        });
        
        var urlvid = '';
        if ($("#reg-submit-teq").length > 0) {
            urlvid = $("#upd_url_vid").val();
        }
        
        var contactoresidencia = $('#upd_med_fis:checked').val();
        var contactoemail = $('#upd_med_vir:checked').val();
        var terminos = $('#upd_terms:checked').val();
        var politica = $('#upd_policy:checked').val();
        var expectativa = $('#upd_expectativa').val();
        var mesg = '';
        
        if (nombre.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Nombre</br>';
        } 
        if (apellido.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Apellido</br>';
        } 
        if (tipodocumento == 0) {
            mesg += '<div class="ax-registro-user">Ingresa Tipo de documento</br>';
        } 
        if (numerodocumento.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Número de documento</br>';
        } 
        if (dia == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Día de nacimiento</br>';
        } 
        if (mes == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Mes de nacimiento</br>';
        } 
        if (ano == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Año de nacimiento</br>';
        } 
        if (genero == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Género</br>';
        } 
        if (pais == '0') {
            mesg += '<div class="ax-registro-user">Ingresa País</br>';
        } 
        if (departamento == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Departamento</br>';
        } 
        if (ciudad == '0') {
            mesg += '<div class="ax-registro-user">Ingresa Ciudad</br>';
        } 
        if (direccion.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Dirección</br>';
        } 
        if (!ax.validateEmail(correo.trim())) {
            mesg += '<div class="ax-registro-user">Ingresa un correo electrónico válido</br>';
        } 
        
        if (celular.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa Celular</div>';
        }
        
        if (!selectedtema) {
            mesg += '<div class="ax-registro-user">Por favor selecciona al menos 1 interés</br>';
        } 
        
        if ($("#reg-submit-teq").length > 0) {
            if (urlvid.trim() == '') {
                mesg += '<div class="ax-registro-user">Ingrese URL del video</br>';
            }
        }
        
        if (!terminos) {
            mesg += '<div class="ax-registro-user">Por favor acepta los Términos y condiciones</br>';
        } 
        if (!politica) {
            mesg += '<div class="ax-registro-user">Por favor acepta la Política de privacidad</br>';
        }
        
        if (mesg != '') {
            $('#axmessage').empty();
            $('#axmessage').append(mesg);
            window.scrollTo(0, 250);
        } else {
            var fechanacimiento = ano+'-'+mes+'-'+dia;
            var temas = '';
            var contemas = 0;
            var contacresi = 'S';
            var contavirt = 'S';
            var terms = 'N';
            var policy = 'N';
            var nameciudad = $('#upd_ciudad option[value='+$('#upd_ciudad').val()+']').text();
            var namedepar = $('#upd_departamento option[value='+$('#upd_departamento').val()+']').text();
            var namepais = $('#upd_pais option[value='+$('#upd_pais').val()+']').text();
            $('.ax-checkbox.ax-temas').each(function() {
              if (this.checked) {
                  if (contemas == 0) {
                    temas += this.value;
                  } else {
                    temas += ','+this.value;   
                  }
                  contemas++;
              }
            });
            
            if (contactoresidencia) {
                contacresi = 'N';
            }
            
            if (contactoemail) {
                contavirt = 'N';
            }
            
            if (terminos) {
                terms = 'S';
            }
            
            if (politica) {
                policy = 'S';
            }
            var data = null;
            
            if ($("#reg-submit-teq").length > 0) {
                data = {
                        datausertequpd: {
                            nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                            tipodocumento: tipodocumento, numdocumento: numerodocumento, anon: ano, mesn: mes, dian: dia, genero: genero,
                            pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio, correo: correo, 
                            celular: celular, temas: temas,urlvid: urlvid, contactoresidencia: contacresi,
                            contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa                   
                       }
                };
            } else {
                data = {
                        datauserupd: {
                            nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                            tipodocumento: tipodocumento, numdocumento: numerodocumento, anon: ano, mesn: mes, dian: dia, genero: genero,
                            pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio,
                            correo: correo, celular: celular, temas: temas, contactoresidencia: contacresi,
                            contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa                   
                       }
                };
            }
            
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: data,
                beforeSend: function () {
                    $('#ax-preload').empty();
                    $('#ax-preload').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData)
                {
                    if (jsonData.res != '') {
                        $('#ax-preload').empty();
                        $('#ax-preload').append('<p>'+jsonData.msg+'</p>');
                    } else {
                        if (jsonData.redirect != '') {
                            window.location.href = jsonData.redirect;
                        } else {
                            if (jsonData.msg.length > 0) {
                                $('#ax-preload').append('<p>'+jsonData.msg+'</p>');
                            } else { 
                                console.log(jsonData);
                            }   
                        }   
                    }
                },
                error: function(jsonData)
                {
                    console.log(jsonData);
                }
            });
        }
    },
    getComments: function () {
        $.ajax({
            type: 'POST',
            dataType: "json",
            url:templateDir+'/inc/soap/ax-soap.php',
            data: {allcomments:{idpost: idcurrentpost}},
            beforeSend: function () {
                $(".ax-back-opa").css("display", "block");
            },
            success: function(jsonData)
            {
                $(".ax-back-opa").css("display", "none");
                if (jsonData.html && jsonData.html != '') {
                    $('#ax-all-comments').empty();
                    $('#ax-all-comments').append(jsonData.html);   
                } else {
                    $('#ax-all-comments').append('<div class="ax-aviso"><p>Aún no se han publicado comentarios.<br>¡Sé el primero en hacerlo!</p></div>'); 
                }
            },
            error: function(jsonData)
            {
                $(".ax-back-opa").css("display", "none");
                console.log(jsonData);
            }
        });
    },
    submitComment: function(textbox,idcomment){
        var content = $(textbox).val();
        var page_type = $('#page_type').val();
        var parent = $(textbox).attr('parentid');
        
        // var filename = $("#my_image_upload").val();
        if (content.trim() != '') {
            
            var data = new FormData();
            data.append('my_image_upload_comment_p', $("#my_image_upload_comment_p")[0].files[0]);  

              data.append('content',content);
              data.append('page_type',page_type); 
              data.append('parent',parent); 
              data.append('inputcomment', textbox);
              data.append('pagecomment', window.location.href);

            $.ajax({
                type: 'POST',
                url:templateDir+'/inc/soap/ax-soap.php',
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                },
                success: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    $("#my_image_upload_comment_p").val("");

                    var obj = JSON.parse(jsonData);
                    if (obj.msg != '') {
                        if (obj.msg == 'sesion') {
                            $('#msgparcomment'+idcomment).empty();
                            $('#msgparcomment'+idcomment).append('<p>Por favor <a href="javascript:void(0)" onclick="$('+"'"+'#mdllogin'+"'"+').modal('+"'"+'show'+"'"+');"><strong>inicia sesión</strong></a> o <a href="'+obj.pageregister+'"><strong>regístrate</strong></a> para comentar</p>');    
                        } else {
                            $('#msgparcomment'+idcomment).empty();
                            $('#msgparcomment'+idcomment).append('<p>'+obj.msg+'</p>');  
                            ax.getComments();
                        }
                        /*window.setTimeout(function() {
                            $('#msgparcomment'+idcomment).empty();
                        },5000);*/
                    } else {
                        console.log(obj);  
                    }
                },
                error: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    console.log(obj);
                }
            });
        } else {
            $(".ax-back-opa").css("display", "none");
            $('#msgparcomment'+idcomment).empty();
            $('#msgparcomment'+idcomment).append('<p>Por favor ingrese comentario.</p>');
        }
    },
    submitSubComment: function(textbox,imgu,idcomment){
        var content = $(textbox).val();
        var page_type = $('#page_type').val();
        var parent = $(textbox).attr('parentid');
        
        // var filename = $("#my_image_upload").val();

        if (content.trim() != '') {
            var data = new FormData();
            data.append('my_image_upload_comment', imgu[0].files[0]);  

            data.append('content',content);
            data.append('page_type',page_type); 
            data.append('parent',parent);
            data.append('inputcomment', textbox);
            data.append('pagecomment', window.location.href);

            $.ajax({
                type: 'POST',
                url:templateDir+'/inc/soap/ax-soap.php',
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $(".ax-back-opa").css("display", "block");
                },
                success: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    var obj = JSON.parse(jsonData);
                    
                    if (obj.msg != '') {
                        
                        if (obj.msg == 'sesion') {
                            $('#msgsubcomment'+idcomment).empty();
                            $('#msgsubcomment'+idcomment).append('<p>Por favor <a href="javascript:void(0)" onclick="$('+"'"+'#mdllogin'+"'"+').modal('+"'"+'show'+"'"+');"><strong>inicia sesión</strong></a> o <a href="'+obj.pageregister+'"><strong>regístrate</strong></a> para comentar</p>'); 
                        } else {
                            $('#msgsubcomment'+idcomment).empty();
                            $('#msgsubcomment'+idcomment).append('<p>'+obj.msg+'</p>');   
                            ax.getComments();
                        }
                    } else {
                        console.log(jsonData);  
                    }
                },
                error: function(jsonData)
                {
                    $(".ax-back-opa").css("display", "none");
                    console.log(jsonData);
                }
            });
        } else {
            $(".ax-back-opa").css("display", "none");
            $('#msgsubcomment'+idcomment).empty();
            $('#msgsubcomment'+idcomment).append('<p>Por favor ingrese comentario.</p>');
        }
    }, shareFacebook: function (titulo, comentario, imagen) {
        data = {};
        data.method = 'share';
        data.title = titulo;
        data.quote = siteurl;
        data.description = comentario;
        data.href = window.location.href;
        
        if (imagen != '') {
            data.picture = imagen;
        } else {
            data.picture = 'http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/11093539/bg-share-facebook.jpg';
        }
        
        FB.ui(data, function(response){});
    }
}