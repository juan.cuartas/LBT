<?php



/*
Template Name: Plantilla reto
*/session_start();
if (empty($_SESSION['idusuario'])) {
    wp_redirect(home_url());
}

if (isset($_SESSION['referer'])) {

    $qusuario = $wpdb->get_var( "SELECT id_usuario FROM wp_users_tena WHERE clave_usuario = '".$_SESSION['referer']."'" );
    $consulta_referido = $wpdb->get_var("SELECT id_referido FROM wp_referidos WHERE id_usuario_referido='".$_SESSION['idusuario']."'");
    
    if (empty($consulta_referido)) {
        
        $datos = array('id_usuario' => $qusuario, 
                       'id_usuario_referido' => $_SESSION['idusuario'], 
                       'clave_usuario_referido' => $_SESSION['userdata']['clave_usuario'] 
                       );
        
		$wpdb->insert('wp_referidos', $datos);
    }
}

$consulta_u_reto = "SELECT * FROM wp_retos WHERE id_usuario=".$_SESSION['idusuario']." ";
$resultado_u_reto = $wpdb->get_results($consulta_u_reto);

if (empty ($resultado_u_reto)) {
  $disab='';

}else{
   wp_redirect(get_site_url().'/comunidad');
   $disab='disabled';
}
  

  get_header(); ?>
 

<div class="ax-bg-t">
   <div class="ax-cont-create-reto">
        <div class="container">
            <div class="col-sm-12 blog-main">
                <div class="formulario-reto">
                      <form id="featured_upload" method="post"  enctype="multipart/form-data">
                          <div class="ax-paso-1">
                           <div class="row">
                                <div class="col-sm-5">

                                    <hgroup>
                                         <h2>Paso 1</h2>
                                         <h3>Sube tu imagen</h3>
                                     </hgroup>
                                     <p>Solo debes subir una imagen o copiar un texto donde expreses por qué estás Más viva que nunca.
</p>
                                      <input type="file" name="my_image_upload" id="my_image_upload"  multiple="false"  <?php echo $disab;?> />
                              <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
                                    <div class="ax-aproved-imagen" style="display: none"><p><img src="/wp-content/themes/masvivas/img/ax-icon-exito.png" alt="">Tu imagen se ha cargado exitosamente.</p></div>
                                </div>
                                 <div class="col-sm-7">
                                      <div class="ax-image-capture">
                                          <img src="/wp-content/themes/masvivas/img/sube-imagen.png" alt="">
                                      </div>
                                  </div>
                                  <div class="ax-flechas"></div>
                              </div>
                          </div>
                          <div class="ax-paso-2">
                              <hgroup>
                                  <h3>Cuéntanos por qué estás Más viva que nunca</h3>
                              </hgroup>
                              <div class="ax-cont-textaera">
                                  <textarea id="descripcion_reto" name="descripcion_reto" maxlength="140" <?php echo $disab;?> ></textarea>
                                  <h6 class="pull-right" id="count_message"></h6>
                              </div>
                              <input type='hidden' <?php echo $disab;?> value='<?php echo $_SESSION['idusuario']?>' id='id_usuario_reto' name='id_usuario_reto'>


                              <input <?php echo $disab;?> type="submit" id="submit_my_image_upload" class="submit_my_image_upload" name="submit_my_image_upload"  value="Continuar" />

                              <div class="ax-aproved-imagen" style="display: none"><p><img src="/wp-content/themes/masvivas/img/ax-icon-exito.png" alt="">El campo de texto ha sido completado exitosamente.</p></div>

                         </div>
                      </form>
                       
                  </div>

                     
                  <div class="ax-paso-3">
                    <div class="row">
                        <div class="col-sm-6">
                             <hgroup>
                                 <h2>Paso 2</h2>
                                 <h3>Invita a tus amigas</h3>
                             </hgroup>
                              <p>Comparte con otras mujeres como tú, por qué estas Más viva que nunca.
                              Ten presente que entre más amigas invites y se registren para que participen, más posiciones subirás en el Top 35+ y así tendrás más oportunidades de ganar una entrada para que disfrutes del evento que hemos preparado para ti.</p>
                              <div class="formulario-invitar-amigos">
                                <form id='frm_invitado' method='post' novalidate>
                                <label>Nombre</label>
                                <input type="text" id="nombre_invitado" data-validate="text" name="nombre_invitado" placeholder="Ingresa el nombre">

                                <label>Apellido</label>
                                <input type="text" id="apellido_invitado" data-validate="text" name="apellido_invitado" placeholder="Ingresa el apellido">

                                 <label>Correo</label>
                                <input type="email" id="email_invitado"  data-validate="email" name="email_invitado" placeholder="Ingresa el correo">

                                <input type="button" id="agregar-otro" name="agregar-otro" value="Agregar otra amiga" disabled class="ax-button">

                                <input type="submit" id="enviar-invitado" name="enviar-invitado" value="Invitar" class="ax-button">

                                <div class="axfinreto"><input type="button" id="finalizar" name="finalizar" value="Finalizar reto" class="ax-button"></div>
                                </form>
                                <div id="mensaje_exitoso_invitado" style="display:none;">Has invitado a tus amigas exitosamente</div>
                              </div>
                              <div class="ax-paso-finish">

                                    
                                  <div id="mensaje_reto" style="display:none;"><h3>¡Felicidades, has completado tu reto!</h3>
                                     <p>Gracias por contarnos por qué estás Más viva que nunca, muy pronto verás tu publicación en <a href="/mujeres-con-experiencias/">Mujeres con experiencias</a>, no te pierdas un solo detalle.</p></div>
                                 </div>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="ax-cont-anuncio">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                       <a href="/comunidad"><div class="ax-banner-1">
                          <div class="cont-text">
                               <p>Descubre por qué muchas mujeres están más vivas que nunca en</p>
                                <span>Nuestra comunidad</span>
                           </div>
                           </div>
                       </a>
                        
                    </div>
                    <div class="col-md-6">
                        <a href="/top-35"><div class="ax-banner-2">
                          <div class="cont-text">
                               <p>Conoce tu posición en el Top 35+</p>
                                <span>Top 35+</span>
                           </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

  <div class="ax-back-opa">
    <div class="ax-preload-content">
        <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
    </div>
  </div>
<?php get_footer(); ?>