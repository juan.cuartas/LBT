<?php

/*
Template Name: Politicas TENA
*/

get_header();

?>

<div class="ax-terminos">
 	<div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
		<h2><?php the_title(); ?></h2>
		<div class="row">
	      	<div class="col-md-9">
	            <?php the_content(); ?>
	        </div>

        	<div class="col-md-3"></div>
	 	</div>
	 	<?php endwhile;?>
    <?php endif; ?>
	</div>
</div>
<?php
    get_footer();
?>

