<?php

/*
Template Name: Comunidad TENA
*/

get_header();

?>

<div class="ax-comunidad">

 	<div class="container">
 		<div class="row">
 			<div class="col-md-8">
				<h2><?php echo get_the_title(); ?></h2>
			    <?php echo get_post()->post_content; ?>
		    </div>
		    <div class="col-md-4"></div>
	 	</div>
	</div>

</div>

<div class="ax-comunidad-2">

 	<div class="container">
 		<div class="row">
 			<div class="col-md-6">
 				<img class="img-responsive center-block" alt="500x500" src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/05193919/Ejercicio2.jpg">
 			</div>
 			<div class="col-md-6">
 				<div class="item">
	 				<span>Hacer ejercicio en la casa</span>
					<h2>es salir de la rutina</h2>
				    <p>diaria, ejercítate en tu lugar preferido y cámbiale el ritmo a tu vida. Cuéntanos cuáles practicas tú.</p>
				    <a class="boton" href="/que-ejercicios-puedo-hacer-desde-mi-casa/" target="blank">Conoce más</a>
		    	</div>
		    </div>
	 	</div>
	</div>



</div>
<div class="ax-comunidad-3">

 	<div class="container">
 		<div class="row">
			

			<div class="col-md-6 visible-xs">
 				<img class="img-responsive center-block" alt="500x500" src="/wp-content/themes/masvivas/img/imagencomunidad.png">
 			</div>
 			<div class="col-md-6 visible-xs">
 				<div class="item">
	 				<span>Decían las abuelas</span>
					<h2>"los remedios caseros</h2>
				    <p>son los mejores”, compártenos tu remedio casero.</p>
				    <a class="boton" href="/decian-las-abuelas-los-remedios-caseros-son-los-mejores/" target="blank">Conoce más</a>
		    	</div>
		    </div>
 			



 			<div class="col-md-6 hidden-xs">
 				<div class="item">
	 				<span>Decían las abuelas</span>
					<h2>"los remedios caseros</h2>
				    <p>son los mejores”, compártenos tu remedio casero.</p>
				    <a class="boton" href="/decian-las-abuelas-los-remedios-caseros-son-los-mejores/" target="blank">Conoce más</a>
		    	</div>
		    </div>
 			<div class="col-md-6 hidden-xs">
 				<img class="img-responsive center-block" alt="500x500" src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/05193925/Remedios-caseros.jpg">
 			</div>
 			
	 	</div>
	</div>

</div>
<div class="ax-comunidad-4">

 	<div class="container">
 		<div class="row">
 			<div class="col-md-6">
 				<img class="img-responsive center-block" alt="500x500" src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/18150852/zona-intima2.jpg">
 			</div>
 			<div class="col-md-6">
 				<div class="item">
	 				<span>A veces sentimos molestias en nuestra zona íntima.</span>
					<h2>Mantener el pH equilibrado </h2>
				    <p>es sentirnos Más vivas que nunca. Compártenos tus secretos para lograrlo.</p>
				    <a class="boton" href="/por-que-mantener-el-ph-de-nuestra-zona-intima-equilibrado/" target="blank">Conoce más</a>
		    	</div>
		    </div>
	 	</div>
	</div>

</div>
<?php
    get_footer();
?>

