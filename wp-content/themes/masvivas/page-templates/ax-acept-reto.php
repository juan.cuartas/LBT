<?php

/*
Template Name: Plantilla aceptar reto
*/

session_start();

$consulta_u_reto = "SELECT * FROM wp_retos WHERE id_usuario=".$_SESSION['idusuario']." ";
$resultado_u_reto = $wpdb->get_results($consulta_u_reto);

if (!empty ($resultado_u_reto)) {
 wp_redirect(get_site_url().'/comunidad');
  
}

  get_header(); ?>
 
<div class="ax-bg-t">
        <div class="acept-reto">
            <div class="container">
               <div class="ax-info">
                    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <?php endwhile;?>
                    <?php endif; ?>
                </div>
                <div class="ax-video">
                
                <div id="player2"></div>
                 <!-- <video width="100%" autoplay>
                  <source src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/12152432/mas-vivas-que-nunca.mp4" type="video/mp4">
                  <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210201/alejandraogg.ogg" type="video/ogg">
                  <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210240/alejandrawebm.webm" type="video/webm">
                  Your browser does not support HTML5 video.
                </video>  -->
            </div>
            </div>
            <div class="ax-cont-interactive-reto">
                <div class="container">
                    <div class="ax-reto-interactive">
                        <h3>¿Aceptas el reto?</h3>
                        <span>Es muy sencillo.</span>
                        <div class="ax-btn-acept">
                            <a href="<?php echo get_site_url();?>/reto-parte-dos"><span class="ax-icon ax-icon-acept"><img src="<?php echo get_site_url();?>/wp-content/themes/masvivas/img/ax-icon-acept.png" alt=""/></span>Acepto el reto</a>
                            <a href="reto-en-otro-momento/"><span class="ax-icon ax-icon-cancel"><img src="/wp-content/themes/masvivas/img/ax-icon-close.png" alt=""/></span>En otro momento</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ax-cont-anuncio">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                       <a href="/comunidad"><div class="ax-banner-1">
                          <div class="cont-text">
                               <p>Descubre por qué muchas mujeres están más vivas que nunca en</p>
                                <span>Nuestra comunidad</span>
                           </div>
                           </div>
                       </a>
                        
                    </div>
                    <div class="col-md-6">
                        <a href="/top-35"><div class="ax-banner-2">
                          <div class="cont-text">
                               <p>Conoce tu posición en el Top 35+</p>
                                <span>Top 35+</span>
                           </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
</div>
      


<?php get_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/video2.js"></script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: CO_Tena_MasVivas_Retos
URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/reto/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 08/17/2016
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te001;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te001;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->