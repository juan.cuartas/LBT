<?php


/*
Template Name: Plantilla top 35
*/session_start();

$rankingmed = array();
$rankingbog = array();

$consulta_u_ranking = "SELECT distinct(r.id_usuario), (SELECT count(id_usuario) FROM wp_referidos where id_usuario = r.id_usuario) as count, ut.nombre,ut.apellido from wp_referidos r JOIN wp_users_tena ut ON r.id_usuario = ut.id_usuario
WHERE ut.ciudad = 'Medellin' order by count desc;";
$resultado_u_ranking = $wpdb->get_results( $consulta_u_ranking);
if (isset($resultado_u_ranking)) {
 $disab='disabled';
}else{
   $disab='';
}

$valor_referencia = $resultado_u_ranking[0]->count;

if ($valor_referencia > 0) {
    foreach ($resultado_u_ranking as $key => $value){
        $rankingmed[] = array($value->nombre . $value->apellido, intval($value->count * 100 / $valor_referencia));
    }   
}

$consulta_u_ranking = "SELECT distinct(r.id_usuario), (SELECT count(id_usuario) FROM wp_referidos where id_usuario = r.id_usuario) as count, ut.nombre,ut.apellido from wp_referidos r JOIN wp_users_tena ut ON r.id_usuario = ut.id_usuario
WHERE ut.ciudad = 'Bogota' order by count desc";
$resultado_u_ranking = $wpdb->get_results( $consulta_u_ranking);
if (isset($resultado_u_ranking)) {
 $disab='disabled';
}else{
   $disab='';
}

$valor_referencia = $resultado_u_ranking[0]->count;

if ($valor_referencia > 0) {
    foreach ($resultado_u_ranking as $key => $value){
        $rankingbog[] = array($value->nombre . $value->apellido, intval($value->count * 100 / $valor_referencia));
    }
}

get_header(); ?>

<div class="ax-bg-t">
    <div class="ax-cont-top-vivas">
        <div class="container">
            <div class="row">
                <div class="ax-cont-info">
                   <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                        <hgroup>
                           <h2><?php the_title(); ?></h2>
                        </hgroup>
                        <?php the_content(); ?>
                        <?php endwhile;?>
                    <?php endif; ?>
                    <div class="ax-cont-rainting">
                       
                       
                        <div id="tabs-top">
                            <ul class="nav nav-tabs">
                              <li><a href="#tab-medellin">Medellín</a></li>
                              <li><a href="#tab-bogota">Bogotá</a></li>
                            </ul>
                        <div id="tab-medellin" class="ax-cont-top">
                            <div id="medellin" class="tab-pane">

                            <?php
                            foreach($rankingmed as $item){
                                echo $html = '<div class="item clearfix">
                                    <div class="col-md-3"><p>'.$item[0].'</p></div>
                                    <div class="col-md-7">
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="'.$item[1].'"
                                          aria-valuemin="0" aria-valuemax="100" style="width:'.$item[1].'%">
                                            <span class="sr-only">'.$item[1].'% Complete</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-2"><p>'.$item[1].'%</p></div>
                                </div>';
                            }
                            
                            ?>
                            </div>
                        </div>
                        <div id="tab-bogota" class="ax-cont-top">
                          <div id="bogota" class="tab-pane">
                            <?php
                            foreach($rankingbog as $item){
                                echo $html = '<div class="item clearfix">
                                    <div class="col-md-3"><p>'.$item[0].'</p></div>
                                    <div class="col-md-7">
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="'.$item[1].'"
                                          aria-valuemin="0" aria-valuemax="100" style="width:'.$item[1].'%">
                                            <span class="sr-only">'.$item[1].'% Complete</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-2"><p>'.$item[1].'%</p></div>
                                </div>';
                            }
                            
                            ?>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>

            </div><!-- /.row -->
        </div><!-- /.containereste -->
        
        <div class="ax-cont-anuncio">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                       <a href="/mujeres-con-experiencias/"><div class="ax-banner-1">
                           <div class="cont-text">
                               <p>Inspírate, anímate a ingresar y conocer más</p>
                                <span>Mujeres con experiencias</span>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <a href="/comunidad/"><div class="ax-banner-2">
                           <div class="cont-text">
                                <p>Descubre por qué muchas mujeres están más vivas que nunca en</p>
                                <span>Nuestra comunidad</span>
                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('#tabs-top > div').hide();
        $('#tabs-top > div:first').show();
        $('#tabs-top > ul li:first').addClass('active');

        $('#tabs-top > ul li a').click(function(){
        $('#tabs-top > ul li').removeClass('active');
        $(this).parent().addClass('active');
        var currentTab = $(this).attr('href');
        $('#tabs-top > div').hide();
        $(currentTab).show();
        return false;
        });
    });
</script>
<?php get_footer(); ?>