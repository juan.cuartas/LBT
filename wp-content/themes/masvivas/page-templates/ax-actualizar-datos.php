<?php

/*
Template Name: Actualización de datos TENA
*/

session_start();

if (empty($_SESSION['idusuario'])) {
    wp_redirect(home_url());
} 

get_header();
global $wpdb;

$mesg = '';
$days = '';

$cont = 1;
for ($i=1; $i<=31; $i++) {
    if ($cont > 9) {
        $days .= '<option value="'.$i.'">'.$i.'</option>';   
    } else {
        $days .= '<option value="0'.$i.'">'.$i.'</option>';
    }
    $cont++;
}

$months = '';
$mont = array(1 => 'Enero',2 => 'Febrero',3 => 'Marzo',4 => 'Abril',
              5 => 'Mayo',6 => 'Junio',7 => 'Julio',8 => 'Agosto',
              9 => 'Septiembre',10 => 'Octubre',11 => 'Noviembre',12 => 'Diciembre');

for ($i=1; $i<=12; $i++) {
    $value = ($i<10) ? '0'.$i : $i;
    $months .= '<option value="'.$value.'">'.$mont[$i].'</option>';
}

$years = '';
for ($i=(int)date("Y"); $i>=1915; $i--) {
    $years .= '<option value="'.$i.'">'.$i.'</option>';
}

?>

<div class="ax-registro">
    <div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
      <div class="row">
        <div class="col-md-9">
          <p><?php the_content(); ?></p>
        </div>
        <div class="col-md-3"></div>
      </div>
      <?php endwhile;?>
    <?php endif; ?>
      
      <form id="form_register" name="form_register" name="registerform" method="post">

        <p id="axmessage" class="ax-message"></p>

        <h3>*Ingresa tus datos</h3>
        

        <div class="row ax-formulario">
            
            <div class="col-md-6">
             
                <div class="form-group">
                   <input id="upd_nombre" type="text" name="upd_nombre" class="ax-input" placeholder="Nombre" required>
                </div>
                
                <div class="form-group">
                    <input id="upd_segnombre" type="text" name="upd_segnombre"  placeholder="Segundo nombre"  class="ax-input">
                </div>
                
                <div class="form-group">
                   <input id="upd_apellido" type="text" name="upd_apellido" class="ax-input" placeholder="Apellido"  required>
                </div>

                <div class="form-group">
                   <input id="upd_segapellido" type="text" name="upd_segapellido" class="ax-input" placeholder="Segundo apellido" >
                </div>

                <div class="form-group">
                  <div class="row">
                      <div class="col-sm-4 col-xs-4">
                        <select name="upd_dia_nac" id="upd_dia_nac" class="ax-select" required>
                            <option value="0" selected="selected">Día</option>
                            <?php echo $days; ?>
                        </select>
                      </div>
                      <div class="col-sm-4 col-xs-4">
                        <select name="upd_mes_nac" id="upd_mes_nac" class="ax-select" required>
                            <option value="0" selected="selected">Mes</option>
                            <?php echo $months; ?>
                        </select>
                      </div>
                      <div class="col-sm-4 col-xs-4">
                        <select name="upd_ano_nac" id="upd_ano_nac" class="ax-select" required>
                            <option value="0" selected="selected">Año</option>
                            <?php echo $years; ?>
                        </select>
                      </div>
                  </div>
                </div>

                <div class="form-group">
                   <select name="upd_pais" id="upd_pais" onchange="ax.getDepartmentsUpd(this.value)" class="ax-select" required>
                       <option value="0" selected="selected">Selecciona tu país</option>
                       <option value="1">Colombia</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <select name="upd_departamento" id="upd_departamento" onchange="ax.getCitiesUpd(this.value)" class="ax-select" required>
                       <option value="0" selected="selected">Selecciona tu departamento</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <select name="upd_ciudad" id="upd_ciudad" class="ax-select" required>
                       <option value="0" selected="selected">Selecciona tu ciudad</option>
                   </select>
                </div>

          </div>
 

          <div class="col-md-6">

                <div class="form-group">
                   <input id="upd_direccion" type="text" class="ax-input" name="upd_direccion" placeholder="Dirección"  required>
                </div>
                
                <div class="form-group">
                   <select name="upd_tipo_documento" id="upd_tipo_documento" class="ax-select" required>
                       <option value="0" selected="selected">Tipo de documento</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <input id="upd_num_documento" type="number" name="upd_num_documento" class="ax-input" placeholder="Número de documento" disabled>
                </div>

                <div class="form-group">
                   <select name="upd_sexo" id="upd_sexo" class="ax-select" required>
                       <option value="0" selected="selected">Sexo</option>
                       <option value="1">Masculino</option>
                       <option value="2">Femenino</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <input id="upd_celular" type="number" name="upd_celular" placeholder="Celular"  class="ax-input">
                </div>
                
                <div class="form-group">
                   <input id="upd_barrio" type="text" name="upd_barrio" placeholder="Barrio"  class="ax-input">
                </div>
                
                <div class="form-group">
                   <input id="upd_email" type="email" name="upd_email" class="ax-input" placeholder="Correo electrónico"  required>
                </div>
          </div>
          
        </div>
        
        <div class="form-group ax-interes">
            <h3>Temas de interés</h3>
            <p>Para recibir información de tu interés, por favor selecciona al menos una de las siguientes actividades.</p>
            <div id="int_group" class="ax-check-group"></div>
        </div>
    </form>
</div>
</div>

<div class="ax-mediosmail">
  <div class="container">
    <form id="form_register" name="form_register" name="registerform" method="post">

        <h3>¿Por cuál medio deseas que te contactemos?</h3>

                <div class="form-group ">
          <div class="col-md-6">
            <div class="checkbox">
                 <label class="custom-control custom-checkbox">
                      <input id="upd_med_fis" type="checkbox" name="upd_med_fis" class="ax-checkbox custom-control-input" required>
                      <span class="custom-control-indicator"></span>
                      <p>Marca aquí si NO deseas que Más vivas te contacte por medios físicos con los datos que acabas de suministrar.</p>
                  </label>
            </div>
          </div>

          <div class="col-md-6">
            <div class="checkbox">
                 <label class="custom-control custom-checkbox">
                      <input id="upd_med_vir" type="checkbox" name="upd_med_vir" class="ax-checkbox custom-control-input" required>
                      <span class="custom-control-indicator"></span>
                      <p>Marca aquí si NO deseas que Más vivas te contacte por medios virtuales con los datos que acabas de suministrar.</p>
                </label>
            </div>
          </div>

        </div>

    </form>
  </div>
</div>

<div class="ax-terminoscond">
  <div class="container">
    <form id="form_register" name="form_register" name="registerform" method="post">

      <div class="form-group">

          <div class="col-md-6">
            <div class="checkbox">
                <label class="custom-control custom-checkbox">
                  <input id="upd_terms" type="checkbox" name="upd_terms" class="ax-checkbox custom-control-input" required>
                  <span class="custom-control-indicator"></span>
                  <a href="/terminos-y-condiciones" target="blank"><p>Acepto los términos y condiciones y el tratamiento de la información.</p></a>
                </label>
            </div>
          </div>

          <div class="col-md-6">
            <div class="checkbox">
                 <label class="custom-control custom-checkbox">
                      <input id="upd_policy" type="checkbox" name="upd_policy" class="ax-checkbox custom-control-input" required>
                      <span class="custom-control-indicator"></span>
                      <a href="politicas" target="blank"><p>Acepto la política de protección de información personal.</p></a>
                </label>
            </div>
          </div>

      </div>


      <div class="ax-campo">
           <p>*Campo obligatorio</p>
      </div>


        <input id="upd_expectativa" type="hidden" value="S" name="upd_expectativa">
        <div id="ax-preload"></div>
        <div class="form-group" style="text-align:center;">
           <input id="reg-submit" type="button" onclick="ax.updateUserInfo()" class="ax-button" value="Actualizar">
        </div>

        <div class="ax-back-opa">
            <div class="ax-preload-content">
                <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
            </div>
        </div>
    </form>
  </div>
</div>

<?php
    get_footer();
?>
<script type="text/javascript">
    var templateDir = "<?php bloginfo('template_directory'); ?>";
    
    $(document).ready(function() {
        ax.getInterest();
        ax.getTypesDocumentUpd();
    });

    var udata = {};
    <?php 
        if (!empty($_SESSION['idusuario'])) {
            echo 'udata.idusuario = "'.$_SESSION['idusuario'].'";';
            echo 'udata.correo = "'.$_SESSION['userdata']['correo'].'";';
            echo 'udata.numdocumento = "'.$_SESSION['userdata']['identificacion'].'";';
        } 
    ?>
    
    if (udata.idusuario != '') {
        $(document).ready(function() {
            ax.getUserInfo(udata);
        });
    }
</script>