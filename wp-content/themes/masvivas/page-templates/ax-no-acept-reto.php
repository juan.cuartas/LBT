<?php

/*
Template Name: Plantilla reto rechazado
*/

session_start();

$consulta_u_reto = "SELECT * FROM wp_retos WHERE id_usuario=".$_SESSION['idusuario']." ";
$resultado_u_reto = $wpdb->get_results($consulta_u_reto);

if (!empty ($resultado_u_reto)) {
 wp_redirect(get_site_url().'/comunidad');
  
}

  get_header(); ?>
 
<div class="ax-bg-t">
        <div class="acept-reto">
            <div class="container">
               <div class="ax-info">
                    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <?php endwhile;?>
                    <?php endif; ?>
                </div>
                <div class="ax-video">
                    <video width="100%" autoplay>
                      <source src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/12152432/mas-vivas-que-nunca.mp4" type="video/mp4">
                      <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210201/alejandraogg.ogg" type="video/ogg">
                      <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210240/alejandrawebm.webm" type="video/webm">
                      Your browser does not support HTML5 video.
                    </video> 
                </div>

            </div>
            <div class="ax-cont-interactive-reto">
                <div class="container">
                    <div class="ax-reto-interactive next">
                        <p>Te esperamos pronto, anímate y siéntete</p>
                        <h3>Más viva que nunca</h3>
                        <p>Recuerda que también puedes invitar a tus amigas a participar y hacer parte de nuestra comunidad.</p>
                        <div class="ax-btn-acept">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#mdlreto"><span class="ax-icon ax-icon-mail"><img src="/wp-content/themes/masvivas/img/ax-icon-mail.png" alt=""/></span>Invitar a una amiga</a>
                            <!--<a href="<?php echo get_site_url();?>/comunidad"><span class="ax-icon ax-icon-mail"><img src="/wp-content/themes/masvivas/img/ax-icon-mail.png" alt=""/></span>Invitar a una amiga</a>-->
                        </div>
                    </div>
                </div>
            </div>
                    <div class="ax-cont-anuncio">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <a href="/mujeres-con-experiencias/"><div class="ax-banner-1">
                           <div class="cont-text">
                               <p>Descubre por qué muchas mujeres están más vivas que nunca en</p>
                                <span>Nuestra comunidad</span>
                           </div>
                        </div></a>
                    </div>
                    <div class="col-md-6">
                    <a href="<?php echo get_site_url();?>/rating">
                        <a href="/top-35/"><div class="ax-banner-2">
                           <div class="cont-text">
                                <p>Conoce <br/> tu posición en el</p>
                                <span>Top 35+</span>
                            </div>

                        </div></a>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="modal fade" id="mdlreto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Invitar una amiga</h4>
                </div>
                <div class="modal-body">
                    <form id='frm_invitado' method='post' novalidate>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" id="nombre_invitado" class="ax-input" data-validate="text" name="nombre_invitado" placeholder="Ingresa el nombre">
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" id="apellido_invitado" class="ax-input" data-validate="text" name="apellido_invitado" placeholder="Ingresa el apellido">
                        </div>
                        <div class="form-group">
                             <label>Correo</label>
                            <input type="email" id="email_invitado" class="ax-input" data-validate="email" name="email_invitado" placeholder="Ingresa el correo">
                        </div>
                        <div class="form-group">
                            <div class="ax-botoninv">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="button" id="agregar-otro"  class="ax-button" name="agregar-otro" value="Agregar otra amiga" disabled>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="submit" id="enviar-invitado"  class="ax-button" name="enviar-invitado" value="Invitar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div id="mensaje_exitoso_invitado" style="display:none;">Has invitado a tus amigas exitosamente</div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo get_site_url();?>/comunidad" class="ax-button">Continuar a la comunidad</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="ax-back-opa">
        <div class="ax-preload-content">
            <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
        </div>
    </div>
</div>
      
<?php get_footer(); ?>