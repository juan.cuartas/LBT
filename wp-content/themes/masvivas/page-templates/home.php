<?php
/**
 * Template Name: Pagina inicio TENA
 */

session_start();

if (isset($_SESSION['correoreg'])) {
    unset($_SESSION['correoreg']);
}

if (isset($_GET['pss'])) {
    $_SESSION['referer'] = $_GET['pss'];
}

get_header(); 

?>
<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
    <div class="cont-ax-slide-text hidden-xs">
        <?php the_content(); ?>
    </div>
    <?php endwhile;?>
<?php endif; ?>
<div class="col-md-12 text-main cont-principal">
<div class="row">
    <video width="100%" preload="auto" muted loop id="videoHome" autoplay webkit-playsinline>
      <source src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/10160147/Lights-Sitio_1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
      <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/05172036/masvivas.ogg" type='video/ogg; codecs="theora, vorbis"'>
      <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/05172052/masvivas.webm" type='video/webm; codecs="vorbis,vp8"'>
      Your browser does not support HTML5 video.
    </video>
    <video id="vidphone" class="hidden" autoplay muted loop webkit-playsinline src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/10160147/Lights-Sitio_1.mp4"></video>
    <input id="btnPlayvideo" type="hidden" onclick="document.getElementById('videoHome').play()">
</div>
</div>
<div class="ax-cont-footer-home" id="ax-footer">
    <div class="ax-bgfooter"></div>
    <div class="blog-footer">
        <div class="container">
            <div class="row">
                <div class="ax-footer-logo">
                    <div class="col-sm-12">
                       <?php if(isset($_SESSION['idusuario'])) : ?>
                           <h2>Más vivas que nunca</h2>
                       <?php else : ?>
                            <form class="form-inline" role="form">
                              <div class="form-group">
                                <button type="button" class="userLogin form-control ax-button">Iniciar sesión</button>    
                              </div>
                              <div class="form-group ax-form-rp"></div>
                              <div class="form-group">
                                <button type="button" class="registration form-control ax-button">Regístrate</button>
                              </div>
                              
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="ax-footer">
                    <div class="col-sm-3">
                        <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
                    </div>
                    <div class="col-sm-6">
                        <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="ax-iconosredes">
                        <a target="_blank" href="https://www.facebook.com/masvivasquenunca"><i class="fa fa-facebook" aria-hidden="true"></i> </a>
                        <a class="ax-inst" href="https://www.instagram.com/masvivas" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a target="_blank" href="https://twitter.com/masvivas"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlhome" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="msghome" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <div id="redirecthome"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var templateDir = "<?php bloginfo('template_directory'); ?>";
var videos = document.querySelectorAll('video');

$(document).ready(function() {        
    ax.validateInfo();
    var videos = document.querySelectorAll('video');
    $('.ax-slide-text').cycle();

    if ($(window).width() <= 768) {
        var os = getMobileOperatingSystem();

        if (os == 'iOS') {
            $('#vidphone').removeClass('hidden');
            $('#videoHome').addClass('hidden');

            enableVideos();   
        } else {
            $('#vidphone').addClass('hidden');
            $('#videoHome').removeClass('hidden');

            $('body').click(function () {
                video = document.getElementById('videoHome');
                video.play();
            });   
        }
    } else {
        $('#vidphone').addClass('hidden');
        $('#videoHome').removeClass('hidden');
    }
});
    
function enableVideos(everywhere) {
    for (var i = 0; i < videos.length; i++) {
        window.makeVideoPlayableInline(videos[i], !videos[i].hasAttribute('muted'), !everywhere);
    }
}
    
function debugEvents(video) {
        [
            'loadstart',
            'progress',
            'suspend',
            'abort',
            'error',
            'emptied',
            'stalled',
            'loadedmetadata',
            'loadeddata',
            'canplay',
            'canplaythrough',
            'playing', // fake event
            'waiting',
            'seeking',
            'seeked',
            'ended',
        // 'durationchange',
            'timeupdate',
            'play', // fake event
            'pause', // fake event
        // 'ratechange',
        // 'resize',
        // 'volumechange',
            'webkitbeginfullscreen',
            'webkitendfullscreen',
        ].forEach(function (event) {
            video.addEventListener(event, function () {
                console.info('@', event);
            });
        });
    }
function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}
</script>

<div id="pixels"></div>
<div id="pixels2"></div>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: CO_Tena_MasVivas_Home
URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 08/17/2016
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
