﻿<?php

/*
Template Name: Formulario registro TENA
*/
session_start();

if (empty($_GET['pss']) && empty($_SESSION['correoreg'])) {
    wp_redirect(home_url());
} 

get_header();
global $wpdb;

$mesg = '';
$days = '';

$cont = 1;
for ($i=1; $i<=31; $i++) {
    if ($cont > 9) {
        $days .= '<option value="'.$i.'">'.$i.'</option>';   
    } else {
        $days .= '<option value="0'.$i.'">'.$i.'</option>';
    }
    $cont++;
}

$months = '';
$mont = array(1 => 'Enero',2 => 'Febrero',3 => 'Marzo',4 => 'Abril',
              5 => 'Mayo',6 => 'Junio',7 => 'Julio',8 => 'Agosto',
              9 => 'Septiembre',10 => 'Octubre',11 => 'Noviembre',12 => 'Diciembre');

for ($i=1; $i<=12; $i++) {
    $value = ($i<10) ? '0'.$i : $i;
    $months .= '<option value="'.$value.'">'.$mont[$i].'</option>';
}

$years = '';
for ($i=(int)date("Y"); $i>=1915; $i--) {
    $years .= '<option value="'.$i.'">'.$i.'</option>';
}

?>

<div class="ax-registro">
    <div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
      <div class="row">
        <div class="col-md-9">
          <p><?php the_content(); ?></p>
        </div>
        <div class="col-md-3"></div>
      </div>
      <?php endwhile;?>
    <?php endif; ?>
      
      <form id="form_register" name="form_register" name="registerform" method="post">

        <p id="axmessage" class="ax-message"></p>

        <h3>*Ingresa tus datos</h3>
        

        <div class="row ax-formulario">
            
            <div class="col-md-6">
             
                <div class="form-group">
                   <input id="reg_nombre" type="text" name="reg_nombre" class="ax-input" placeholder="Nombre" required>
                </div>
                
                <div class="form-group">
                    <input id="reg_segnombre" type="text" name="reg_segnombre"  placeholder="Segundo nombre"  class="ax-input">
                </div>
                
                <div class="form-group">
                   <input id="reg_apellido" type="text" name="reg_apellido" class="ax-input" placeholder="Apellido"  required>
                </div>

                <div class="form-group">
                   <input id="reg_segapellido" type="text" name="reg_segapellido" class="ax-input" placeholder="Segundo apellido" >
                </div>

                <div class="form-group">
                  <div class="row">
                      <div class="col-sm-4 col-xs-4">
                        <select name="reg_dia_nac" id="reg_dia_nac" class="ax-select" required>
                            <option value="0" selected="selected">Día</option>
                            <?php echo $days; ?>
                        </select>
                      </div>
                      <div class="col-sm-4 col-xs-4">
                        <select name="reg_mes_nac" id="reg_mes_nac" class="ax-select" required>
                            <option value="0" selected="selected">Mes</option>
                            <?php echo $months; ?>
                        </select>
                      </div>
                      <div class="col-sm-4 col-xs-4">
                        <select name="reg_ano_nac" id="reg_ano_nac" class="ax-select" required>
                            <option value="0" selected="selected">Año</option>
                            <?php echo $years; ?>
                        </select>
                      </div>
                  </div>
                </div>

                <div class="form-group">
                   <select name="reg_pais" id="reg_pais" onchange="ax.getDepartments(this.value)" class="ax-select" required>
                       <option value="0" selected="selected">Selecciona tu país</option>
                       <option value="1">Colombia</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <select name="reg_departamento" id="reg_departamento" onchange="ax.getCities(this.value)" class="ax-select" required>
                       <option value="0" selected="selected">Selecciona tu departamento</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <select name="reg_ciudad" id="reg_ciudad" class="ax-select" required>
                       <option value="0" selected="selected">Selecciona tu ciudad</option>
                   </select>
                </div>

          </div>
 

          <div class="col-md-6">

                <div class="form-group">
                   <input id="reg_direccion" type="text" class="ax-input" name="reg_direccion" placeholder="Dirección"  required>
                </div>
                
                <div class="form-group">
                   <select name="reg_tipo_documento" id="reg_tipo_documento" class="ax-select" required>
                       <option value="0" selected="selected">Tipo de documento</option>
                   </select>
                </div>
                
                <div class="form-group">
                    <?php if (isset($_GET['pss'])) {
                            echo '<input id="reg_num_documento" type="number" name="reg_num_documento" class="ax-input" placeholder="Número de documento" required>';
                        } else {
                            echo '<input id="reg_num_documento" type="number" name="reg_num_documento" class="ax-input" placeholder="Número de documento" value="'.$_SESSION['numdocreg'].'" required>';
                        }
                    ?>
                </div>

                <div class="form-group">
                   <select name="reg_sexo" id="reg_sexo" class="ax-select" required>
                       <option value="0" selected="selected">Sexo</option>
                       <option value="1">Masculino</option>
                       <option value="2">Femenino</option>
                   </select>
                </div>
                
                <div class="form-group">
                   <input id="reg_celular" type="number" name="reg_celular" placeholder="Celular"  class="ax-input">
                </div>
                
                <div class="form-group">
                   <input id="reg_barrio" type="text" name="reg_barrio" placeholder="Barrio"  class="ax-input">
                </div>
                
                <div class="form-group">
                    <?php if (isset($_GET['pss'])) {
                            echo '<input id="reg_email_register" type="email" name="reg_email_register" class="ax-input" placeholder="Correo electrónico" required>';
                        } else {
                            echo '<input id="reg_email_register" type="email" name="reg_email_register" class="ax-input" placeholder="Correo electrónico" value="'.$_SESSION['correoreg'].'" required>';
                        }
                    ?>
                </div>
          </div>
          
        </div>
        
        <div class="form-group ax-interes">
            <h3>Temas de interés</h3>
            <p>Para recibir información de tu interés, por favor selecciona al menos una de las siguientes actividades.</p>
            <div id="int_group" class="ax-check-group"></div>
        </div>
    </form>
</div>
</div>

<div class="ax-mediosmail">
  <div class="container">
    <form id="form_register" name="form_register" name="registerform" method="post">

        <h3>¿Por cuál medio deseas que te contactemos?</h3>

        <div class="form-group ">
          <div class="col-md-6">
            <div class="checkbox">
                 <label class="custom-control custom-checkbox">
                      <input id="reg_med_fis" type="checkbox" name="reg_med_fis" class="ax-checkbox custom-control-input" required>
                      <span class="custom-control-indicator"></span>
                      <p>Marca aquí si NO deseas que Más vivas te contacte por medios físicos con los datos que acabas de suministrar.</p>
                  </label>
            </div>
          </div>

          <div class="col-md-6">
            <div class="checkbox">
                 <label class="custom-control custom-checkbox">
                      <input id="reg_med_vir" type="checkbox" name="reg_med_vir" class="ax-checkbox custom-control-input" required>
                      <span class="custom-control-indicator"></span>
                      <p>Marca aquí si NO deseas que Más vivas te contacte por medios virtuales con los datos que acabas de suministrar.</p>
                </label>
            </div>
          </div>

        </div>

    </form>
  </div>
</div>

<div class="ax-terminoscond">
  <div class="container">
    <form id="form_register" name="form_register" name="registerform" method="post">

      <div class="form-group">

          <div class="col-md-6">
            <div class="checkbox">
                <label class="custom-control custom-checkbox">
                  <input id="reg_terms" type="checkbox" name="reg_terms" class="ax-checkbox custom-control-input" required>
                  <span class="custom-control-indicator"></span>
                    <p><a href="/terminos-y-condiciones/" target="_blank">Acepto los términos y condiciones y el tratamiento de la información.</a></p>

                </label>
            </div>
          </div>

          <div class="col-md-6">
            <div class="checkbox">
                 <label class="custom-control custom-checkbox">
                      <input id="reg_policy" type="checkbox" name="reg_policy" class="ax-checkbox custom-control-input" required>
                      <span class="custom-control-indicator"></span>
                     <p><a href="/politicas/" target="_blank">Acepto la política de protección de información personal.</a></p>
                </label>
            </div>
          </div>

      </div>


      <div class="ax-campo">
           <p>*Campo obligatorio</p>
      </div>


        <input id="reg_expectativa" type="hidden" value="S" name="reg_expectativa">
        <div id="ax-preload"></div>
        <div class="form-group" style="text-align:center;">
           <input id="reg-submit" type="button" onclick="ax.register()" class="ax-button" value="Regístrate">
        </div>

        
    </form>
  </div>
</div>

<div class="modal fade" id="mdlregistro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="msgregistro" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <div id="redirectregistro"></div>
            </div>
        </div>
    </div>
</div>

<div class="ax-back-opa">
    <div class="ax-preload-content">
        <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
    </div>
</div>

<?php
    get_footer();
?>
<script type="text/javascript">
    var templateDir = "<?php bloginfo('template_directory'); ?>";
    $(document).ready(function() {
        ax.getInterest();
        ax.getTypesDocument();
    });
</script>

<div id="pixels"></div>
<div id="pixels2"></div>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: CO_Tena_MasVivas_Formulario
URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/registrate/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 08/17/2016
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te00;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te00;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->