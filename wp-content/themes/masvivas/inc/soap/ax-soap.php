<?php
require('../../../../../wp-blog-header.php');
require_once "nusoap/nusoap.php";
ini_set("soap.wsdl_cache_enabled", 0);

$client = new nusoap_client(URL_WEB_SERVICE);

if (isset($_POST['validate'])) {
    global $wpdb;
    $response = array();
    session_start();
    
    $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['validate']['correo'],
                                                                      'identificacion' => $_POST['validate']['numdocumento'],
                                                                      'idpais' => '1')));

    
    if ($validate['errno'] == 'VAL-0') {
        $pagea = get_page_by_title('Actualiza tus datos');
        $pageactualizar = get_page_link($pagea->ID);
        // Variables de sesion
        $idusuario = $validate['visitorid'];
        
        $wprows = $wpdb->get_results("SELECT ciudad, clave_usuario FROM wp_users_tena WHERE id_usuario = '".$idusuario."'");
        $wpdata = (array)$wprows[0];
        $userdata = array('nombre1' => utf8_encode($validate['visitorData']['nombre1']),
                                        'nombre2' => utf8_encode($validate['visitorData']['nombre2']),
                                        'apellido1' => utf8_encode($validate['visitorData']['apellido1']),
                                        'apellido2' => utf8_encode($validate['visitorData']['apellido2']),
                                        'correo' => utf8_encode($validate['visitorData']['email']),
                                        'identificacion' => utf8_encode($validate['visitorData']['identificacion']),
                                        'sexo' => utf8_encode($validate['visitorData']['sexo']),
                                        'ciudad' => utf8_encode($wpdata['ciudad']),
                                        'clave_usuario' => utf8_encode($wpdata['clave_usuario']));
        
        $_SESSION['idusuario'] = $idusuario;// A string
        $_SESSION['userdata'] = $userdata;
        $response['response'] = 'update';
        $response['redirect'] = $pageactualizar;
    }  
    else if ($validate['errno'] == 'VAL-1' || $validate['errno'] == 'VAL-4') {
        $pager = get_page_by_title('Regístrate');
        $pageregistro = get_page_link($pager->ID);
        $response['response'] = 'register';
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
        $response['redirect'] = $pageregistro;
        $_SESSION['correoreg'] = $_POST['validate']['correo'];
        $_SESSION['numdocreg'] = $_POST['validate']['numdocumento'];
    } 
    else if ($validate['errno'] == 'VAL-3' || $validate['errno'] == 'VAL-2') {
        $response['resno'] = 'email';
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    else {
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    
    echo json_encode($response);
}

if (isset($_POST['ingresaruser'])) {
    session_start();
    $response = array();
    $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['ingresaruser']['correo'],
                                                                      'identificacion' => $_POST['ingresaruser']['numdocumento'],
                                                                      'idpais' => '1')));
    
    if ($validate['errno'] == 'VAL-0') {        
        $pageredirect = '';      
        $idusuario = $validate['visitorid'];
        
        $wprows = $wpdb->get_results("SELECT ciudad, clave_usuario FROM wp_users_tena WHERE id_usuario = '".$idusuario."'");
        $wpdata = (array)$wprows[0];
        $userdata = array('nombre1' => utf8_encode($validate['visitorData']['nombre1']),
                                        'nombre2' => utf8_encode($validate['visitorData']['nombre2']),
                                        'apellido1' => utf8_encode($validate['visitorData']['apellido1']),
                                        'apellido2' => utf8_encode($validate['visitorData']['apellido2']),
                                        'correo' => utf8_encode($validate['visitorData']['email']),
                                        'identificacion' => utf8_encode($validate['visitorData']['identificacion']),
                                        'sexo' => utf8_encode($validate['visitorData']['sexo']),
                                        'ciudad' => utf8_encode($wpdata['ciudad']),
                                        'clave_usuario' => utf8_encode($wpdata['clave_usuario']));
        
        $_SESSION['idusuario'] = $idusuario;
        $_SESSION['userdata'] = $userdata;
        
        if ($wpdata['ciudad'] == 'Medellin' || $wpdata['ciudad'] == 'Bogota') {
            $wprowsret = $wpdb->get_results("SELECT id_reto FROM wp_retos WHERE id_usuario = '".$idusuario."'");
            $wpdataret = (array)$wprowsret[0];
            
            if (empty($wpdataret['id_reto'])) {
                $pagea = get_page_by_path('reto');
                $pageredirect = get_page_link($pagea->ID);   
            } else {
                $pagea = get_page_by_path('comunidad');
                $pageredirect = get_page_link($pagea->ID);
            }
        } else {
            $pagea = get_page_by_path('comunidad');
            $pageredirect = get_page_link($pagea->ID);
        }
        
        $response['redirect'] = $pageredirect;
    }   
    else if ($validate['errno'] == 'VAL-1' || $validate['errno'] == 'VAL-4') {
        $_SESSION['correoreg'] = $_POST['ingresaruser']['correo'];
        $_SESSION['numdocreg'] = $_POST['ingresaruser']['numdocumento'];
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
        $pager = get_page_by_path('registrate');
        $pageredirect = get_page_link($pager->ID);
        $response['redirect'] = $pageredirect;
    } 
    else if ($validate['errno'] == 'VAL-3' || $validate['errno'] == 'VAL-2') {
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    else {
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    
    echo json_encode($response);
}

if (isset($_POST['updatedatauser'])) {
    session_start();
    $response = array();
    $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['updatedatauser']['correo'],
                                                                      'identificacion' => $_POST['updatedatauser']['numdocumento'],
                                                                      'idpais' => '1')));

    if ($validate['errno'] == 'VAL-0') {
        $pageredirect = '';
        
        $idusuario = $validate['visitorid'];
        
        $wprows = $wpdb->get_results("SELECT ciudad, clave_usuario FROM wp_users_tena WHERE id_usuario = '".$idusuario."'");
        $wpdata = (array)$wprows[0];
        $userdata = array('nombre1' => utf8_encode($validate['visitorData']['nombre1']),
                                        'nombre2' => utf8_encode($validate['visitorData']['nombre2']),
                                        'apellido1' => utf8_encode($validate['visitorData']['apellido1']),
                                        'apellido2' => utf8_encode($validate['visitorData']['apellido2']),
                                        'correo' => utf8_encode($validate['visitorData']['email']),
                                        'identificacion' => utf8_encode($validate['visitorData']['identificacion']),
                                        'sexo' => utf8_encode($validate['visitorData']['sexo']),
                                        'ciudad' => utf8_encode($wpdata['ciudad']),
                                        'clave_usuario' => utf8_encode($wpdata['clave_usuario']));
        
        $_SESSION['idusuario'] = $idusuario;
        $_SESSION['userdata'] = $userdata;
        $pagea = get_page_by_title('Actualiza tus datos');
        $pageredirect = get_page_link($pagea->ID);
        $response['redirect'] = $pageredirect;
    } else if ($validate['errno'] == 'VAL-1' || $validate['errno'] == 'VAL-4') {
        $_SESSION['correoreg'] = $_POST['updatedatauser']['correo'];
        $_SESSION['numdocreg'] = $_POST['updatedatauser']['numdocumento'];
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
        $pager = get_page_by_path('registrate');
        $pageredirect = get_page_link($pager->ID);
        $response['redirect'] = $pageredirect;
    } 
    else if ($validate['errno'] == 'VAL-3' || $validate['errno'] == 'VAL-2') {
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    else {
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    
    echo json_encode($response);
}

if (isset($_POST['getuserinfo'])) {
    global $wpdb;
    $response = array();
    session_start();
    
    $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['getuserinfo']['correo'],
                                                                      'identificacion' => $_POST['getuserinfo']['numdocumento'],
                                                                      'idpais' => '1')));
    
    $userdata = array('nombre1' => utf8_encode($validate['visitorData']['nombre1']),
                      'nombre2' => utf8_encode($validate['visitorData']['nombre2']),
                      'apellido1' => utf8_encode($validate['visitorData']['apellido1']),
                      'apellido2' => utf8_encode($validate['visitorData']['apellido2']),
                      'tipoidentificacion' => $validate['visitorData']['tipoidentificacion'],
                      'identificacion' => $validate['visitorData']['identificacion'],
                      'barrio' => utf8_encode($validate['visitorData']['barrio']),
                      'direccion' => $validate['visitorData']['direccion'],
                      'fechanacimiento' => $validate['visitorData']['fechanacimiento'],
                      'celular' => $validate['visitorData']['celular'],
                      'correo' => $validate['visitorData']['email'],
                      'identificacion' => $validate['visitorData']['identificacion'],
                      'sexo' => $validate['visitorData']['sexo'],
                      'idciudad' => $validate['visitorData']['idciudad'],
                      'iddepartamento' => $validate['visitorData']['iddepartamento'],
                      'idpais' => $validate['visitorData']['idpais'],
                      'contactoemail' => $validate['visitorData']['contactoemail'],
                      'contactoresidencia' => $validate['visitorData']['contactoresidencia'],
                      'terminosYcondiciones' => $validate['visitorData']['terminosYcondiciones'],
                      'politicaPrivacidad' => $validate['visitorData']['politicaPrivacidad']);
    
    $response[] = $userdata;
    
    $depart = $client->call('getDepartamentos', array('idPais' => (int)$validate['visitorData']['idpais']));
    $deparm = array();
    
    foreach ($depart as $dep) {
        array_push($deparm, array('id' => $dep['iddepartamento'], 'departamento' => utf8_encode($dep['nombre'])));
    }
    
    $city = $client->call('getCiudades', array('idDepartamento' => (int)$validate['visitorData']['iddepartamento']));
    $cities = array();
    
    foreach ($city as $citi) {
        array_push($cities, array('id' => $citi['idciudad'], 'ciudad' => utf8_encode($citi['nombre'])));
    }
    
    $interest =  $client->call('getIntereses');
    $intereses = array();
    $allintereses = array();
    
    foreach ($interest as $allinte) {
        foreach ($validate['interesesData'] as $interdata) {
            foreach ($allinte['interesData'] as $getdata) {
                if ($getdata == $interdata) {
                   if (!in_array($allinte['categoria'], $allintereses)) {
                       $allintereses[] = $allinte['categoria'];
                   }
                }
            }
        }
        if ($allinte['interesData'] == $validate['interesesData']) {
            $allintereses[] = $allinte['categoria'];
        }
    }
    
    $response['temasinteres'] = $allintereses;
    $response['departamentos'] = $deparm;
    $response['ciudades'] = $cities;
    echo json_encode($response);
}

if (isset($_POST['idcountry'])) {
    $depart = $client->call('getDepartamentos', array('idPais' => (int)$_POST['idcountry']));
    $deparm = array();
    
    foreach ($depart as $dep) {
        array_push($deparm, array('id' => $dep['iddepartamento'], 'departamento' => utf8_encode($dep['nombre'])));
    }
    
    echo json_encode($deparm);
}

if (isset($_POST['iddepart'])) {
    $city = $client->call('getCiudades', array('idDepartamento' => (int)$_POST['iddepart']));
    $cities = array();
    
    foreach ($city as $citi) {
        array_push($cities, array('id' => $citi['idciudad'], 'ciudad' => utf8_encode($citi['nombre'])));
    }
    
    echo json_encode($cities);
}

if (isset($_POST['intereses'])) {
    $interest =  $client->call('getIntereses');
    $intereses = array();
    
    foreach ($interest as $int) {
        array_push($intereses, array('name' => utf8_encode($int['categoria'])));   
    }
    
    echo json_encode($intereses);
}

if (isset($_POST['tipodoc'])) {
    $tdoc =  $client->call('getTiposDocumentos', array('idPais' => 1));
    $tiposdoc = array();
    
    foreach ($tdoc as $td) {
        array_push($tiposdoc, array('id' => $td['idtipo'], 'documento' => utf8_encode($td['nombre'])));
    }
    
    echo json_encode($tiposdoc);
}

if (isset($_POST['datauser'])) {
    global $wpdb;
    $response = array('res' => '', 'msg' => '', 'visitordata' => array());
    
    $interest =  $client->call('getIntereses');
    $objtemas = explode(',', $_POST['datauser']['temas']);
    $intereses = array();
    
    foreach ($interest as $int) {
        foreach ($objtemas as $temasel) {
            if ($temasel == $int['categoria']) {
                if ($int['categoria'] == 'Otros') {
                    $intereses[] = new soapval('long', 'xsd:long', (int)'137');     
                } else {
                    foreach ($int['interesData'] as $intdata) {
                        $totaldata = explode('|',$intdata);
                        $intereses[] = new soapval('long', 'xsd:long', (int)$totaldata[0]); 
                    } 
                }    
            }
        }
    }
    
    $registerVisitor = $client->call('registroVisitor', array('UsuarioLightsReq' => array('nombre1' => utf8_decode($_POST['datauser']['nombre']),
                                                                                          'nombre2' => utf8_decode($_POST['datauser']['segnombre']),
                                                                                          'apellido1' => utf8_decode($_POST['datauser']['apellido']),
                                                                                          'apellido2' => utf8_decode($_POST['datauser']['segapellido']),
                                                                                          'tipoidentificacion' => $_POST['datauser']['tipodocumento'],
                                                                                          'identificacion' => $_POST['datauser']['numdocumento'],
                                                                                          'fechanacimiento' => $_POST['datauser']['anon'].$_POST['datauser']['mesn'].$_POST['datauser']['dian'],
                                                                                          'sexo' => $_POST['datauser']['genero'],
                                                                                          'idpais' => $_POST['datauser']['pais'],
                                                                                          'iddepartamento' => $_POST['datauser']['departamento'],
                                                                                          'idciudad' => $_POST['datauser']['ciudad'],
                                                                                          'direccion' => $_POST['datauser']['direccion'],
                                                                                          'barrio' => utf8_decode($_POST['datauser']['barrio']),
                                                                                          'email' => $_POST['datauser']['correo'],
                                                                                          'celular' => $_POST['datauser']['celular'],
                                                                                          'interesesData' => $intereses,
                                                                                          'contactoresidencia' => $_POST['datauser']['contactoresidencia'],
                                                                                          'contactoemail' => $_POST['datauser']['contactoemail'],
                                                                                          'terminosYcondiciones' => $_POST['datauser']['terminos'],
                                                                                          'politicaPrivacidad' => $_POST['datauser']['politica'],
                                                                                          'expectativa' => $_POST['datauser']['expectativa'])));
    
    
    if ($registerVisitor['errno'] == 'REG-0') {
        
        $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['datauser']['correo'],
                                                                      'identificacion' => $_POST['datauser']['numdocumento'],
                                                                      'idpais' => '1')));
        $userdata_t = array( 
            'id_usuario' => $validate['visitorid'],
            'nombre'  =>   ucfirst(strtolower($_POST['datauser']['nombre'])).' '.ucfirst(strtolower($_POST['datauser']['segnombre'])),
            'apellido' =>  ucfirst(strtolower($_POST['datauser']['apellido'])).' '.ucfirst(strtolower($_POST['datauser']['segapellido'])),
            'tipo_documento' => $_POST['datauser']['tipodocumento'],
            'numero_documento' => $_POST['datauser']['numdocumento'],
            'fecha_nacimiento' => $_POST['datauser']['anon'].'-'.$_POST['datauser']['mesn'].'-'.$_POST['datauser']['dian'],
            'genero' => $_POST['datauser']['genero'],
            'pais' => $_POST['datauser']['namepais'],
            'departamento' => $_POST['datauser']['namedepartamento'],
            'ciudad' => $_POST['datauser']['nameciudad'],
            'direccion' => $_POST['datauser']['direccion'],
            'barrio' => $_POST['datauser']['barrio'],
            'correo' => $_POST['datauser']['correo'],
            'celular' => $_POST['datauser']['celular'],
            'temasinteres' => $_POST['datauser']['temas'],
            'contactoresidencia' => $_POST['datauser']['contactoresidencia'],
            'contactoemail' => $_POST['datauser']['contactoemail'],
            'terminos' => $_POST['datauser']['terminos'],
            'politica' => $_POST['datauser']['politica'],
            'expectativa' => $_POST['datauser']['expectativa'],
            'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
            'createduser' => current_time('mysql'),
            'updateuser' => current_time('mysql')
         );

        $usert = $wpdb->insert('wp_users_tena', $userdata_t);
        
        if (!is_wp_error($usert)) {
            session_start();
            $_SESSION['idusuario'] = $userdata_t['id_usuario'];
            $_SESSION['userdata'] = $userdata_t;
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            
            if ($userdata_t['ciudad'] == 'Medellin' || $userdata_t['ciudad'] == 'Bogota') {
                $pager = get_page_by_path('reto');
                $pagereto = get_page_link($pager->ID);
                $response['redirect'] = $pagereto;
            } else if (isset($_SESSION['pagecomment'])) {
                $response['redirect'] = $_SESSION['pagecomment'];
            } else {
                $pagec = get_page_by_path('comunidad');
                $pagecomunidad = get_page_link($pagec->ID);
                $response['redirect'] = $pagecomunidad;
            }
        } else {
            $response['res'] = 'errorlocal';
            $response['msg'] = $usert->get_error_message(); 
        }   
    } else if ($registerVisitor['errno'] == 'REG-1') {
        $response['res'] = 'datos';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'VAL-2') {
        $response['res'] = 'email';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'VAL-3') {
        $response['res'] = 'identificacion';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-4') {
        $response['res'] = 'errordb';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-5') {
        $response['res'] = 'errordb';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-6') {
        $response['res'] = 'userexist';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-7') {
        $response['res'] = 'ismen';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-8') {
        $response['res'] = 'is35';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else {
        $response['res'] = 'errorgeneral';
        $response['msg'] = $registerVisitor;
    }
    
    echo json_encode($response);
}

if (isset($_POST['datauserteq'])) {
    global $wpdb;
    $response = array('res' => '', 'msg' => '', 'visitordata' => array());
    
    $interest =  $client->call('getIntereses');
    $objtemas = explode(',', $_POST['datauserteq']['temas']);
    $intereses = array();
    
    foreach ($interest as $int) {
        foreach ($objtemas as $temasel) {
            if ($temasel == $int['categoria']) {
                if ($int['categoria'] == 'Otros') {
                    $intereses[] = new soapval('long', 'xsd:long', (int)'137');     
                } else {
                    foreach ($int['interesData'] as $intdata) {
                        $totaldata = explode('|',$intdata);
                        $intereses[] = new soapval('long', 'xsd:long', (int)$totaldata[0]); 
                    } 
                }    
            }
        }
    }
    
    $registerVisitor = $client->call('registroVisitor', array('UsuarioLightsReq' => array('nombre1' => utf8_decode($_POST['datauserteq']['nombre']),
                                                                                          'nombre2' => utf8_decode($_POST['datauserteq']['segnombre']),
                                                                                          'apellido1' => utf8_decode($_POST['datauserteq']['apellido']),
                                                                                          'apellido2' => utf8_decode($_POST['datauserteq']['segapellido']),
                                                                                          'tipoidentificacion' => $_POST['datauserteq']['tipodocumento'],
                                                                                          'identificacion' => $_POST['datauserteq']['numdocumento'],
                                                                                          'fechanacimiento' => $_POST['datauserteq']['anon'].$_POST['datauserteq']['mesn'].$_POST['datauserteq']['dian'],
                                                                                          'sexo' => $_POST['datauserteq']['genero'],
                                                                                          'idpais' => $_POST['datauserteq']['pais'],
                                                                                          'iddepartamento' => $_POST['datauserteq']['departamento'],
                                                                                          'idciudad' => $_POST['datauserteq']['ciudad'],
                                                                                          'direccion' => $_POST['datauserteq']['direccion'],
                                                                                          'barrio' => utf8_decode($_POST['datauserteq']['barrio']),
                                                                                          'email' => $_POST['datauserteq']['correo'],
                                                                                          'celular' => $_POST['datauserteq']['celular'],
                                                                                          'interesesData' => $intereses,
                                                                                          'contactoresidencia' => $_POST['datauserteq']['contactoresidencia'],
                                                                                          'contactoemail' => $_POST['datauserteq']['contactoemail'],
                                                                                          'terminosYcondiciones' => $_POST['datauserteq']['terminos'],
                                                                                          'politicaPrivacidad' => $_POST['datauserteq']['politica'],
                                                                                          'expectativa' => $_POST['datauserteq']['expectativa'])));
    
    
    
    if ($registerVisitor['errno'] == 'REG-0') {
        
        $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['datauserteq']['correo'],
                                                                      'identificacion' => $_POST['datauserteq']['numdocumento'],
                                                                      'idpais' => '1')));
        $userdata_t = array( 
            'id_usuario' => $validate['visitorid'],
            'nombre'  =>  $_POST['datauserteq']['nombre'].' '.$_POST['datauserteq']['segnombre'],
            'apellido' =>  $_POST['datauserteq']['apellido'].' '.$_POST['datauserteq']['segapellido'],
            'tipo_documento' => $_POST['datauserteq']['tipodocumento'],
            'numero_documento' => $_POST['datauserteq']['numdocumento'],
            'fecha_nacimiento' => $_POST['datauserteq']['anon'].'-'.$_POST['datauserteq']['mesn'].'-'.$_POST['datauserteq']['dian'],
            'genero' => $_POST['datauserteq']['genero'],
            'pais' => $_POST['datauserteq']['namepais'],
            'departamento' => $_POST['datauserteq']['namedepartamento'],
            'ciudad' => $_POST['datauserteq']['nameciudad'],
            'direccion' => $_POST['datauserteq']['direccion'],
            'correo' => $_POST['datauserteq']['correo'],
            'celular' => $_POST['datauserteq']['celular'],
            'temasinteres' => $_POST['datauserteq']['temas'],
            'contactoresidencia' => $_POST['datauserteq']['contactoresidencia'],
            'contactoemail' => $_POST['datauserteq']['contactoemail'],
            'terminos' => $_POST['datauserteq']['terminos'],
            'politica' => $_POST['datauserteq']['politica'],
            'expectativa' => $_POST['datauserteq']['expectativa'],
            'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
            'createduser' => current_time('mysql'),
            'updateuser' => current_time('mysql')
         );

        $usert = $wpdb->insert('wp_users_tena', $userdata_t);

        if (!is_wp_error($usert)) {
            $retdata = array('id_usuario' => $validate['visitorid'],
                             'url_archivo' => $_POST['datauserteq']['urlvid'],
                             'rg_tequila' => 1);
            $retot = $wpdb->insert('wp_retos', $retdata);
            
            if (!is_wp_error($retot)) {
                $response['res'] = 'success';
                $response['msg'] = $registerVisitor['errormsg'];
            }  
        } else {
            $response['res'] = 'errorlocal';
            $response['msg'] = $usert->get_error_message(); 
        }   
    } else if ($registerVisitor['errno'] == 'REG-1') {
        $response['res'] = 'datos';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'VAL-2') {
        $response['res'] = 'email';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'VAL-3') {
        $response['res'] = 'identificacion';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-4') {
        $response['res'] = 'errordb';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-5') {
        $response['res'] = 'errordb';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-6') {
        $response['res'] = 'userexist';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-7') {
        $response['res'] = 'ismen';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else if ($registerVisitor['errno'] == 'REG-8') {
        $response['res'] = 'is35';
        $response['msg'] = utf8_encode($registerVisitor['errormsg']);
    } else {
        $response['res'] = 'errorgeneral';
        $response['msg'] = $registerVisitor;
    }
    
    echo json_encode($response);
}

if (isset($_POST['datauserupd'])) {
    global $wpdb;
    session_start();
    $response = array('res' => '', 'msg' => '', 'visitordata' => array());
    
    $interest =  $client->call('getIntereses');
    $objtemas = explode(',', $_POST['datauserupd']['temas']);
    $intereses = array();
    
    foreach ($interest as $int) {
        foreach ($objtemas as $temasel) {
            if ($temasel == $int['categoria']) {
                if ($int['categoria'] == 'Otros') {
                    $intereses[] = new soapval('long', 'xsd:long', (int)'137');     
                } else {
                    foreach ($int['interesData'] as $intdata) {
                        $totaldata = explode('|',$intdata);
                        $intereses[] = new soapval('long', 'xsd:long', (int)$totaldata[0]); 
                    } 
                }    
            }
        }
    }
    
    $updateVisitor = $client->call('actualizarVisitor', array('UsuarioLightsReq' => array('nombre1' => utf8_decode($_POST['datauserupd']['nombre']),
                                                                                          'nombre2' => utf8_decode($_POST['datauserupd']['segnombre']),
                                                                                          'apellido1' => utf8_decode($_POST['datauserupd']['apellido']),
                                                                                          'apellido2' => utf8_decode($_POST['datauserupd']['segapellido']),
                                                                                          'tipoidentificacion' => $_POST['datauserupd']['tipodocumento'],
                                                                                          'identificacion' => $_POST['datauserupd']['numdocumento'],
                                                                                          'fechanacimiento' => $_POST['datauserupd']['anon'].$_POST['datauserupd']['mesn'].$_POST['datauserupd']['dian'],
                                                                                          'sexo' => $_POST['datauserupd']['genero'],
                                                                                          'idpais' => $_POST['datauserupd']['pais'],
                                                                                          'iddepartamento' => $_POST['datauserupd']['departamento'],
                                                                                          'idciudad' => $_POST['datauserupd']['ciudad'],
                                                                                          'direccion' => $_POST['datauserupd']['direccion'],
                                                                                          'barrio' => utf8_decode($_POST['datauserupd']['barrio']),
                                                                                          'email' => $_POST['datauserupd']['correo'],
                                                                                          'celular' => $_POST['datauserupd']['celular'],
                                                                                          'interesesData' => $intereses,
                                                                                          'contactoresidencia' => $_POST['datauserupd']['contactoresidencia'],
                                                                                          'contactoemail' => $_POST['datauserupd']['contactoemail'],
                                                                                          'terminosYcondiciones' => $_POST['datauserupd']['terminos'],
                                                                                          'politicaPrivacidad' => $_POST['datauserupd']['politica'],
                                                                                          'visitorid' => new soapval('visitorid','long',(int)$_SESSION['idusuario'],false,false,false))));
    
    
    if ($updateVisitor['errno'] == 'UPD-0') {
        
        $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['datauserupd']['correo'],
                                                                      'identificacion' => $_POST['datauserupd']['numdocumento'],
                                                                      'idpais' => '1')));
        $userdata_t = array( 
            'id_usuario' => $validate['visitorid'],
            'nombre'  =>  $_POST['datauserupd']['nombre'].' '.$_POST['datauserupd']['segnombre'],
            'apellido' =>  $_POST['datauserupd']['apellido'].' '.$_POST['datauserupd']['segapellido'],
            'numero_documento' => $_POST['datauserupd']['numdocumento'],
            'fecha_nacimiento' => $_POST['datauserupd']['anon'].'-'.$_POST['datauserupd']['mesn'].'-'.$_POST['datauserupd']['dian'],
            'genero' => $_POST['datauserupd']['genero'],
            'pais' => $_POST['datauserupd']['namepais'],
            'departamento' => $_POST['datauserupd']['namedepartamento'],
            'ciudad' => $_POST['datauserupd']['nameciudad'],
            'direccion' => $_POST['datauserupd']['direccion'],
            'correo' => $_POST['datauserupd']['correo'],
            'celular' => $_POST['datauserupd']['celular'],
            'temasinteres' => $_POST['datauserupd']['temas'],
            'contactoresidencia' => $_POST['datauserupd']['contactoresidencia'],
            'contactoemail' => $_POST['datauserupd']['contactoemail'],
            'terminos' => $_POST['datauserupd']['terminos'],
            'politica' => $_POST['datauserupd']['politica'],
            'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
            'updateuser' => current_time('mysql')
         );
        
        $consultu = $wpdb->get_results('SELECT id_usuario FROM wp_users_tena WHERE id_usuario = "'.$_SESSION['idusuario'].'"');
        
        if (!empty($consultu)) {
            $usert = $wpdb->update('wp_users_tena', $userdata_t, array('id_usuario' => $_SESSION['idusuario']));
        } else {
            $usert = $wpdb->insert('wp_users_tena', array( 
                                                    'id_usuario' => $validate['visitorid'],
                                                    'nombre'  =>  $_POST['datauserupd']['nombre'].' '.$_POST['datauserupd']['segnombre'],
                                                    'apellido' =>  $_POST['datauserupd']['apellido'].' '.$_POST['datauserupd']['segapellido'],
                                                    'tipo_documento' => $_POST['datauserupd']['tipodocumento'],
                                                    'numero_documento' => $_POST['datauserupd']['numdocumento'],
                                                    'fecha_nacimiento' => $_POST['datauserupd']['anon'].'-'.$_POST['datauserupd']['mesn'].'-'.$_POST['datauserupd']['dian'],
                                                    'genero' => $_POST['datauserupd']['genero'],
                                                    'pais' => $_POST['datauserupd']['namepais'],
                                                    'departamento' => $_POST['datauserupd']['namedepartamento'],
                                                    'ciudad' => $_POST['datauserupd']['nameciudad'],
                                                    'direccion' => $_POST['datauserupd']['direccion'],
                                                    'correo' => $_POST['datauserupd']['correo'],
                                                    'celular' => $_POST['datauserupd']['celular'],
                                                    'temasinteres' => $_POST['datauserupd']['temas'],
                                                    'contactoresidencia' => $_POST['datauserupd']['contactoresidencia'],
                                                    'contactoemail' => $_POST['datauserupd']['contactoemail'],
                                                    'terminos' => $_POST['datauserupd']['terminos'],
                                                    'politica' => $_POST['datauserupd']['politica'],
                                                    'expectativa' => 'S',
                                                    'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
                                                    'createduser' => current_time('mysql'),
                                                    'updateuser' => current_time('mysql')
                                                 ));
        }
        
        if (!is_wp_error($usert)) {
            if ($userdata_t['ciudad'] == 'Medellin' || $userdata_t['ciudad'] == 'Bogota') {
                $pager = get_page_by_path('reto');
                $pagereto = get_page_link($pager->ID);
                $response['redirect'] = $pagereto;
            } else if (isset($_SESSION['pagecomment'])) {
                $response['redirect'] = $_SESSION['pagecomment'];
            } else {
                $pagec = get_page_by_path('comunidad');
                $pagecomunidad = get_page_link($pagec->ID);
                $response['redirect'] = $pagecomunidad;
            } 
        } else {
            $response['res'] = 'errorlocal';
            $response['msg'] = $usert->get_error_message(); 
        }   
    } else if ($updateVisitor['errno'] == 'UPD-1') {
        $response['res'] = 'datos';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-2') {
        $response['res'] = 'email';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-3') {
        $response['res'] = 'identificacion';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-4') {
        $response['res'] = 'errordb';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-5') {
        $response['res'] = 'errordb';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-6') {
        $response['res'] = 'userexist';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-7') {
        $response['res'] = 'ismen';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else if ($updateVisitor['errno'] == 'UPD-8') {
        $response['res'] = 'is35';
        $response['msg'] = utf8_encode($updateVisitor['errormsg']);
    } else {
        $response['res'] = 'errorgeneral';
        $response['msg'] = $updateVisitor;
    }
    
    echo json_encode($response);
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'año',
        'm' => 'mes',
        'w' => 'semana',
        'd' => 'día',
        'h' => 'hora',
        'i' => 'minuto',
        's' => 'segundo',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? 'Hace ' . implode(', ', $string)  : 'Justo ahora';
}

if (isset($_POST['allcomments'])) {
    session_start();
    $post_id = $_POST['allcomments']['idpost'];
    $comments = get_comments(array('post_id' => $post_id, 'status' => 'approve', 'parent' => 0 ));
    $html = '';
    foreach($comments as $comment){

        $subcomments = get_comments(array('post_id' => $post_id, 'status' => 'approve', 'parent' => $comment->comment_ID ));

        $html .= '<div class="ax-userbg">
        <input id="thepostid" type="hidden" value="'.$post_id.'"/>
        <div class="ax-user">
            <h4>'.$comment->comment_author.'</h4>
            <span>· '.time_elapsed_string($comment->comment_date_gmt).'</span>';
        
            if (isset($_SESSION['idusuario'])) {                
                /*if ($comment->user_id == $_SESSION['idusuario']) {
                    $html .= '<p>'.$comment->comment_content.'<a onclick="editComment()"><i class="fa fa-pencil"></i>Editar</a></p>';
                } else {
                    $html .= '<p>'.$comment->comment_content.'</p>';   
                }*/
                $html .= '<p id="p_subirsubcomentarios'.$comment->comment_ID.'">'.$comment->comment_content.'</p>'; 
            } else {
                $html .= '<p id="p_subirsubcomentarios'.$comment->comment_ID.'">'.$comment->comment_content.'</p>';
            }
            if ($comment->comment_author_url != '') {
             $html .= '<img class="img-responsive center-block" alt="500x500" src="'.$comment->comment_author_url.'">';   
            }
          $html .= '</div>
          <div class="row">
            <div class="ax-compartir">
              <div class="col-md-4 col-xs-4">
                <i class="fa fa-commenting" aria-hidden="true"></i>
                <span>'.count($subcomments).'</span>
              </div>
              <div class="col-md-8 col-xs-8">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                <a href="javascript:void(0)" onclick="ax.shareFacebook('."'".$comment->comment_author."'".','."'".$comment->comment_content."'".','."'".$comment->comment_author_url."'".')"><span>Compartir</span></a>
              </div>
            </div>
          </div>';

          foreach($subcomments as $subcomment){
          $html .= '<div class="ax-textcomentario">
            <div class="ax-nombreusuario">
                <p>
                  <span>'.$subcomment->comment_author.'</span> '.$subcomment->comment_content.'
                </p>
              <div class="ax-usuarioimagen">';
                if ($subcomment->comment_author_url != '') {
                    $html .= '<img class="img-responsive center-block" alt="500x500" src="'.$subcomment->comment_author_url.'">';   
                }
              $html .= '</div>
            </div>
          </div>';
          }
          $html .= '<div class="ax-subircomentarios">
            <input id="reg_subirsubcomentarios'.$comment->comment_ID.'" type="text" name="reg_subirsubcomentarios" parentid="'.$comment->comment_ID.'" class="ax-input form-control" placeholder="Escribe tu comentario" required/>
             <span id="spanb'.$comment->comment_ID.'" class="btn btn-default btn-file"><i class="fa fa-camera" aria-hidden="true"></i><input type="file" name="my_image_upload_comment" id="my_image_upload_comment'.$comment->comment_ID.'"  multiple="false"  /></span>
             <input type="button" class="ax-button btn btn-default" onclick="ax.submitSubComment('."'".'#reg_subirsubcomentarios'.$comment->comment_ID."'".', $('."'".'#my_image_upload_comment'.$comment->comment_ID."'".'),'.$comment->comment_ID.')" value="Comentar" />
             <div id="msgsubcomment'.$comment->comment_ID.'"></div>
          </div>
          </div>
          <script type="text/javascript">
              $("#my_image_upload_comment'.$comment->comment_ID.'").change(function () {
                    if (this.value != "") {
                        $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-camera");
                        $("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-check");
                        $("#spanb'.$comment->comment_ID.' i.fa").css("color","#fff");
                        $("#spanb'.$comment->comment_ID.'").css("background-color","#ad004b");
                    } else {
                        $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-check");
                        $("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-camera");
                        $("#spanb'.$comment->comment_ID.' i.fa").css("color","#b2b2b2");
                        $("#spanb'.$comment->comment_ID.'").css("background-color","#fff");
                    } 
                });';
            $currcomment = "#reg_subirsubcomentarios".$comment->comment_ID;
            
            if ($_SESSION['inputcomment'] == $currcomment) {
                $html .= '$("'.$_SESSION['inputcomment'].'").val("'.$_SESSION['comment'].'");';
            }
        
          $html .= '$("#reg_subirsubcomentarios'.$comment->comment_ID.'").emojioneArea();</script>';
    }
    
    if (isset($_SESSION['comment'])) {
        unset($_SESSION['comment']);
        unset($_SESSION['inputcomment']);
    }
    
    echo json_encode(array("html" => (string)$html));
}
 
if (isset($_POST['content'])) {

        session_start();
        require_once('../../../../../wp-admin/includes/image.php' );
        require_once('../../../../../wp-admin/includes/file.php' );
        require_once( '../../../../../wp-admin/includes/media.php' );

    if (isset($_SESSION['idusuario'])) {
        
        if($_POST['parent'] =='') {
            $p='_p';
        } else {
            $p='';
        }
            
        $attachment_id = media_handle_upload( 'my_image_upload_comment'.$p.'',$_SESSION['idusuario']);
        $consulta_post = "SELECT * FROM wp_posts WHERE post_parent=".$_SESSION['idusuario']." ";
        $resultado_post = $wpdb->get_results( $consulta_post );
        $url_post = '';

        if (!is_wp_error($attachment_id)) {
            foreach ($resultado_post as $key => $value) {
                $url_post = wp_get_attachment_url($value->ID);
            }   
        }

        $post_id = $_POST['page_type'];
        $content = $_POST['content'];
        $parentid = $_POST['parent'] ? $_POST['parent'] : 0;
        $name = $_SESSION['userdata']['nombre1'] . ' ' . $_SESSION['userdata']['apellido1'];
        $email = $_SESSION['userdata']['email'];

            $datos=array( 
            'id_usuario' => $_SESSION['idusuario'],
            'url_archivo' => $url_post, 
            'descripcion' => $_POST["descripcion_reto"] 
            );

        $time = current_time('mysql');

        $data = array(
            'comment_post_ID' => $post_id ,
            'comment_author' => $name,
            'comment_author_email' => $email,
            'comment_author_url' => $url_post ,
            'comment_content' => $content,
            'comment_type' => '',
            'comment_parent' => $parentid,
            'user_id' => 1,
            'comment_author_IP' => '127.0.0.1',
            'comment_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
            'comment_date' => $time,
            'comment_approved' => 1,
        );

        $commentid = wp_insert_comment($data);

        $response['commentid'] = $commentid;
        $response['msg'] = 'Tu comentario ha sido enviado con &eacute;xito.';   
    } else {
        $_SESSION['comment'] = $_POST['content'];
        $_SESSION['inputcomment'] = $_POST['inputcomment'];
        $_SESSION['pagecomment'] = $_POST['pagecomment'];
        $pager = get_page_by_title('Regístrate');
        $pageregistro = get_page_link($pager->ID);
        $response['pageregister'] = $pageregistro;
        $response['msg'] = 'sesion';
    }

    echo json_encode($response);
}
?>