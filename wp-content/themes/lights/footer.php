<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
?>
<div class="blog-footer">
    <?php get_sidebar( 'footer' ); ?>
    <?php $copyright_text = get_theme_mod( 'copyrighttext', '' ); ?>
</div>
<script type=text/javascript>
    var siteurl = '<?php echo get_site_url(); ?>';
    var templateDir = "<?php bloginfo('template_directory'); ?>";
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#icon").click(function(){
            $("#icon1").fadeToggle("slow");
            $("#icon2").fadeToggle("slow");
            $("#icon3").fadeToggle("slow");
        });
    });
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/libs/emojionearea/dist/jquery.textcomplete.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/ax-register.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/swfobject.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jwplayer.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jwplayer.license.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/scripts.min.js"></script>
<?php
    /*
     * Always have wp_footer() just before the closing </body>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to reference JavaScript files.
     */
    wp_footer();
?>

</body>
</html>