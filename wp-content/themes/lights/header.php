<?php
/**
* Header template for our theme
*
* Displays all of the <head> section and everything up till <div id="main">.
*
* @package Bootstrap Canvas WP
* @since Bootstrap Canvas WP 1.0
*/
$now = time();
if (isset($_SESSION['inicio-sesion']) && $now > $_SESSION['inicio-sesion']) {
    session_unset();
    session_destroy();
    wp_redirect(home_url());
}
session_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script>
    /* Google Tag Manager */
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WNZTX7L');
    /* End Google Tag Manager */
</script>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, minimum-scale=0.25,maximum-scale=1.6,initial-scale=1.0,user-scalable=no"/>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() ?>/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<title><?php echo bloginfo('name'); ?></title>
<?php
/*
* We add some JavaScript to pages with the comment form
* to support sites with threaded comments (when in use).
*/
if ( is_singular() && get_option( 'thread_comments' ) ){
    wp_enqueue_script( 'comment-reply' );
}

if (isset($_POST['cerrarSesion'])) {
    session_unset();
    session_destroy();
    wp_redirect(home_url());
    /*echo json_encode(array('sessionClosed' => true));*/
}
?>
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/favicon/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/favicon/favicon-32x32.png">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-76x76.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri() ?>/favicon/favicon-96x96.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri() ?>/favicon/android-icon-192x192.png">
<link rel="manifest" href="<?php echo get_template_directory_uri() ?>/favicon/manifest.json">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jPages.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/localization/messages_es.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/flipclock.min.js"></script>

<?php
/*
* Always have wp_head() just before the closing </head>
* tag of your theme, or you will break many plugins, which
* generally use this hook to add elements to <head> such
* as styles, scripts, and meta tags.
*/
wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WNZTX7L"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php $header_image = get_header_image(); ?>
    <div id="pixelsall">
        <!-- Start of DoubleClick Floodlight Tag: Please do not remove
        Activity name of this tag: CO_Tena_MasVivas_Home
        URL of the webpage where the tag is expected to be placed: http://www.lightsbytena.co/
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 11/08/2016 -->
        <script type="text/javascript">
            var axel = Math.random() + "";
            var a = axel * 10000000000000;
            document.write('<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>
        <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>
        <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
    </div>
    <div id="pixelsall2"></div>
    <div id="pixels"></div>
    <div id="pixelsf"></div>
    <div id="pixelsf2"></div>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-2 hidden-xs">
                <h1><a href="<?php echo get_site_url(); ?>"><img class="img-responsive" alt="" src="/wp-content/themes/lights/img/ax-logo.png" alt="Lights by TENA" title="Lights by TENA"></a></h1>
            </div>
            <div class="col-sm-7 hidden-xs">
                <ul id="menu-top-menu" class="nav nav-pills pull-right">
                    <?php
                    (string)$account = '';
                    if (isset($_SESSION['idusuario'])) {
                        $account .= '<li class="menu-item menu-item-type-custom menu-item-object-custom" style="border-right: 2px solid #db74a6; margin-right:8px;">
                        <a href="javascript:void(0)" title="Cerrar sesión" onclick="ax.cerrarSesion()"><i class="fa fa-user-times"></i> Cerrar sesión</a>
                        <form id="frmCerrar" method="post">
                        <input type="hidden" name="cerrarSesion" value="true">
                        </form>
                        </li> <li class="menu-item menu-item-object-page"><a title="Actualiza tus datos" href="/actualiza-tus-datos/">Actualiza tus datos</a></li>';
                    } else {
                        $account = '<li class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a href="/iniciar-sesion/" title="Iniciar sesión">Iniciar sesión</a>
                        </li>

                        <li class="menu-item menu-item-type-post_type menu-item-object-page" style="border-right: 2px solid #db74a6;">
                        <a title="Regístrate comunidad Lights" href="/registrate">Registro comunidad Lights</a>
                        </li>';
                    }
                    echo $account;
                    wp_nav_menu( array(
                        'menu'              => 'topmenu',
                        'theme_location'    => 'topmenu',
                        'depth'             => 5,
                        'container'         => '',
                        'container_class'   => '',
                        'container_id'      => '',
                        'before'            => '',
                        'items_wrap' => '%3$s',
                        'menu_class'        => 'nav nav-pills pull-right',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                );

                ?>
                </ul>
            </div>
            <div class="col-md-2 col-sm-3  hidden-xs ax-iconosredes pull-right">
                <div class="row">
                    <?php dynamic_sidebar( 'social-widget-area' ); ?>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"><?php _e( 'Toggle navigation', 'bootstrapcanvaswp' ); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h1 class="visible-xs"><a href="<?php echo get_site_url(); ?>"><img class="img-responsive" alt="" src="/wp-content/themes/lights/img/ax-logo.png"></a></h1>
            <div class="visible-xs">
                <ul id="ax-iconouser" class="">

                    <li id="icon" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </li>
                    <?php if (isset($_SESSION['idusuario'])): ?>
                        <li id="icon2"  class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="javascript:void(0)" title="Cerrar sesión" onclick="ax.cerrarSesion()">
                                <!-- <i class="fa fa-user-times" aria-hidden="true"></i> -->
                                <span>Cerrar sesión</span>
                            </a>
                            <form id="frmCerrar" method="post">
                                <input type="hidden" name="cerrarSesion" value="true">
                            </form>
                            <pre style="display:none"><?php echo $_SESSION['idusuario'].'</br>'.$_SESSION['userdata']['correo'].'</br>'.$_SESSION['userdata']['identificacion'];?></pre>
                        </li>
                    <?php else: ?>
                        <li id="icon1"  class="menu-item menu-item-type-custom menu-item-object-custom">
                            <!--<a onclick="$('#mdllogin').modal('show');" href="javascript:void(0)" title="Iniciar sesión">
                            <span>Iniciar sesión</span>
                            </a>-->
                            <a href="/iniciar-sesion/" title="Iniciar sesión">
                                <span>Iniciar sesión</span>
                            </a>
                        </li>
                        <li id="icon3">
                            <a href="/registrate" title="Regístrate"> <span>Regístrate</span>  </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php
        wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'depth'             => 5,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
        );
        ?><!--/.nav-collapse -->
    </div>
</nav>
<nav class="navbar navbar-inverse navbar-static-top visible-xs" role="navigation">
    <div class="container">
        <ul id="menu-principal" class="nav nav-pills collapse navbar-collapse">
            <?php if (isset($_SESSION['idusuario'])): ?>
                <li class="menu-item menu-item-type-custom menu-item-object-custom" style="border-right: 2px solid #db74a6;">
                    <a href="javascript:void(0)" title="Cerrar sesión" onclick="ax.cerrarSesion()">Cerrar sesión</a>
                    <form id="frmCerrar" method="post">
                        <input type="hidden" name="cerrarSesion" value="true">
                    </form>
                </li>
                <li class="menu-item menu-item-object-page">
                    <a title="Actualiza tus datos" href="/actualiza-tus-datos/">Actualiza tus datos</a>
                </li>
            <?php else: ?>
            <!--<li class="menu-item menu-item-type-custom menu-item-object-custom">
            <a href="javascript:void(0)" onclick="$('#mdllogin').modal('show');" title="Iniciar sesión">Iniciar sesión</a>
            </li>-->
            <li class="menu-item menu-item-type-post_type menu-item-object-page" style="border-right: 2px solid #db74a6;">
                <a title="Regístrate comunidad Lights" href="registrate">Registro comunidad Lights</a>
            </li>
            <?php endif; ?>
        </ul>
        <?php
        wp_nav_menu( array(
            'menu'              => 'topmenu',
            'theme_location'    => 'topmenu',
            'depth'             => 5,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'menu-principal',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
        );
        ?><!--/.nav-collapse -->
    </div>
</nav>
<?php if ($post->post_name != 'home'): ?>
    <section class="breadcrumbs">
        <div class="container">
            <?php $pagename = get_query_var('pagename');
            if($pagename != 'lightson')
                if($pagename != 'actualiza-tus-datos')
                    if(function_exists('bavota_breadcrumbs')) bavota_breadcrumbs(); ?>
        </div>
    </section>
    <?php endif; ?>
    <div class="modal fade" id="mdllogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="row">
                <div class="modal-content inicio">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Iniciar sesión</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="email" class="form-control ax-input" id="reg_email_mdl" placeholder="Ingresa tu correo">
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control ax-input" id="reg_numdocumento_mdl" placeholder="Ingresa tu identificación">
                        </div>
                        <div class="form-group">
                            <a href="javascript:void(0);" class="ax-recuperar-pass" onclick="$( '.modal' ).modal('hide');$('#recuperar').modal('show');">Recuperar contraseña</a>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <button id="actualizar" type="button" onclick="ax.updateUserLogin()" class="btn btn-default ax-button">Actualiza tus datos</button>
                                </div>
                                <div class="col-md-6">
                                    <button id="regcontinuar" type="button" onclick="ax.ingresarLogin()" class="btn btn-default ax-button">Ingresa</button>
                                </div>
                            </div>
                        </div>
                        <div id="axmsgregister_mdl"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="recuperar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="row">
                <div class="modal-content inicio">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Recuperar contraseña</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="email" class="form-control ax-input" id="rec_email_mdl" placeholder="Ingresa tu correo">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="regRecuperar" type="button" onclick="ax.recuperarPass()" class="btn btn-default ax-button">Recuperar</button>
                                </div>
                            </div>
                        </div>
                        <div id="axmsgrecuperar_mdl"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ax-back-opa">
        <div class="ax-preload-content">
            <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
        </div>
    </div>