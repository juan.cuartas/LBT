<?php get_header(); ?>
<div class="ax-blog ax-blog-home">
    <div class="container">
        <div class="col-md-12 ax-destacado">
            <?php $the_query = new WP_Query( array(
                'post_type' => 'post',
                'order' => 'DESC',
                'posts_per_page' => 1,
            ));
            if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) { ?>
                <?php $the_query->the_post(); $identificador = $post->ID; ?>

                <article>
                    <?php if (has_post_thumbnail()) {
                        $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id( $post->ID ), '' );
                        ?>
                        <figure>
                            <img src="<?php echo $imagen_cabecera['src']; ?>" alt="<?php echo $imagen_cabecera['alt']; ?>" title="<?php echo $imagen_cabecera['title']; ?>" class="attachment-blog_destacado size-blog_destacado wp-post-image">
                            <!--<?php //the_post_thumbnail('blog_destacado'); ?>-->
                        </figure>
                        <?php } ?>
                        <div class="ax-cont-destacado">
                            <header>
                              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                              <div class="ax-time-date">
                                <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j / F / Y'); ?></time>
                            </div>
                        </header>
                        <div class="ax-content">
                            <?php the_excerpt(); ?>
                        </div>
                        <div>

                            <div class="ax-learm-more">
                                <a href="<?php the_permalink(); ?>">Leer más...</a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php }
            }
            wp_reset_postdata();
            ?>
        </div>
        <div class="col-md-9 ax-blog-articulos paginador-general">
            <?php
            $the_query = new WP_Query( array(
                'post_type' => 'post',
                'post__not_in' => array($identificador),
                'order' => 'DESC',
                'paged' => get_query_var( 'paged' ),
            ) );

            if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) { ?>
                <?php $the_query->the_post(); ?>
                <article>
                    <?php if (has_post_thumbnail()) {
                        $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id( $post->ID ), '' );
                        ?>
                        <figure>
                            <img src="<?php echo $imagen_cabecera['src']; ?>" alt="<?php echo $imagen_cabecera['alt']; ?>" title="<?php echo $imagen_cabecera['title']; ?>" class="attachment-blog_destacado size-blog_destacado wp-post-image">
                            <!--<?php //the_post_thumbnail('blog_destacado'); ?>-->
                        </figure>
                        <?php } ?>
                        <div class="ax-cont-destacado">
                            <header>
                              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                              <div class="ax-time-date">
                                <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j / F / Y'); ?></time>
                            </div>
                        </header>
                        <div class="ax-content">
                            <?php the_excerpt(); ?>
                        </div>
                        <div>
                            <div class="ax-learm-more">
                                <a href="<?php the_permalink(); ?>">Leer más...</a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php }
                    $big = 999999999; // need an unlikely integer

                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );

                }
                wp_reset_postdata();
                ?>
            </div>

            <div class="col-md-3 ax-sidebar-cont">
                <div class="ax-sidebar-comunidad">
                    <?php
                    $args = array(
                        'sort_order' => 'desc',
                        'sort_column' => 'post_date',
                        'hierarchical' => 1,
                        'exclude' => '1097',
                        'include' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'authors' => '',
                        'child_of' => 36,
                        'parent' => -1,
                        'exclude_tree' => '',
                        'number' => '',
                        'offset' => 0,
                        'post_type' => 'page',
                        'post_status' => 'publish'
                    );

                    $comunitypages = get_pages($args);
                    $numpost = count($comunitypages);
                    $randnum = rand(0,$numpost-1);
                    $comunityp = '';

                    foreach ($comunitypages as $key => $page) {

                        if ($key == $randnum) {

                            $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id($page->ID) );
                            $comunityp .= '<div class="ax-comunidad ax-comunidad-blog">';
                            $comunityp .= '<img class="img-responsive center-block" src="'.$imagen_cabecera['src'].'" alt="'.$imagen_cabecera['alt'].'" title="'.$imagen_cabecera['title'].'">
                            </div>
                            <div class="item">
                            <h2>'.$page->post_title.'</h2>
                            <a class="boton" target="_blank" href="'.get_page_link($page->ID).'" target="blank">Conoce más</a>
                            </div>';
                        }
                    }
                    echo $comunityp;

                    ?>
                </div>
                <a href="/solicitar-muestra/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/muestra-gratis.png" alt="enlace muestra gratis" title="Muestra gratis"></a>
            </div>

        </div>
    </div>
    <?php get_footer(); ?>