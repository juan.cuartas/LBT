<?php
/**
 * Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

session_start();

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0, user-scalable=no" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() ?>/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php echo bloginfo('name'); ?></title>
    
	<?php
      
      if (isset($_POST['btnCerrarSesion'])) {
        session_destroy();
        wp_redirect(home_url());
      }
      
	  /*
	   * We add some JavaScript to pages with the comment form
	   * to support sites with threaded comments (when in use).
	   */
	  if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
		
	  /*
	   * Always have wp_head() just before the closing </head>
	   * tag of your theme, or you will break many plugins, which
	   * generally use this hook to add elements to <head> such
	   * as styles, scripts, and meta tags.
	   */
      ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri() ?>/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri() ?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri() ?>/favicon/manifest.json">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/ax-main.media.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/jPages.css">
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jPages.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/tether.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
    <script src="http://malsup.github.io/min/jquery.cycle.all.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/ax-register.js"></script>
    <?php wp_head(); ?>
    <script type=text/javascript>var siteurl = '<?php echo get_site_url(); ?>';</script>
  </head>
  <body <?php body_class(); ?>>
    
    
    <?php $header_image = get_header_image(); ?>
    

    <header class="cabezote">
      <div class="container">
        <div class="row ax-iconosredes">
        <div class="col-md-1 hidden-xs"></div>
          <div class="col-md-8 ax-logo col-sm-12">
            <a href="<?php echo get_site_url(); ?>"><h1>Más vivas que nunca</h1></a>
          </div>
      
          <div class="col-md-3 col-sm-12 hidden-xs">
           <form method="post" action="">

              <a href="https://www.facebook.com/masvivasquenunca" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="https://www.instagram.com/masvivas" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <?php if (!empty($_SESSION['idusuario'])) {
                  echo '<button type="submit" data-toggle="tooltip" name="btnCerrarSesion" data-placement="bottom" title="Cerrar Sesion"><i class="fa fa-user-times" aria-hidden="true"></i> Cerrar sesión</button>';
                } ?>

            </form>
          </div>
          <div class="col-md-3 col-sm-12 visible-xs">
           <form method="post" action="">
                <?php if (!empty($_SESSION['idusuario'])) {
                  echo '<button type="submit" data-toggle="tooltip" name="btnCerrarSesion" data-placement="bottom" title="Cerrar Sesion"><i class="fa fa-user-times" aria-hidden="true"></i> Cerrar sesión</button>';
                } ?>

            </form>
          </div>
        
        </div>
      </div>
    </header>
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only"><?php _e( 'Toggle navigation', 'bootstrapcanvaswp' ); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <h1 class="hidden-lg"><img class="img-responsive" data-src="holder.js/500x500/auto" alt="" src="img/ax-logo.png"></h1>
        </div>
    <?php
          wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
          );
        ?><!--/.nav-collapse -->
      </div>
    </nav>
    