<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

get_header(); ?>


<div class="ax-error404">

    <div class="container">	

		<h2 class="center">ERROR 404</h2>
		<hr class="featurette-divider">       
        <h2> En Lights queremos que conozcas más recomendaciones para tener una vida plena.</h2>
        <p class="center">
        	La página que buscabas no se encuentra disponible o no existe.
        </p>


    </div><!-- /.row -->

</div>

      
<?php get_footer(); ?>