<?php
require('../../../../../wp-blog-header.php');
require_once "nusoap/nusoap.php";
ini_set("soap.wsdl_cache_enabled", 0);
header("Content-type: text/html; charset=iso-8859-1");
$client = new nusoap_client(URL_WEB_SERVICE);

if (isset($_POST['validate'])) {
    global $wpdb;
    $response = array();
    session_start();

    $validate =  $client->call('autenticacion',array('datos' =>
        array(
            'email' => $_POST['validate']['correo'],
            'identificacion' => $_POST['validate']['numdocumento'],
            'idpais' => '1'
        )
    ));

    if ($validate['errno'] == 'VAL-0') {
        $pagea = get_page_by_title('Actualiza tus datos');
        $pageactualizar = get_page_link($pagea->ID);
        // Variables de sesion
        $idusuario = $validate['visitorid'];

        $wprows = $wpdb->get_results("SELECT ciudad, clave_usuario FROM wp_users_tena WHERE id_usuario = '".$idusuario."'");
        $wpdata = (array)$wprows[0];
        $userdata = array('nombre1' => utf8_encode($validate['visitorData']['nombre1']),
        'nombre2' => utf8_encode($validate['visitorData']['nombre2']),
        'apellido1' => utf8_encode($validate['visitorData']['apellido1']),
        'apellido2' => utf8_encode($validate['visitorData']['apellido2']),
        'correo' => utf8_encode($validate['visitorData']['email']),
        'identificacion' => utf8_encode($validate['visitorData']['identificacion']),
        'sexo' => utf8_encode($validate['visitorData']['sexo']),
        'ciudad' => utf8_encode($wpdata['ciudad']),
        'clave' => utf8_encode($wpdata['clave_usuario']));

        $_SESSION['idusuario'] = $idusuario;// A string
        $_SESSION['userdata'] = $userdata;
        $response['response'] = 'update';
        $response['redirect'] = $pageactualizar;
    }
    else if ($validate['errno'] == 'VAL-1' || $validate['errno'] == 'VAL-4') {
        $pager = get_page_by_title('Regístrate');
        $pageregistro = get_page_link($pager->ID);
        $response['response'] = 'register';
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
        $response['redirect'] = $pageregistro;
        $_SESSION['correoreg'] = $_POST['validate']['correo'];
        $_SESSION['numdocreg'] = $_POST['validate']['numdocumento'];
    }
    else if ($validate['errno'] == 'VAL-3' || $validate['errno'] == 'VAL-2') {
        $response['resno'] = 'email';
        $response['msg'] = utf8_encode($validate['errormsg']);
    }
    else {
        $response['resno'] = $validate['errno'];
        $response['msg'] = utf8_encode($validate['errormsg']);
    }

    echo json_encode($response);
}

/**
 * login 16/09/2017
 */
if (isset($_POST['ingresaruser'])) {
    session_start();
    global $wpdb;
    $response = array();

    $isService = validateService();

    if($isService) {
        $validate =  $client->call('autenticacion',
            ['datos' => [
                'email' => strtolower($_POST['ingresaruser']['correo']),
                'identificacion' => $_POST['ingresaruser']['numdocumento'],
                'idpais' => '1']
            ]);
    }else{
        $validate = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_users_tena WHERE numero_documento=%s AND correo=%s ",
         $_POST['ingresaruser']['numdocumento'], strtolower($_POST['ingresaruser']['correo']) ));

        $validate['visitorid'] = $validate[0]->id_usuario;
        $validate['visitorData']['identificacion'] = $validate[0]->numero_documento;
        $validate['visitorData']['nombre1'] = modificarCadena($validate[0]->nombre);
        $validate['visitorData']['apellido1'] = modificarCadena($validate[0]->apellido);
        $validate['visitorData']['email'] = strtolower($validate[0]->correo);
        $validate['errno'] = 'VAL-0';
    }

    switch ( $validate['errno'] ) {
        case 'VAL-0':
            $pageredirect = '';
            iniciarSession($validate);
            $pagerIniciar = get_page_by_path('lightson');
            $pageredirect = get_page_link($pagerIniciar->ID);
            $response['redirect'] = $pageredirect;
            break;
        case 'VAL-1':
            $response = usuarioNoExisteServicio($validate);
            break;
        case 'VAL-2':
            $response['resno'] = $validate['errno'];
            $response['msg'] = utf8_encode($validate['errormsg']);
            break;
        case 'VAL-3':
            $response['resno'] = $validate['errno'];
            $response['msg'] = utf8_encode($validate['errormsg']);
            break;
        case 'VAL-4':
            $response = usuarioNoExisteServicio($validate);
            break;
        case 'VAL-6':
            $response['resno'] = 'VAL-6';
            $response['msg'] = "correo o contraseña incorrectos";
            $response['redirect'] = '';
            break;
        default:
            $response['resno'] = 'VAL-6';
            $response['msg'] = "correo o contraseña incorrectos";
            $response['redirect'] = '';
            //$response['resno'] = $validate['errno'];
            //$response['msg'] = utf8_encode($validate['errormsg']);
            break;
    }

    echo json_encode($response);
}

/**
 *  registro de usuario
 */
if (isset($_POST['datauser'])) {

    $isService = validateService();
    $response = array('res' => '', 'msg' => '', 'visitordata' => array());

    $datos = llenarDatosUsuario();

    $registerVisitor = $client->call('registroVisitor', [ 'UsuarioLightsReq' => $datos ]);

    switch ($registerVisitor['errno']) {
        case 'REG-0':
            $validate = $client->call('autenticacion', ['datos' => ['email' => $_POST['datauser']['correo'],
                'identificacion' => $_POST['datauser']['numdocumento'], 'idpais' => '1']]);
            iniciarSession($validate);
            $pagec = get_page_by_path('lightson');
            $pagesolic = get_page_link($pagec->ID);
            $response['redirect'] = $pagesolic;
            break;
        case 'REG-1':
            $response['res'] = 'datos';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        case 'VAL-2':
            $response['res'] = 'email';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        case 'VAL-3':
            $response['res'] = 'identificacion';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        case 'REG-4':
            $response['res'] = 'errordb';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        case 'REG-5':
            $response['res'] = 'errordb';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        case 'REG-6':
            $response['res'] = 'userexist';
            $response['msg'] = 'Ya haces parte de la comunidad Más Vivas. Te invitamos a que actualices tus datos. Recuerda que tu usuario es tu correo y tu contraseña tu número de cédula.';
            $pagec = get_page_by_path('iniciar-sesion');
            $pagesolic = get_page_link($pagec->ID);
            $response['redirect'] = $pagesolic;
            break;
        case 'REG-7':
            $response['res'] = 'ismen';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        case 'REG-8':
            $response['res'] = 'is35';
            $response['msg'] = utf8_encode($registerVisitor['errormsg']);
            break;
        default:
            $response['res'] = 'errorgeneral';
            $response['msg'] = $registerVisitor;
            break;
    }

    echo json_encode($response);
}

/**
 * suscripción
 * envia email y utiliza servicio de pragma
 */
if (isset($_POST['newsletter'])) {
    session_start();
    $response = array();

    if (empty($_SESSION['idusuario'])) {
        $pager = get_page_by_title('Regístrate');
        $pageregistro = get_page_link($pager->ID);
        $response['pageregister'] = $pageregistro;
        $response['sesion'] = true;
    } else {
        $newsletter = $client->call('suscribirNews',[ 'datos' => [ 'email' => $_POST['newsletter']['correo'],
                'pais' => '1' ]
        ]);
        if ($newsletter['errormsg'] == 'suscrito correctamente') {
            $archivo_html = file_get_contents('../../mailing/news/index.html');
            $archivo_html = str_replace('{$url_site}', get_site_url(), $archivo_html);
            $archivo_html = str_replace('{$url_mail}', get_site_url()."/wp-content/themes/lights/mailing/news/", $archivo_html);
            $archivo_html = str_replace('{$nombre}', $_SESSION['userdata']['nombre1'], $archivo_html);
            $para      = $_POST['newsletter']['correo'];
            $titulo    = 'Suscripción - Lights by TENA';
            $body = $archivo_html;
            $headers[] = "Content-Type: text/html; charset=UTF-8".' ' . "\r\n";
            $headers[]   = 'From: multienlace@familia.com.co <multienlace@familia.com.co>';
            $enviar= wp_mail( $para, $titulo, $body, $headers);
            $response['registered'] = true;
        } else if ($newsletter['errormsg'] == 'ya se encuenta suscrito') {
            $response['alreadyexist'] = true;
        } else {
            $response = $newsletter;
        }
    }

    echo json_encode($response);
}

/**
 * actualiza informacion del usuario
 */
if (isset($_POST['datauserupd'])) {
    $response = actualizarUsuarioTena();
    echo json_encode($response);
}

/**
 * devuelve informacion del usuario
 */
if (isset($_POST['getuserinfo'])) {
    $identificacion = $_POST['getuserinfo']['numdocumento'];
    $email = $_POST['getuserinfo']['correo'];
    $validate = consultarInformacionUsuarioTena($identificacion, $email);
    echo json_encode($validate);
}

/**
 * solicitar muestra de producto
 */
if (isset($_POST['registerprod'])) {
    session_start();
    $response = array();
    $response = solicitarMuestraProducto();
    echo json_encode($response);
}

//no se esta utilizando 29/09/2017
if (isset($_POST['datauserteq'])) {
    global $wpdb;
    $response = array('res' => '', 'msg' => '', 'visitordata' => array());


    /*$interest =  $client->call('getIntereses');
    $objtemas = explode(',', $_POST['datauserteq']['temas']);
    $intereses = array();

    foreach ($interest as $int) {
        foreach ($objtemas as $temasel) {
            if ($temasel == $int['categoria']) {
                if ($int['categoria'] == 'Otros') {
                    $intereses[] = new soapval('long', 'xsd:long', (int)'137');
                } else {
                    foreach ($int['interesData'] as $intdata) {
                        $totaldata = explode('|',$intdata);
                        $intereses[] = new soapval('long', 'xsd:long', (int)$totaldata[0]);
                    }
                }
            }
        }
    }

    $registerVisitor = $client->call('registroVisitor', array('UsuarioLightsReq' => array('nombre1' => utf8_decode($_POST['datauserteq']['nombre']),
      'nombre2' => utf8_decode($_POST['datauserteq']['segnombre']),
      'apellido1' => utf8_decode($_POST['datauserteq']['apellido']),
      'apellido2' => utf8_decode($_POST['datauserteq']['segapellido']),
      'tipoidentificacion' => $_POST['datauserteq']['tipodocumento'],
      'identificacion' => $_POST['datauserteq']['numdocumento'],
      'fechanacimiento' => $_POST['datauserteq']['anon'].$_POST['datauserteq']['mesn'].$_POST['datauserteq']['dian'],
      'sexo' => $_POST['datauserteq']['genero'],
      'idpais' => $_POST['datauserteq']['pais'],
      'iddepartamento' => $_POST['datauserteq']['departamento'],
      'idciudad' => $_POST['datauserteq']['ciudad'],
      'direccion' => $_POST['datauserteq']['direccion'],
      'barrio' => utf8_decode($_POST['datauserteq']['barrio']),
      'email' => $_POST['datauserteq']['correo'],
      'celular' => $_POST['datauserteq']['celular'],
      'interesesData' => $intereses,
      'contactoresidencia' => $_POST['datauserteq']['contactoresidencia'],
      'contactoemail' => $_POST['datauserteq']['contactoemail'],
      'terminosYcondiciones' => $_POST['datauserteq']['terminos'],
      'politicaPrivacidad' => $_POST['datauserteq']['politica'],
      'expectativa' => $_POST['datauserteq']['expectativa'])));

    */

    /*if ($registerVisitor['errno'] == 'REG-0') {

        $validate =  $client->call('autenticacion',array('datos' => array('email' => $_POST['datauserteq']['correo'],
                                                                      'identificacion' => $_POST['datauserteq']['numdocumento'],
                                                                      'idpais' => '1')));
        $userdata_t = array(
            'id_usuario' => '000',
            'nombre'  =>  $_POST['datauserteq']['nombre'].' '.$_POST['datauserteq']['segnombre'],
            'apellido' =>  $_POST['datauserteq']['apellido'].' '.$_POST['datauserteq']['segapellido'],
            'tipo_documento' => $_POST['datauserteq']['tipodocumento'],
            'numero_documento' => $_POST['datauserteq']['numdocumento'],
            'fecha_nacimiento' => $_POST['datauserteq']['anon'].'-'.$_POST['datauserteq']['mesn'].'-'.$_POST['datauserteq']['dian'],
            'genero' => $_POST['datauserteq']['genero'],
            'pais' => $_POST['datauserteq']['namepais'],
            'departamento' => $_POST['datauserteq']['namedepartamento'],
            'ciudad' => $_POST['datauserteq']['nameciudad'],
            'direccion' => $_POST['datauserteq']['direccion'],
            'correo' => $_POST['datauserteq']['correo'],
            'celular' => $_POST['datauserteq']['celular'],
            'temasinteres' => $_POST['datauserteq']['temas'],
            'contactoresidencia' => $_POST['datauserteq']['contactoresidencia'],
            'contactoemail' => $_POST['datauserteq']['contactoemail'],
            'terminos' => $_POST['datauserteq']['terminos'],
            'politica' => $_POST['datauserteq']['politica'],
            'expectativa' => $_POST['datauserteq']['expectativa'],
            'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
            'createduser' => current_time('mysql'),
            'updateuser' => current_time('mysql'),
            'acceptvideo' => $_POST['datauserteq']['acceptvideo']
         );

        $usert = $wpdb->insert('wp_users_tena', $userdata_t);

        if (!is_wp_error($usert)) {
            $response['res'] = 'success';
        } else {
            $response['res'] = 'errorlocal';
            $response['msg'] = $usert->get_error_message();
        }
    } */

    if ($_POST['datauserteq']['genero'] == '1') {
        $response['res'] = 'ismen';
        $response['msg'] = 'Esta es una comunidad exclusiva para mujeres.';
    } else if ($_POST['datauserteq']['anon'] >= 1982) {
        $response['res'] = 'is35';
        $response['msg'] = 'Lo sentimos, este sitio es para mujeres mayores de 35 años, por ese motivo aún no puedes hacer parte de nuestra comunidad, esperamos contar contigo más adelante.';
    } else {
        $userdata_t = array(
            'id_usuario' => '000',
            'nombre'  =>  $_POST['datauserteq']['nombre'].' '.$_POST['datauserteq']['segnombre'],
            'apellido' =>  $_POST['datauserteq']['apellido'].' '.$_POST['datauserteq']['segapellido'],
            'tipo_documento' => $_POST['datauserteq']['tipodocumento'],
            'numero_documento' => $_POST['datauserteq']['numdocumento'],
            'fecha_nacimiento' => $_POST['datauserteq']['anon'].'-'.$_POST['datauserteq']['mesn'].'-'.$_POST['datauserteq']['dian'],
            'genero' => $_POST['datauserteq']['genero'],
            'pais' => $_POST['datauserteq']['namepais'],
            'departamento' => $_POST['datauserteq']['namedepartamento'],
            'ciudad' => $_POST['datauserteq']['nameciudad'],
            'direccion' => $_POST['datauserteq']['direccion'],
            'correo' => $_POST['datauserteq']['correo'],
            'celular' => $_POST['datauserteq']['celular'],
            'temasinteres' => $_POST['datauserteq']['temas'],
            'contactoresidencia' => $_POST['datauserteq']['contactoresidencia'],
            'contactoemail' => $_POST['datauserteq']['contactoemail'],
            'terminos' => $_POST['datauserteq']['terminos'],
            'politica' => $_POST['datauserteq']['politica'],
            'expectativa' => $_POST['datauserteq']['expectativa'],
            'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
            'createduser' => current_time('mysql'),
            'updateuser' => current_time('mysql'),
            'acceptvideo' => $_POST['datauserteq']['acceptvideo']
         );

        $usert = $wpdb->insert('wp_users_tena', $userdata_t);

        if (!is_wp_error($usert)) {
            $response['res'] = 'success';
            $response['msg'] = 'Tu registro con la marca ha sido exitoso. </br>Recuerda que tu usuario es tu correo y tu contraseña tu cédula.';
        } else {
            $response['res'] = 'errorlocal';
            $response['msg'] = 'Al parecer ocurrió un error, por favor inténtelo más tarde.';
        }
    }

    echo json_encode($response);
}

/**
 * carga comentarios
 */
if (isset($_GET['allcomments'])) {
    $html = cargarTodosComentarios();
    echo json_encode(array("html" => (string)$html));
}

/**
 * retorna todo los comentarios dependiendo del id del post
 */
if (isset($_GET['allcommentsExpectativa'])) {
    $html = cargarComentariosExpectativa();
    echo json_encode(array("html" => (string)$html));
}

/**
 * registro de comentarios y subcomentarios
 */
if (isset($_POST['content'])) {

    session_start();
    require_once('../../../../../wp-admin/includes/image.php' );
    require_once('../../../../../wp-admin/includes/file.php' );
    require_once( '../../../../../wp-admin/includes/media.php' );

    if (isset($_SESSION['idusuario'])) {

        if($_POST['parent'] =='') {
            $p='_p';
        } else {
            $p='';
        }

        $attachment_id = media_handle_upload( 'my_image_upload_comment'.$p.'',$_SESSION['idusuario']);
        $consulta_post = "SELECT * FROM wp_posts WHERE post_parent=".$_SESSION['idusuario']." ";
        $resultado_post = $wpdb->get_results( $consulta_post );
        $url_post = '';

        if (!is_wp_error($attachment_id)) {
            foreach ($resultado_post as $key => $value) {
                $url_post = wp_get_attachment_url($value->ID);
            }
        }

        $name = $_SESSION['userdata']['nombre1'] . ' ' . $_SESSION['userdata']['apellido1'];
        $email = $_SESSION['userdata']['email'];
        $post_id = $_POST['page_type'];
        $content = $_POST['content'];
        $parentid = $_POST['parent'] ? $_POST['parent'] : 0;

        $datos= [
            'id_usuario' => $_SESSION['idusuario'],
            'url_archivo' => $url_post,
            'descripcion' => $_POST["descripcion_reto"]
        ];

        $time = current_time('mysql');

        $data = [
            'comment_post_ID' => $post_id ,
            'comment_author' => $name,
            'comment_author_email' => $email,
            'comment_author_url' => $url_post ,
            'comment_content' => $content,
            'comment_type' => '',
            'comment_parent' => $parentid,
            'user_id' => $_SESSION['idusuario'],
            'comment_author_IP' => '127.0.0.1',
            'comment_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
            'comment_date' => $time,
            'comment_approved' => 1,
        ];

        if (!empty($name)) {
            $commentid = wp_insert_comment($data);
            $response['commentid'] = $commentid;
            $response['msg'] = 'Tu comentario ha sido enviado con &eacute;xito.';
        } else {
            $pagea = get_page_by_title('Actualiza tus datos');
            $pageactua = get_page_link($pagea->ID);
            $response['nombreuser'] = true;
            $response['msg'] = 'Por favor <a href="'.$pageactua.'">actualiza tus datos</a> para comentar.';
        }

    } else {
        $_SESSION['comment'] = $_POST['content'];
        $_SESSION['inputcomment'] = $_POST['inputcomment'];
        $_SESSION['pagecomment'] = $_POST['pagecomment'];
        $_SESSION['messageimage'] = $_POST['messageimage'];
        $_SESSION['imagecomment'] = $_POST['imagecomment'];
        $pager = get_page_by_title('Regístrate');
        $pageregistro = get_page_link($pager->ID);
        $response['pageregister'] = $pageregistro;
        $response['msg'] = 'sesion';
    }
    echo json_encode($response);
}

/**
 * envio de email para recuperacion de contraseña
 */
if (isset($_POST['forgotPassword'])) {
    global $wpdb;
    $wprows = $wpdb->get_results("SELECT NOMBRE1, IDENTIFICACION FROM tenalightsArkix WHERE EMAIL = '".$_POST['mail']."'");
    if(!empty($wprows)){
        $wpdata = (array)$wprows[0];


        $archivo_html = file_get_contents('../../mailing/recuperar_pass/index.html');
        $archivo_html = str_replace('{$url_site}', get_site_url(), $archivo_html);
        $archivo_html = str_replace('{$url_mail}', get_site_url()."/wp-content/themes/lights/mailing/recuperar_pass/", $archivo_html);
        $archivo_html = str_replace('%email%', $_POST['mail'], $archivo_html);
        $archivo_html = str_replace('%pass%', $wpdata['IDENTIFICACION'], $archivo_html);
        $archivo_html = str_replace('%Nombre%', $wpdata['NOMBRE1'], $archivo_html);
        $para      = $_POST['mail'];
        $titulo    = 'Recuperar contraseña - Lights by TENA';
        $body = $archivo_html;
        //die(print_r($body));
        $headers[] = "Content-Type: text/html; charset=UTF-8".' ' . "\r\n";
        $headers[]   = 'From: multienlace@familia.com.co <multienlace@familia.com.co>';
        $enviar= wp_mail( $para, $titulo, $body, $headers);
        $response['error'] = false;
    } else {
        $response['error'] = true;
    }
    echo json_encode($response);
}

if (isset($_POST['idcountry'])) {
    $depart = $client->call('getDepartamentos', array('idPais' => (int)$_POST['idcountry']));
    $deparm = array();

    foreach ($depart as $dep) {
        array_push($deparm, array('id' => $dep['iddepartamento'], 'departamento' => utf8_encode($dep['nombre'])));
    }

    echo json_encode($deparm);
}

if (isset($_POST['iddepart'])) {
    $city = $client->call('getCiudades', array('idDepartamento' => (int)$_POST['iddepart']));
    $cities = array();

    foreach ($city as $citi) {
        array_push($cities, array('id' => $citi['idciudad'], 'ciudad' => utf8_encode($citi['nombre'])));
    }

    echo json_encode($cities);
}
// no se esta utilizando 15/09/2017
if (isset($_POST['tipodoc'])) {
    $tdoc =  $client->call('getTiposDocumentos', array('idPais' => 1));
    $tiposdoc = array();

    foreach ($tdoc as $td) {
        array_push($tiposdoc, array('id' => $td['idtipo'], 'documento' => utf8_encode($td['nombre'])));
    }

    echo json_encode($tiposdoc);
}

//no se esta utilizando 29/09/2017
if (isset($_POST['updatedatauser'])) {
    session_start();
    $response = array();

    $validate = $wpdb->get_results(
        $wpdb->prepare("SELECT * FROM tenalightsArkix WHERE IDENTIFICACION=%s AND EMAIL=%s ",
    $_POST['updatedatauser']['numdocumento'], $_POST['updatedatauser']['correo']));

    $validate['idarkix'] = $validate[0]->IDARKIX;
    $validate['visitorid'] = $validate[0]->VISITORID;
    $validate['visitorData']['IDENTIFICACION'] = $validate[0]->IDENTIFICACION;
    $validate['visitorData']['NOMBRE1'] = $validate[0]->NOMBRE1;
    $validate['visitorData']['APELLIDO1'] = $validate[0]->APELLIDO1;
    $validate['visitorData']['EMAIL'] = $validate[0]->EMAIL;
    unset($validate[0]);

    if (!empty($validate['visitorData']['IDENTIFICACION'])) {
        $pageredirect = '';
        iniciarSession($validate);

        $pagea = get_page_by_title('Actualiza tus datos');
        $pageredirect = get_page_link($pagea->ID);
        $response['redirect'] = $pageredirect;
    }else{
        $response['resno'] = 'VAL-6';
        $response['msg'] = "correo o contraseña incorrectos";
        $pager = get_page_by_path('registrate');
        $pageredirect = get_page_link($pager->ID);
        $response['redirect'] = $pageredirect;
    }
    echo json_encode($response);
}

if (isset($_GET['intereses'])) {
    $interest =  $client->call('getIntereses');
    $intereses = array();
    foreach ($interest as $int) {
        array_push($intereses, array('name' => utf8_encode($int['categoria'])));
    }
    echo json_encode($intereses);
}

if(isset($_GET['getProducts'])){
    $response = $client->call('getProductos',array());
    $products = array();
    foreach ($response as $res) {
        array_push($products, array('name' => utf8_encode($res['nombre']),'id' => $res['idproducto']));
    }
    echo json_encode($products);
}

/**
 * crea las variables session para registro y login
 * crea una variable de inicio de sesion para
 * compararla en el header que no exceda las 2 horas
 * @param  [type] $datos [description]
 * @return [type]        [description]
 */
function iniciarSession($datos) {

    session_start();
    $now = time();
    $_SESSION['inicio-sesion'] = $now + 7200;
    $idusuario = $datos['visitorid'];
    $_SESSION['idusuario'] = $idusuario;
    $_SESSION['visitorid'] = $datos['visitorid'];
    $_SESSION['userdata'] = $datos['visitorData'];
}

/**
 * el usuario no existe en pragma, se retorna a la pagina de registro
 * @return array [description]
 */
function usuarioNoExisteServicio($validate) {

    $_SESSION['correoreg'] = $_POST['ingresaruser']['correo'];
    $_SESSION['numdocreg'] = $_POST['ingresaruser']['numdocumento'];
    $response['resno'] = $validate['errno'];
    $response['msg'] = utf8_encode($validate['errormsg']);
    $pager = get_page_by_path('registrate');
    $pageredirect = get_page_link($pager->ID);
    $response['redirect'] = $pageredirect;
    return $response;
}

/**
 * retorna array con la información del usuario
 * @param  [type] $client    instancia del servicio
 * @param  [type] $wpdb       wordpress
 * @param  [type] $productos contiene el id de los productos escogidos por el usuario
 * @return array            respuesta con mensaje
 */
function llenarDatosUsuario(){
    return [

        'tipoidentificacion' => $_POST['datauser']['tipodocumento'],
        'identificacion' => $_POST['datauser']['numdocumento'],
        'apellido1' => modificarCadena($_POST['datauser']['apellido']),
        'apellido2' => modificarCadena($_POST['datauser']['segapellido']),
        'nombre1' => modificarCadena($_POST['datauser']['nombre']),
        'nombre2' => modificarCadena($_POST['datauser']['segnombre']),
        'direccion' => $_POST['datauser']['direccion'],
        'barrio' => modificarCadena($_POST['datauser']['barrio']),
        'email' => strtolower($_POST['datauser']['correo']),
        'celular' => $_POST['datauser']['celular'],
        'fechanacimiento' => date('Ymd',strtotime(str_replace('/', '-',$_POST['datauser']['fechan']))),
        'contactoresidencia' => $_POST['datauser']['contactoresidencia'],
        'contactoemail' => $_POST['datauser']['contactoemail'],
        'idpais' => $_POST['datauser']['pais'],
        'idciudad' => $_POST['datauser']['ciudad'],
        'iddepartamento' => $_POST['datauser']['departamento'],
        'sexo' => $_POST['datauser']['genero'],
        'expectativa'=> $_POST['datauser']['expectativa'],
        'terminosYcondiciones' => $_POST['datauser']['terminos'],
        'politicaPrivacidad' => $_POST['datauser']['politica'],
        'esusuario' => $_POST['datauser']['esusuario'],
    ];
}

/**
 * actualiza la información del usuario
 * @return array                respuesta con mensaje
 */
function actualizarUsuarioTena() {

    header("Content-type: text/html; charset=utf8");
    session_start();
    global $wpdb;
    $client = new nusoap_client(URL_WEB_SERVICE);
    $response = array('res' => '', 'msg' => '', 'visitordata' => array());

    $interest =  $client->call('getIntereses');
    $intereses = array();
    $str_temas = str_replace('MÃƒÂºsica y ocio', 'Música y ocio', $_POST['datauserupd']['temas']);
    $objtemas = explode(',', $str_temas);

    foreach ($interest as $int) {
        foreach ($objtemas as $temasel) {
            if ($temasel == utf8_encode($int['categoria'])) {
                if ($int['categoria'] == 'Otros') {
                    $intereses[] = new soapval('long', 'xsd:long', (int)'137');
                } else {
                    foreach ($int['interesData'] as $intdata) {
                        $totaldata = explode('|',$intdata);
                        $intereses[] = new soapval('long', 'xsd:long', (int)$totaldata[0]);
                    }
                }
            }
        }
    }

    $objproductos = explode(',', $_POST['datauserupd']['productos']);
    $productos = array();
    if(!empty($objproductos) && count($objproductos) > 1){
        foreach($objproductos as $prod){
            $productos[] = new soapval('long', 'xsd:long', (int)$prod);
        }
    }

    $nuevoUsuario = [
        'tipoidentificacion' => $_POST['datauserupd']['tipodocumento'],
        'identificacion' => $_POST['datauserupd']['numdocumento'],
        'apellido1' => modificarCadena($_POST['datauserupd']['apellido']),
        'apellido2' => modificarCadena($_POST['datauserupd']['segapellido']),
        'nombre1' => modificarCadena($_POST['datauserupd']['nombre']),
        'nombre2' => modificarCadena($_POST['datauserupd']['segnombre']),
        'direccion' => $_POST['datauserupd']['direccion'],
        'barrio' => modificarCadena($_POST['datauserupd']['barrio']),
        'email' => strtolower($_POST['datauserupd']['correo']),
        'celular' => $_POST['datauserupd']['celular'],
        'fechanacimiento' => date('Ymd',strtotime(str_replace('/', '-',$_POST['datauserupd']['fechan']))),
        'contactoresidencia' => $_POST['datauserupd']['contactoresidencia'],
        'contactoemail' => $_POST['datauserupd']['contactoemail'],
        'idpais' => $_POST['datauserupd']['pais'],
        'idciudad' => $_POST['datauserupd']['ciudad'],
        'iddepartamento' => $_POST['datauserupd']['departamento'],
        'sexo' => $_POST['datauserupd']['genero'],
        'terminosYcondiciones' => $_POST['datauserupd']['terminos'],
        'politicaPrivacidad' => $_POST['datauserupd']['politica'],
        'esusuario' => $_POST['datauserupd']['esusuario'],
        'visitorid' => new soapval('visitorid','long', (int) $_SESSION['idusuario'],false,false,false),
        'interesesData' => $intereses
    ];

    $updateVisitor = $client->call('actualizarVisitor', ['UsuarioLightsReq' => $nuevoUsuario ]);

    switch ($updateVisitor['errno']) {
        case 'UPD-0':
            $response = actualizarUsuarioTenaDB($productos);
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-1':
            $response['res'] = 'datos';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-2':
            $response['res'] = 'email';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-3':
            $response['res'] = 'identificacion';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-4':
            $response['res'] = 'errordb';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-5':
            $response['res'] = 'errordb';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-6':
            $response['res'] = 'userexist';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-7':
            $response['res'] = 'ismen';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        case 'UPD-8':
            $response['res'] = 'is35';
            $response['msg'] = utf8_encode($updateVisitor['errormsg']);
            break;
        default:
            $response['res'] = 'errorgeneral';
            $response['msg'] = $updateVisitor;
            break;
    }

    return $response;
}

/**
 * guarda los productos en el servicio de pragma,
 * agrega o modifica el usuario en la base de datos
 * si este no existe
 * @return array respuesta exitosa o erronea del procedimiento
 */
function actualizarUsuarioTenaDB($productos) {
    global $wpdb;
    $client = new nusoap_client(URL_WEB_SERVICE);
    $response = [];
    $validate = $client->call('autenticacion',['datos' => [
        'email' => $_POST['datauserupd']['correo'],
        'identificacion' => $_POST['datauserupd']['numdocumento'],
        'idpais' => '1']
    ]);

    $registerProduct = $client->call('ProductosxUsuario',['productosData' => [
        'productosid' => $productos,
        'visitorid' =>  new soapval('visitorid', 'long', (int)$validate['visitorid'],false,false,false)
    ]]);

    $userdata_t = [
        'id_usuario' => $validate['visitorid'],
        'nombre'  =>  $_POST['datauserupd']['nombre'].' '.$_POST['datauserupd']['segnombre'],
        'apellido' =>  $_POST['datauserupd']['apellido'].' '.$_POST['datauserupd']['segapellido'],
        'numero_documento' => $_POST['datauserupd']['numdocumento'],
        'fecha_nacimiento' => date('Y-m-d',strtotime ( str_replace('/', '-',$_POST['datauserupd']['fechan']))),
        'genero' => $_POST['datauserupd']['genero'],
        'pais' => $_POST['datauserupd']['namepais'],
        'departamento' => $_POST['datauserupd']['namedepartamento'],
        'ciudad' => $_POST['datauserupd']['nameciudad'],
        'direccion' => $_POST['datauserupd']['direccion'],
        'correo' => $_POST['datauserupd']['correo'],
        'celular' => $_POST['datauserupd']['celular'],
        'temasinteres' => $_POST['datauserupd']['temas'],
        'productos' => $_POST['datauserupd']['productos'],
        'contactoresidencia' => $_POST['datauserupd']['contactoresidencia'],
        'contactoemail' => $_POST['datauserupd']['contactoemail'],
        'terminos' => $_POST['datauserupd']['terminos'],
        'politica' => $_POST['datauserupd']['politica'],
        'esusuario' => $_POST['datauserupd']['esusuario'],
        'clave_usuario' => bin2hex(openssl_random_pseudo_bytes(20)),
        'updateuser' => current_time('mysql')
    ];

    $consultu = $wpdb->get_results(
        $wpdb->prepare("SELECT id_usuario FROM wp_users_tena WHERE id_usuario %s", $_SESSION['idusuario']));

    if (!empty($consultu)) {
        $usert = $wpdb->update('wp_users_tena', $userdata_t, array('id_usuario' => $_SESSION['idusuario']));

        $datauser['visitorid'] = $validate['visitorid'];
        $datauser['visitorData']['nombre1'] = modificarCadena($validate['visitorData']['nombre1']);
        $datauser['visitorData']['apellido1'] = modificarCadena($validate['visitorData']['apellido1']);
        $datauser['visitorData']['email'] = strtolower($validate['visitorData']['email']);
        $datauser['visitorData']['identificacion'] = $validate['visitorData']['identificacion'];
        iniciarSession($datauser);
    }else{
        $userdata_t['expectativa'] = 'N';
        $userdata_t['createduser'] = current_time('mysql');
        $usert = $wpdb->insert('wp_users_tena', $userdata_t);
        $datauser['visitorid'] = $validate['visitorid'];
        $datauser['visitorData']['nombre1'] = modificarCadena($validate['visitorData']['nombre1']);
        $datauser['visitorData']['apellido1'] = modificarCadena($validate['visitorData']['apellido1']);
        $datauser['visitorData']['email'] =strtolower($validate['visitorData']['email']);
        $datauser['visitorData']['identificacion'] = $validate['visitorData']['identificacion'];
        iniciarSession($datauser);
    }

    if (!is_wp_error($usert)) {
        if (isset($_SESSION['idprodmuestra'])) {
            $pagec = get_page_by_path('solicitar-muestra');
            $pagesolic = get_page_link($pagec->ID);
            $response['redirect'] = $pagesolic;
            unset($_SESSION['medioscontacto']);
        } else if (isset($_SESSION['pagecomment'])) {
                $response['redirect'] = $_SESSION['pagecomment'];
                unset($_SESSION['medioscontacto']);
        } else {
            $pagec = get_page_by_path('lightson');
            $pagesolic = get_page_link($pagec->ID);
            $response['redirect'] = $pagesolic;
            unset($_SESSION['medioscontacto']);
        }
    } else {
        $response['res'] = 'errorlocal';
        $response['msg'] = $usert->get_error_message();
    }

    return $response;
}

/**
 * retorna la información del usuario registrado
 * @param  [type] $identificacion [description]
 * @param  [type] $email          [description]
 * @return [type]                 [description]
 */
function consultarInformacionUsuarioTena($identificacion, $email) {
    global $wpdb;
    $client = new nusoap_client(URL_WEB_SERVICE);
    $response = array();
    session_start();

    $data = $client->call('autenticacion',['datos' => [
        'email' => strtolower($email),
        'identificacion' => $identificacion,
        'idpais' => '1']
    ]);

    //$validate['idarkix'] = $resultado[0]->IDARKIX;
    $validate['visitorid'] = $data['visitorid'];
    $validate['datauser']['tipoidentificacion'] = $data['visitorData']['tipoidentificacion'];
    $validate['datauser']['identificacion'] = $data['visitorData']['identificacion'];
    $validate['datauser']['nombre1'] = modificarCadena($data['visitorData']['nombre1']);
    $validate['datauser']['nombre2'] = modificarCadena($data['visitorData']['nombre2']);
    $validate['datauser']['apellido1'] = modificarCadena($data['visitorData']['apellido1']);
    $validate['datauser']['apellido2'] = modificarCadena($data['visitorData']['apellido2']);
    $validate['datauser']['direccion'] = $data['visitorData']['direccion'];
    $validate['datauser']['barrio'] = modificarCadena($data['visitorData']['barrio']);
    $validate['datauser']['correo'] = strtolower($data['visitorData']['email']);
    $validate['datauser']['celular'] = $data['visitorData']['celular'];
    $validate['datauser']['fechanacimiento'] = date('d/m/Y',strtotime(str_replace('-', '/',$data['visitorData']['fechanacimiento'])));
    $validate['datauser']['contactoresidencia'] = $data['visitorData']['contactoresidencia'];
    $validate['datauser']['contactoemail'] = $data['visitorData']['contactoemail'];
    $validate['datauser']['idpais'] = $data['visitorData']['idpais'];
    $validate['datauser']['iddepartamento'] = $data['visitorData']['iddepartamento'];
    $validate['datauser']['idciudad'] = $data['visitorData']['idciudad'];
    $validate['datauser']['idsexo'] = $data['visitorData']['sexo'];
    $validate['datauser']['terminosycondiciones'] = $data['visitorData']['terminosYcondiciones'];
    $validate['datauser']['politicaprivacidad'] = $data['visitorData']['politicaPrivacidad'];
    $validate['datauser']['esusuario'] = $data['visitorData']['esusuario'];
    $validate['datauser']['aceptaingreso'] = $data['visitorData']['aceptaIngreso'];

    $interest = $client->call('getIntereses');
    $allintereses = array();

    foreach ($interest as $allinte) {
        if(!empty($data['interesesData'])){
            foreach ($data['interesesData'] as $interdata) {
                foreach ($allinte['interesData'] as $getdata) {
                    if ($getdata == $interdata) {
                    if (!in_array($allinte['categoria'], $allintereses)) {
                        $allintereses[] = utf8_encode($allinte['categoria']);
                    }
                    }
                }
            }
        }
        if ($allinte['interesData'] == $data['interesesData']) {
            $allintereses[] = utf8_encode($allinte['categoria']);
        }
    }
    /*foreach ($response as $res) {
        array_push($products, array('name' => utf8_encode($res['nombre']),'id' => $res['idproducto']));
    }*/
    //echo json_encode($products);

    //productosData





    /*$interesesData = $wpdb->get_results(
        $wpdb->prepare("SELECT * FROM wp_intereses_usuario WHERE IDARKIX=%s", $validate['idarkix'])
    );
    $intereses = $wpdb->get_results("SELECT id, CONVERT(categoria USING utf8) as categoria FROM wp_intereses GROUP BY categoria");
    $allintereses = array();
    //intereses
    foreach ($interesesData as $allinte) {
        if(!empty($intereses)){
            foreach ($intereses as $interdata) {
                if ($interdata->id == $allinte->INTERESID) {
                   if (!in_array($interdata->categoria, $allintereses)) {
                        $allintereses[] = $interdata->categoria;
                    }
                }
            }
        }
    }

    $productosData = $wpdb->get_results(
        $wpdb->prepare("SELECT * FROM wp_producto_usuario WHERE IDARKIX=%s", $validate['idarkix'])
    );  */

    $validate['productosData'] = $data['productosData'];
    $validate['temasinteres'] = $allintereses;
    unset($data);
    return $validate;
}

/**
 * [enviarCorreoRegistro description]
 * @param  [type] $email [description]
 * @return [type]        [description]
 */
function enviarCorreoRegistro($email) {
    global $wpdb;
    $wprows = $wpdb->get_results($wpdb->prepare("SELECT NOMBRE1, APELLIDO1, IDENTIFICACION FROM tenalightsArkix WHERE EMAIL=%s ", $email));
    if(!empty($wprows)){
        $wpdata = (array)$wprows[0];

        $archivo_html = file_get_contents('../../mailing/Registro/registro.html');
        $archivo_html = str_replace('{$url_site}', get_site_url(), $archivo_html);
        $archivo_html = str_replace('{$url_mail}', get_site_url()."/wp-content/themes/lights/mailing/Registro/", $archivo_html);
        $archivo_html = str_replace('%Nombre%', $wpdata['NOMBRE1'].' '.$wpdata['APELLIDO1'], $archivo_html);
        $para      = $email;
        $titulo    = 'Bienvenida - Lights by TENA';
        $body = $archivo_html;
        //die(print_r($body));
        $headers[] = "Content-Type: text/html; charset=UTF-8".' ' . "\r\n";
        $headers[]   = 'From: multienlace@familia.com.co <multienlace@familia.com.co>';
        $enviar= wp_mail( $para, $titulo, $body, $headers);
        $response['error'] = false;
    } else {
        $response['error'] = true;
    }
    return $response;
}

/**
 * [time_elapsed_string description]
 * @param  [type]  $datetime [description]
 * @param  boolean $full     [description]
 * @return [type]            [description]
 */
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'año',
        'm' => 'mes',
        'w' => 'semana',
        'd' => 'día',
        'h' => 'hora',
        'i' => 'minuto',
        's' => 'segundo',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? 'Hace ' . implode(', ', $string)  : 'Justo ahora';
}

/**
 * [time_elapsed_string_expectativa description]
 * @param  [type]  $datetime [description]
 * @param  boolean $full     [description]
 * @return [type]            [description]
 */
function time_elapsed_string_expectativa($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'año',
        'm' => 'mes',
        'w' => 'sem',
        'd' => 'día',
        'h' => 'hora',
        'i' => 'min',
        's' => 'seg',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string)  : 'Justo ahora';
}

/**
 *  valida si el servicio esta en funcionamiento
 * @return [type] [description]
 */
function validateService(){
    $rutaArchivo = get_template_directory() .'/inc/soap/errorLog.txt';
    $headers = get_headers(URL_WEB_SERVICE);
    $return = true;
    if(!strstr($headers[0],'200')){
        $logFile = fopen($rutaArchivo,'a+');
        fwrite($logFile,date('d-m-Y H:i:s'). '  Response = ' .print_r($headers,TRUE).PHP_EOL);
        fclose($logFile);
        $return = false;
    }
    return $return;
}

function modificarCadena($cadena) {
    return ucfirst(strtolower($cadena));
}

function cargarComentariosExpectativa() {
    session_start();
    $post_id = $_GET['allcommentsExpectativa']['idpost'];
    $comments = get_comments([
        'post_id' => $post_id,
        'status' => 'approve',
        'parent' => 0
    ]);

    $html = '';
    foreach($comments as $comment){

        $subcomments = get_comments(array('post_id' => $post_id, 'status' => 'approve', 'parent' => $comment->comment_ID, 'order' => 'ASC'));

        $html .= '<div class="ax-userbg" id="ax-userbg'.$comment->comment_ID.'"><input id="thepostid" type="hidden" value="'.$post_id.'"/><div class="ax-user"> <p id="p_subirsubcomentarios'.$comment->comment_ID.'"><span class="ax-autor">'.$comment->comment_author.'</span> &nbsp;'.$comment->comment_content.'</p>';
            $html .= '<p>'.time_elapsed_string_expectativa($comment->comment_date_gmt).'<a href="javascript:void(0)" class="ax-responder" id="ax-responder'.$comment->comment_ID.'" data-id="'.$comment->comment_ID.'">Responder</a></p>';
            if ($comment->comment_author_url != '') {
                $html .= '<img class="img-responsive center-block" alt="500x500" src="'.$comment->comment_author_url.'">';
            }
          $html .= '</div><div class="row"><div class="ax-compartir"><div class="ax-icomen col-xs-3"><i class="fa fa-commenting" aria-hidden="true"></i><span>'.count($subcomments).'</span></div><div class="col-md-8 col-xs-9"><a href="javascript:void(0)" onclick="ax.shareFacebook('."'".$comment->comment_author."'".','."'".$comment->comment_content."'".','."'".$comment->comment_author_url."'".')"><i class="fa fa-facebook" aria-hidden="true"></i><span>Compartir</span></a></div></div></div><div class="row ax-subcomment-wrapper" id="ax-subcomment-wrapper'.$comment->comment_ID.'">';

          foreach($subcomments as $subcomment){
          $html .= '<div class="ax-textcomentario"><div class="ax-nombreusuario"><p><span>'.$subcomment->comment_author.'</span> '.$subcomment->comment_content.'</p><div class="ax-usuarioimagen">';
                if ($subcomment->comment_author_url != '') {
                    $html .= '<img class="img-responsive center-block" alt="500x500" src="'.$subcomment->comment_author_url.'">';
                }
              $html .= '</div></div></div>';
          }

          $html .= '<div class="ax-subircomentarios col-md-9"><textarea id="reg_subirsubcomentarios'.$comment->comment_ID.'" type="text" name="reg_subirsubcomentarios" parentid="'.$comment->comment_ID.'" class="ax-input form-control ax-expectativa" placeholder="Escribe tu comentario" required/></textarea> <span id="spanb'.$comment->comment_ID.'" class="btn btn-default btn-file"><i class="fa fa-camera" aria-hidden="true"></i><input type="file" name="my_image_upload_comment" id="my_image_upload_comment'.$comment->comment_ID.'" accept="image/x-png, image/gif, image/jpeg" multiple="false"/></span><div id="msgsubcomment'.$comment->comment_ID.'"></div></div><div class="col-md-3"><button id="reg-submit" class="ax-button btn btn-default" onclick="ax.submitSubComment('."'".'#reg_subirsubcomentarios'.$comment->comment_ID."'".', $('."'".'#my_image_upload_comment'.$comment->comment_ID."'".'),'.$comment->comment_ID.')"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></div></div></div> <script type="text/javascript"> $("#my_image_upload_comment'.$comment->comment_ID.'").change(function () { if (this.value != "") { var size = (this.files[0].size / (1024*1024)).toFixed(2); if (size > 2) { $("#msgsubcomment'.$comment->comment_ID.'").empty(); $("#msgsubcomment'.$comment->comment_ID.'").append("<p>No se admiten imágenes con un peso superior a 2mb</p>"); this.value = ""; $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-camera"); $("#spanb'.$comment->comment_ID.' i.fa").css("color","#fff"); $("#spanb'.$comment->comment_ID.'").css("background-color","#ad004b"); $("#msgsubcomment'.$comment->comment_ID.'").empty(); } else { $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-camera"); $("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-check"); $("#spanb'.$comment->comment_ID.' i.fa").css("color","#fff"); $("#spanb'.$comment->comment_ID.'").css("background-color","#ad004b"); $("#msgsubcomment'.$comment->comment_ID.'").empty(); } } else { $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-check"); $("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-camera"); $("#spanb'.$comment->comment_ID.' i.fa").css("color","#b2b2b2"); $("#spanb'.$comment->comment_ID.'").css("background-color","#fff"); } });';
            $currcomment = "#reg_subirsubcomentarios".$comment->comment_ID;

            if ($_SESSION['inputcomment'] == $currcomment) {
                $html .= '$("'.$_SESSION['inputcomment'].'").val("'.$_SESSION['comment'].'");';
            }

            if (isset($_SESSION['messageimage']) && !empty($_SESSION['imagecomment'])) {
                $html .= '$("'.$_SESSION['messageimage'].'").append("<p>Por favor selecciona de nuevo la imagen que elegiste previamente.</p>");';
                unset($_SESSION['messageimage']);
                unset($_SESSION['imagecomment']);
            }

          $html .= /*'$("#reg_subirsubcomentarios'.$comment->comment_ID.'").emojioneArea();*/'</script>';
    }
    $html .= '<script>$(".ax-responder").click(function(){ $("#ax-subcomment-wrapper"+$(this).attr("data-id")).slideToggle(); }); </script>';

    if (isset($_SESSION['comment'])) {
        unset($_SESSION['comment']);
        unset($_SESSION['inputcomment']);
    }

    if (isset($_SESSION['messageimage']) && !empty($_SESSION['imagecomment'])) {
        unset($_SESSION['messageimage']);
        unset($_SESSION['imagecomment']);
    }
    return $html;
}

function cargarTodosComentarios() {
    session_start();
    $post_id = $_GET['allcomments']['idpost'];
    $comments = get_comments(array('post_id' => $post_id, 'status' => 'approve', 'parent' => 0 ));
    $html = '';
    foreach($comments as $comment){

        $subcomments = get_comments(array('post_id' => $post_id, 'status' => 'approve', 'parent' => $comment->comment_ID, 'order' => 'ASC'));

        $html .= '<div class="ax-userbg">
        <input id="thepostid" type="hidden" value="'.$post_id.'"/>
        <div class="ax-user">
            <h4>'.$comment->comment_author.'</h4>
            <span>· '.time_elapsed_string($comment->comment_date_gmt).'</span>';

            if (isset($_SESSION['idusuario'])) {
                /*if ($comment->user_id == $_SESSION['idusuario']) {
                    $html .= '<p>'.$comment->comment_content.'<a onclick="editComment()"><i class="fa fa-pencil"></i>Editar</a></p>';
                } else {
                    $html .= '<p>'.$comment->comment_content.'</p>';
                }*/
                $html .= '<p id="p_subirsubcomentarios'.$comment->comment_ID.'">'.$comment->comment_content.'</p>';
            } else {
                $html .= '<p id="p_subirsubcomentarios'.$comment->comment_ID.'">'.$comment->comment_content.'</p>';
            }
            if ($comment->comment_author_url != '') {
             $html .= '<img class="img-responsive center-block" alt="500x500" src="'.$comment->comment_author_url.'">';
            }
          $html .= '</div><div class="row"><div class="ax-compartir"><div class="ax-icomen col-xs-4"><div class="row"><i class="fa fa-commenting" aria-hidden="true"></i><span>'.count($subcomments).'</span></div></div><div class="col-md-8 col-xs-8"><i class="fa fa-facebook" aria-hidden="true"></i><a href="javascript:void(0)" onclick="ax.shareFacebook('."'".$comment->comment_author."'".','."'".$comment->comment_content."'".','."'".$comment->comment_author_url."'".')"><span>Compartir</span></a></div></div></div>';

          foreach($subcomments as $subcomment){
          $html .= '<div class="ax-textcomentario"><div class="ax-nombreusuario"><p><span>'.$subcomment->comment_author.'</span> '.$subcomment->comment_content.'</p><div class="ax-usuarioimagen">';
                if ($subcomment->comment_author_url != '') {
                    $html .= '<img class="img-responsive center-block" alt="500x500" src="'.$subcomment->comment_author_url.'">';
                }
              $html .= '</div>
            </div>
          </div>';
          }
          $html .= '<div class="ax-subircomentarios"><textarea id="reg_subirsubcomentarios'.$comment->comment_ID.'" type="text" name="reg_subirsubcomentarios" parentid="'.$comment->comment_ID.'" class="ax-input form-control" placeholder="Escribe tu comentario" required/></textarea><span id="spanb'.$comment->comment_ID.'" class="btn btn-default btn-file"><i class="fa fa-camera" aria-hidden="true"></i><input type="file" name="my_image_upload_comment" id="my_image_upload_comment'.$comment->comment_ID.'" accept="image/x-png, image/gif, image/jpeg" multiple="false"  /></span>
             <input type="button" class="ax-button btn btn-default" onclick="ax.submitSubComment('."'".'#reg_subirsubcomentarios'.$comment->comment_ID."'".', $('."'".'#my_image_upload_comment'.$comment->comment_ID."'".'),'.$comment->comment_ID.')" value="Comentar" />
             <div id="msgsubcomment'.$comment->comment_ID.'"></div></div></div>
          <script type="text/javascript">$("#my_image_upload_comment'.$comment->comment_ID.'").change(function () {
                    if (this.value != "") { var size = (this.files[0].size / (1024*1024)).toFixed(2); if (size > 2) {
                            $("#msgsubcomment'.$comment->comment_ID.'").empty();$("#msgsubcomment'.$comment->comment_ID.'").append("<p>No se admiten imágenes con un peso superior a 2mb</p>");
                            this.value = "";$("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-camera");$("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-check");$("#spanb'.$comment->comment_ID.' i.fa").css("color","#fff");$("#spanb'.$comment->comment_ID.'").css("background-color","#ad004b");$("#msgsubcomment'.$comment->comment_ID.'").empty();} else { $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-camera");$("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-check");$("#spanb'.$comment->comment_ID.' i.fa").css("color","#fff");$("#spanb'.$comment->comment_ID.'").css("background-color","#ad004b");$("#msgsubcomment'.$comment->comment_ID.'").empty();}} else { $("#spanb'.$comment->comment_ID.' i.fa").removeClass("fa-check");$("#spanb'.$comment->comment_ID.' i.fa").addClass("fa-camera");$("#spanb'.$comment->comment_ID.' i.fa").css("color","#b2b2b2");$("#spanb'.$comment->comment_ID.'").css("background-color","#fff");}});';
            $currcomment = "#reg_subirsubcomentarios".$comment->comment_ID;

            if ($_SESSION['inputcomment'] == $currcomment) {
                $html .= '$("'.$_SESSION['inputcomment'].'").val("'.$_SESSION['comment'].'");';
            }

            if (isset($_SESSION['messageimage']) && !empty($_SESSION['imagecomment'])) {
                $html .= '$("'.$_SESSION['messageimage'].'").append("<p>Por favor selecciona de nuevo la imagen que elegiste previamente.</p>");';
                unset($_SESSION['messageimage']);
                unset($_SESSION['imagecomment']);
            }

          $html .= /*'$("#reg_subirsubcomentarios'.$comment->comment_ID.'").emojioneArea();*/'</script>';
    }

    if (isset($_SESSION['comment'])) {
        unset($_SESSION['comment']);
        unset($_SESSION['inputcomment']);
    }

    if (isset($_SESSION['messageimage']) && !empty($_SESSION['imagecomment'])) {
        unset($_SESSION['messageimage']);
        unset($_SESSION['imagecomment']);
    }
    return $html;
}

function solicitarMuestraProducto() {
    $response = [];
    if (isset($_SESSION['idusuario'])) {

        if (isset($_POST['registerprod']['idprod'])) {

            $registro = false;
            $response['msg'] = '';
            $rproducts = array();

            foreach ($_POST['registerprod']['idprod'] as $key => $prod) {

                $registro_muestra = $client->call('registroMuestra',
                    [
                        'muestra'=>[
                            'visitorid' => (int) $_SESSION['idusuario'],
                            'idproducto' => (int) $prod
                        ]
                    ]);

                if ($registro_muestra['errno']) {
                    if (empty($rproducts['case'.$registro_muestra['errno']])) {
                        $rproducts['case'.$registro_muestra['errno']] = 0;
                    }

                    $lineaatencion = '018000524848';

                    switch ($registro_muestra['errno']) {
                        case 301:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tienes una muestra en estado pendiente por entrega. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.', productos pendientes: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 305:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'El sistema encuentra duplicidad en el requerimiento de la muestra o el usuario se encuentra desactivado. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.'. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 306:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tu muestra ha sido reprogramada. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.'. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 307:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No tenemos tu dirección de residencia. Ingresa y actualiza tus datos en nuestro sitio. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 388:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Estamos depurando la información. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.'. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 310:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No puedes solicitar varias veces la misma muestra, si deseas elige otro producto. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.'. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 312:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Completa la ciudad donde vives. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 313:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Lo sentimos, en este momento no tenemos inventario. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 314:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'La muestra que te enviamos fue devuelta porque tu dirección está errada. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 315:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'la muestra que te enviamos fue devuelta porque en tu dirección no había nadie. Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 316:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tu dirección está incompleta. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 317:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'la muestra que te enviamos fue devuelta porque tu domicilio cambió. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 318:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Lo sentimos, no tenemos cubrimiento en esta zona. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 319:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'La muestra que te enviamos fue devuelta porque tu dirección no existe. Ingresa y actualiza tus datos en nuestro sitio. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 321:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tu usuario no está activo. Ingresa y suscríbete de nuevo en nuestro sitio. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 322:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tu nombre está incompleto. Ingresa y actualiza tus datos en nuestro sitio. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 323:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No pudimos comunicarnos contigo. Por favor ponte en contacto con nosotros en la Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 325:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'La muestra que te enviamos fue devuelta porque tu dirección está en una zona de alto riesgo. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 326:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Faltan tus apellidos. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 329:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tu dirección está incompleta. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 331:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'El envío de la muestra ha sido cancelado porque no actualizaste tus datos a tiempo. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 337:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No pudimos comunicarnos contigo. Por favor ponte en contacto con nosotros en la Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 338:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Tu cédula está incorrecta o incompleta. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 341:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No has ingresado tu teléfono. Ingresa y actualiza tus datos en nuestro sitio Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 342:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No pudimos comunicarnos contigo. Por favor ponte en contacto con nosotros en la Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 344:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No pudimos comunicarnos contigo. Por favor ponte en contacto con nosotros en la Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 345:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No pudimos comunicarnos contigo. Por favor ponte en contacto con nosotros en la Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 347:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'En el momento tienes devoluciones reportadas, tu muestra no puede ser enviada. Para más información comunícate con nuestra Línea de atención al cliente '.$lineaatencion.' Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 400:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'El usuario no fue encontrado. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 401:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'No existe una solución de muestra para el producto seleccionado. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 402:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Datos vacíos o incompletos Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 403:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Ocurrió un error al tratar de registrar la muestra. Productos solicitados: '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        case 404:
                            $rproducts['case'.$registro_muestra['errno']] = $rproducts['case'.$registro_muestra['errno']] + 1;
                            $response['msg'] = 'Solicitud registrada, productos solicitados '.$rproducts['case'.$registro_muestra['errno']].'<br><br>';
                            $registro = true;
                            $response['code'] = $registro_muestra['errno'];
                            break;
                        default:
                            $response['msg'] = $registro_muestra['errormsg'];
                            $response['code'] = $registro_muestra['errno'];
                            break;
                    }
                } else {
                    $response['errser'] = $registro_muestra;
                    $response['msg'] = "Error en el servicio";
                }
            }

            if ($registro) {

                $archivo_html = file_get_contents('../../mailing/suscripcion/index.html');
                $archivo_html = str_replace('{$url_mail}', get_site_url()."/wp-content/themes/lights/mailing/suscripcion/", $archivo_html);
                $para      = $_SESSION['userdata']['EMAIL'];
                $titulo    = 'Solicitud de muestra';
                $body = $archivo_html;
                $headers[] = "Content-Type: text/html; charset=UTF-8".' ' . "\r\n";
                $headers[]   = 'From: multienlace@familia.com.co <multienlace@familia.com.co>';
                $enviar= wp_mail( $para, $titulo, $body, $headers);
            }
        }
    } else {
        $_SESSION['idprodmuestra'] = $_POST['registerprod']['idprod'];
        $response['res'] = 'sesion';
    }
    return $response;
}
