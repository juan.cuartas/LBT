<?php
require('../../../../../wp-blog-header.php');
global $wpdb;
header("Content-type: text/html; charset=iso-8859-1");
/**
 * Servicio para las consultas de los departamentos y las ciudades
 */
if ($_GET['tipo'] == 'departamentos') {
	$departamentos = $wpdb->get_results("SELECT * FROM wp_departamentos ORDER BY nombre");
	die(json_encode($departamentos));
}
/**
 * retorna la s ciudades de dicho departamento
 */
if (!empty($_GET['departamento'])) {
	$ciudades =  $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_ciudades WHERE departamento_id=%d ORDER BY nombre", $_GET['departamento']));
	if (count($ciudades) > 0) {
		die(json_encode($ciudades));
	}else{
		$ciudades = [];
		die(json_encode($ciudades));
	}

}
/**
 * retorna los productos registrados en la base de datos
 */
if ($_GET['tipo'] == 'productos') {
	$productos = $wpdb->get_results("SELECT * FROM wp_productos ORDER BY nombre");
	die(json_encode($productos));
}

if ($_GET['tipo'] == 'intereses') {
	$intereses = $wpdb->get_results("SELECT * FROM wp_intereses GROUP BY categoria");
	die(json_encode($intereses));
}