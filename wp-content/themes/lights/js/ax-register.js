ax = {
    episodios: [],
    getMobileOperatingSystem: function() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }
        if (/android/i.test(userAgent)) {
            return "Android";
        }
        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }
        return "unknown";
    },
    validateInfo: function() {
        $('.registration').bind('click', function() {
            $('.ax-form-rp').empty();
            $('.ax-bgfooter').empty();
            if($(window).width() >= 768){
                $('.ax-bgfooter').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificaci&oacute;n"><button id="regcontinuar" type="button" onclick="ax.validateUser()" class="btn btn-default ax-button">Continuar</button></form><div id="axmsgregister"></div></div></div>').animate({opacity: 1},500);
            }else {
                $('.ax-form-rp').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificaci&oacute;n"><button id="regcontinuar" type="button" onclick="ax.validateUser()" class="btn btn-default ax-button">Continuar</button></form><div id="axmsgregister"></div></div></div>').animate({opacity: 1},500);
            }
        });
        $('.userLogin').bind('click', function() {
            $('.ax-form-rp').empty();
            $('.ax-bgfooter').empty();
            if($(window).width() >= 768){
                $('.ax-bgfooter').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificaci&oacute;n"><button id="regcontinuar" type="button" onclick="ax.ingresar()" class="btn btn-default ax-button">Continuar</button><button id="actualizar" type="button" onclick="ax.updateUserData()" class="btn btn-default ax-button">Actualizar datos</button></form><div id="axmsgregister"></div></div></div>').animate({opacity: 1},500);
            }else {
                $('.ax-form-rp').css('opacity','0').append('<div class="blog-footer ax-bgfooter" style="background-color: rgba(245, 0, 106, 0.25);"><div class="container"><div id="axmessager"></div><form class="form-inline" role="form"><input type="email" class="form-control ax-input" id="reg_email" placeholder="Ingresa tu correo"><input type="number" class="form-control ax-input" id="reg_numdocumento" placeholder="Ingresa tu identificaci&oacute;n"><button id="regcontinuar" type="button" onclick="ax.ingresar()" class="btn btn-default ax-button">Continuar</button><button id="actualizar" type="button" onclick="ax.validateUser()" class="btn btn-default ax-button">Actualizar datos</button></form><div id="axmsgregister"></div></div></div>').animate({opacity: 1},500);
            }
        });
    },
    ingresar: function (muestra) {
        $("#pixelsf").empty();
        $("#pixelsf2").empty();
        $("#pixelsf").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 10/21/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsf2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script> <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>');

        if (typeof(muestra)==='undefined') muestra = false;
        var numerodocumento = $('#reg_numdocumento_mdl').val();
        var correo = $('#reg_email_mdl').val();
        var mesg = '';
        $('#axmsgregister_mdl').empty();
        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }
        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }
        if (mesg != '') {
            $('#axmsgregister_mdl').empty();
            $('#axmsgregister_mdl').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento};

            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data:{ ingresaruser: data },
                beforeSend: function () {
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData) {
                    $('#axmessager').empty();
                    $('#reg_email_mdl').val('');
                    $('#reg_numdocumento_mdl').val('');
                    $("#msghome").empty();
                    $("#redirecthome").empty();
                    if (jsonData.resno != null) {

                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            if (muestra) {
                                $('#axmsgregister_mdl').append('<p>El correo o el n&uacute;mero de identificaci&oacute;n ya se encuentran registrados.</p>');
                            } else {
                                $("#msghome").append(jsonData.msg);
                                $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                                $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                                $("#mdlhome").modal("show");
                            }
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        if (muestra) {
                            $('#mdlsolicitar').modal('hide');
                        } else {
                            //window.location.href = jsonData.redirect;
                        }
                    } else {
                        //console.log(jsonData);
                        $('#axmsgregister_mdl').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    ingresarLogin: function (el) {
        $("#pixelsf").empty();
        $("#pixelsf2").empty();
        $("#pixelsf").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 10/21/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsf2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script> <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>');
        if($(el).attr('id') == "regcontinuar_ligthson"){
            var doc = $('#reg_numdocumento_ligthson');
            var correo = $('#reg_email_ligthson');
            var mesg = '';
            var register_div = $('#axmsgregister_ligthson');
            register_div.empty();
            var isLigthsOn = "yes";
        }else{
            var doc = $('#reg_numdocumento_mdl');
            var correo = $('#reg_email_mdl');
            var mesg = '';
            var register_div = $('#axmsgregister_mdl');
            register_div.empty();
            var isLigthsOn = "no";
        }
        if (!ax.validateEmail(correo.val().trim())) {
            mesg += 'Escribe un correo v&aacute;lido</br>';
        }
        if (doc.val().trim() == '') {
            mesg += 'Escribe tu identificaci&oacute;n</br>';
        }
        if (mesg != '') {
            register_div.empty();
            register_div.append('<p>'+mesg+'</p>');
            return;
        } else {
            var data = {correo: correo.val(), numdocumento: doc.val()};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { ingresaruser: data, lightson: isLigthsOn },
                success: function(jsonData) {
                    //console.log(jsonData);
                    if (jsonData.resno != null) {

                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            correo.val('');
                            doc.val('');
                            register_div.append('<p>El correo o el n&uacute;mero de identificaci&oacute;n ya se encuentran registrados.</p>');
                        }
                        else if (jsonData.resno == 'VAL-3') {
                            register_div.append('<p>'+jsonData.msg+'. Te invitamos a que actualices tus datos.</p>');
                        }
                        else {
                            //correo.val('');
                            doc.val('');
                            register_div.append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        //console.log(jsonData);
                        register_div.append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    ingresarM: function (muestra) {
        $("#pixelsf").empty();
        $("#pixelsf2").empty();
        $("#pixelsf").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 10/21/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsf2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script> <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>');

        if (typeof(muestra)==='undefined') muestra = false;
        var numerodocumento = $('#reg_numdocumento').val();
        var correo = $('#reg_email').val();
        var mesg = '';
        $('#axmsgregister').empty();
        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }
        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }
        if (mesg != '') {
            $('#axmsgregister').empty();
            $('#axmsgregister').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { ingresaruser: data },
                beforeSend: function () {
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData) {
                    $('#axmessager').empty();
                    $('#reg_email').val('');
                    $('#reg_numdocumento').val('');
                    $("#msghome").empty();
                    $("#redirecthome").empty();
                    //console.log(jsonData);
                    if (jsonData.resno != null) {

                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            if (muestra) {
                                $('#axmsgregister').append('<p>El correo o el n&uacute;mero de identificaci&oacute;n ya se encuentran registrados.</p>');
                            } else {
                                $("#msghome").append(jsonData.msg);
                                $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                                $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                                $("#mdlhome").modal("show");
                            }
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        if (muestra) {
                            window.location.reload();
                        } else {
                            window.location.href = jsonData.redirect;
                        }
                    } else {
                        //console.log(jsonData);
                        $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    loginMuestra: function (muestra) {
        if (typeof(muestra)==='undefined') muestra = false;
        var numerodocumento = $('#reg_numdocumento_muestra').val();
        var correo = $('#reg_email_muestra').val();
        var mesg = '';
        $('#axmsgregister_muestra').empty();
        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }
        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }
        if (mesg != '') {
            $('#axmsgregister_muestra').empty();
            $('#axmsgregister_muestra').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { ingresaruser: data },
                success: function(jsonData) {
                    $('#axmessager').empty();
                    $('#reg_email').val('');
                    $('#reg_numdocumento').val('');
                    $("#msghome").empty();
                    $("#redirecthome").empty();

                    console.log(jsonData);
                    if (jsonData.resno != null) {

                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            if (muestra) {
                                $('#axmsgregister_muestra').append('<p>El correo o el n&uacute;mero de identificaci&oacute;n ya se encuentran registrados.</p>');
                            } else {
                                $("#msghome").append(jsonData.msg);
                                $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                                $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                                $("#mdlhome").modal("show");
                            }
                        } else {
                            $('#axmsgregister_muestra').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        if (muestra) {
                            window.location.reload();
                        } else {
                            window.location.href = jsonData.redirect;
                        }
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister_muestra').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    validateUser: function () {
        var numerodocumento = $('#reg_numdocumento').val();
        var correo = $('#reg_email').val();
        var mesg = '';
        $('#axmsgregister').empty();

        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }

        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }

        if (mesg != '') {
            $('#axmessager').empty();
            $('#axmessager').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { validate: data },
                beforeSend: function () {
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData) {
                    $('#axmessager').empty();
                    $('#axmsgregister').empty();
                    $('#reg_email').val();
                    $('#reg_numdocumento').val();
                    $("#msghome").empty();
                    $("#redirecthome").empty();

                    if (jsonData.resno != null) {

                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            $("#msghome").append(jsonData.msg);
                            $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                            $("#mdlhome").modal("show");
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    updateUserData: function (muestra) {
        $("#pixelsf").empty();
        $("#pixelsf2").empty();
        $("#pixelsf").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 10/21/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsf2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script> <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>');

        if (typeof(muestra)==='undefined') muestra = false;
        var numerodocumento = $('#reg_numdocumento').val();
        var correo = $('#reg_email').val();
        var mesg = '';
        $('#axmsgregister').empty();

        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }

        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }

        if (mesg != '') {
            $('#axmsgregister').empty();
            $('#axmsgregister').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento, home: true};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { updatedatauser: data },
                beforeSend: function () {
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData) {
                    $('#axmessager').empty();
                    $('#reg_email').val();
                    $('#reg_numdocumento').val();
                    $("#msghome").empty();
                    $("#redirecthome").empty();

                    if (jsonData.resno != null) {
                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            if (muestra) {
                                window.location.href = jsonData.redirect;
                            } else {
                                $("#msghome").append(jsonData.msg);
                                $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                                $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                                $("#mdlhome").modal("show");
                            }
                        } else {
                            $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    updateUserLogin: function (muestra,el) {
        $("#pixelsf").empty();
        $("#pixelsf2").empty();
        $("#pixelsf").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 10/21/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsf2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script> <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>');

        if (typeof(muestra)==='undefined') muestra = false;
        if($(el).attr('id') == "regactualizar_ligthson"){
            var doc = $('#reg_numdocumento_ligthson');
            var correo = $('#reg_email_ligthson');
            var mesg = '';
            var register_div = $('#axmsgregister_ligthson');
            register_div.empty();
        }else{
            var doc = $('#reg_numdocumento_mdl');
            var correo = $('#reg_email_mdl');
            var mesg = '';
            var register_div = $('#axmsgregister_mdl');
            register_div.empty();
        }

        if (!ax.validateEmail(correo.val().trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }

        if (doc.val().trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }

        if (mesg != '') {
            register_div.empty();
            register_div.append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo.val(), numdocumento: doc.val(), home: true};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { updatedatauser: data },
                beforeSend: function () {
                    $('#axmessager').empty();
                    $('#axmessager').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
                },
                success: function(jsonData) {
                    $('#axmessager').empty();
                    $("#msghome").empty();
                    $("#redirecthome").empty();

                    if (jsonData.resno != null) {
                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            if (muestra) {
                                window.location.href = jsonData.redirect;
                            } else {
                                $("#msghome").append(jsonData.msg);
                                $("#redirecthome").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                                $("#redirecthome").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                                $("#mdlhome").modal("show");
                            }
                        } else {
                            $('#axmsgregister_mdl').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister_mdl').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    updateUserDataM: function (muestra) {
        $("#pixelsf").empty();
        $("#pixelsf2").empty();
        $("#pixelsf").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 10/21/2016 --> <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsf2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script> <noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>');

        if (typeof(muestra)==='undefined') muestra = false;
        var numerodocumento = $('#reg_numdocumento_muestra').val();
        var correo = $('#reg_email_muestra').val();
        var mesg = '';
        $('#axmsgregister_muestra').empty();

        if (!ax.validateEmail(correo.trim())) {
            mesg += 'Ingresa un correo v&aacute;lido</br>';
        }

        if (numerodocumento.trim() == '') {
            mesg += 'Ingresa tu identificaci&oacute;n</br>';
        }

        if (mesg != '') {
            $('#axmsgregister_muestra').empty();
            $('#axmsgregister_muestra').append('<p>'+mesg+'</p>');
        } else {
            var data = {correo: correo, numdocumento: numerodocumento, home: true};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: { updatedatauser: data },
                success: function(jsonData) {

                    $('#reg_email_muestra').val();
                    $('#reg_numdocumento_muestra').val();
                    if (jsonData.resno != null) {

                        if (jsonData.resno == 'VAL-1' || jsonData.resno == 'VAL-4') {
                            if (muestra) {
                                window.location.href = jsonData.redirect;
                            }
                        } else {
                            $('#axmsgregister_muestra').append('<p>'+jsonData.msg+'</p>');
                        }
                    } else if (jsonData.redirect != '') {
                        window.location.href = jsonData.redirect;
                    } else {
                        console.log(jsonData);
                        $('#axmsgregister_muestra').append('<p>'+jsonData.msg+'</p>');
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    validateEmail: function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    register: function() {
        $("#pixelsr").empty();
        $("#pixelsr2").empty();
        $("#pixelsr").append('<!--Start of DoubleClick Floodlight Tag: Please do not remove Activity name of this tag: CO_Tena_MasVivas_Home_BotonRegistrate URL of the webpage where the tag is expected to be placed: http://www.masvivas.com/ This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag. Creation Date: 11/08/2016 -->  <script type="text/javascript"> var axel = Math.random() + ""; var a = axel * 10000000000000; $("#pixelsr2").append('+"'"+'<iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te002;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord='+"'"+' + a + '+"'"+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>'+"'"+');</script><noscript><iframe src="https://5979606.fls.doubleclick.net/activityi;src=5979606;type=regis0;cat=co_te002;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript><!-- End of DoubleClick Floodlight Tag: Please do not remove -->');

        var nombre = $('#reg_nombre').val();
        var segnombre = $('#reg_segnombre').val();
        var apellido = $('#reg_apellido').val();
        var segapellido = $('#reg_segapellido').val();
        var tipodocumento = $('#reg_tipo_documento').val();
        var numerodocumento = $('#reg_num_documento').val();
        var fecha = $('#reg_fecha').val();
        var genero = $('#reg_sexo').val();
        var pais = $('#reg_pais').val();
        var departamento = $('#reg_departamento').val();
        var ciudad = $('#reg_ciudad').val();
        var direccion = $('#reg_direccion').val();
        var correo = $('#reg_correo').val();
        var celular = $('#reg_celular').val();
        var barrio = $('#reg_barrio').val();
        var esusuaria = $("input[name=user_radio]").val();
        var selectedtema = false;
        var urlvid = '';
        var contactoresidencia = $('#reg_med_fis:checked').val();
        var contactoemail = $('#reg_med_vir:checked').val();
        var terminos = $('#reg_terms:checked').val();
        var politica = $('#reg_policy:checked').val();
        var expectativa = $('#reg_expectativa').val();

        $('.ax-checkbox.ax-temas').each(function() {
            if (this.checked) {
               selectedtema = true;
            }
        });

        if ($("#reg-submit-teq").length > 0) {
            urlvid = $("#reg_url_vid").val();
        }
        /*if (!selectedtema) {
            $(".ax-check-group").addClass("has-error").append('<div class="ax-registro-user">Por favor selecciona al menos 1 inter&eacute;s</div>');
        }else{
            $(".ax-check-group").find('.ax-registro-user').remove();
        }*/

        if (!terminos) {
            $(".ax-terminoscond").addClass("has-error").append('<span class="ax-registro-user">Por favor acepta los T&eacute;rminos y condiciones</span>');
        }else{
            $(".ax-terminoscond").find('.ax-registro-user').remove();
        }

        if (!politica) {
            $(".ax-terminoscond").addClass("has-error").append('<span class="ax-registro-user">Por favor acepta la Pol&iacute;tica de privacidad</span>');
        }else{
            $(".ax-terminoscond").find('.ax-registro-user').remove();
        }

        if (!politica || !terminos) {
            return;
        } else {
            var temas = '';
            var productos = '';
            var conprodutos = 0;
            var contemas = 0;
            var contacresi = 'S';
            var contavirt = 'S';
            var terms = 'N';
            var policy = 'N';
            var accvideo = 'N';
            var nameciudad = $('#reg_ciudad option[value='+$('#reg_ciudad').val()+']').text();
            var namedepar = $('#reg_departamento option[value='+$('#reg_departamento').val()+']').text();
            var namepais = $('#reg_pais option[value='+$('#reg_pais').val()+']').text();

            $('.ax-checkbox.ax-productos').each(function() {
                if (this.checked) {
                    if (conprodutos == 0) {
                        productos += this.value;
                    } else {
                            productos += ','+this.value;
                    }
                    conprodutos++;
                }
            });
            $('.ax-checkbox.ax-temas').each(function() {
                if (this.checked) {
                    if (contemas == 0) {
                        temas += this.value;
                    } else {
                        temas += ','+this.value;
                    }
                    contemas++;
                }
            });
            if (contactoresidencia) {
                contacresi = 'N';
            }
            if (contactoemail) {
                contavirt = 'N';
            }
            if (terminos) {
                terms = 'S';
            }
            if (politica) {
                policy = 'S';
            }
            var data = null;

            if ($("#reg-submit-teq").length > 0) {
                data = {
                    datauserteq: {
                        nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                        tipodocumento: tipodocumento, numdocumento: numerodocumento, fechan: fecha, genero: genero,
                        pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio, correo: correo,
                        celular: celular, temas: temas,urlvid: urlvid, contactoresidencia: contacresi,
                        contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa
                    }
                };
            } else {
                data = {
                    datauser: {
                        nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                        tipodocumento: tipodocumento, numdocumento: numerodocumento, fechan: fecha, genero: genero,
                        pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio,
                        correo: correo, celular: celular, temas: temas, contactoresidencia: contacresi,
                        contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa,productos: productos, esusuario: esusuaria
                    }
                };
            }

            $("#msgregister").empty();
            $("#redirectregistro").empty();

            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: data,
                success: function(jsonData) {

                    if (jsonData.redirect) {
                        if (jsonData.res == 'errordb') {
                            $("#msgregister").append("Error al registrar el usuario, por favor int&eacute;ntelo m&aacute;s tarde.");
                        } else {
                            $("#msgregister").append(jsonData.msg);
                        }

                        $("#redirectregistro").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                        $("#mdlregistro").modal("show");
                    } else {
                        if (jsonData.res != '') {
                            if (jsonData.res == 'errordb') {
                                $("#msgregister").append("Error al registrar el usuario, por favor int&eacute;ntelo m&aacute;s tarde.");
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            }
                            else if (jsonData.res == 'is35') {
                                $("#msgregister").append('Lo sentimos, este sitio es para mujeres mayores de 35 a&ntilde;os, por ese motivo a&uacute;n no puedes hacer parte de nuestra comunidad, esperamos contar contigo m&aacute;s adelante.');
                                $("#redirectregistro").append('<a class="btn btn-default" href="'+siteurl+'">Cerrar</a>');
                            }
                            else if (jsonData.res == 'ismen') {
                                $("#msgregister").append(jsonData.msg);
                                $("#redirectregistro").append('<a class="btn btn-default" href="'+siteurl+'">Cerrar</a>');
                            }
                            else if (jsonData.res == 'userexist') {
                                $("#msgregister").append(jsonData.msg);
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
                                //$("#redirectregistro").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Inciar sesi&oacute;n</a>');
                            }
                            else if (jsonData.res == 'identificacion') {
                                $("#msgregister").append(jsonData.msg+'. Te invitamos a que actualices tus datos.');
                                $("#redirectregistro").append('<a class="btn btn-default" href="'+siteurl+'">Cerrar</a>');
                            }
                            else {
                                $("#msgregister").append(jsonData.msg);
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            }
                            $("#mdlregistro").modal("show");
                        } else {
                            console.log(jsonData);
                            $("#msgregister").append('Error al registrar, por favor int&eacute;ntelo m&aacute;s tarde.');
                            $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                        }
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        }
    },
    registertequila: function() {
        var nombre = $('#reg_nombre').val();
        var segnombre = $('#reg_segnombre').val();
        var apellido = $('#reg_apellido').val();
        var segapellido = $('#reg_segapellido').val();
        var tipodocumento = $('#reg_tipo_documento').val();
        var numerodocumento = $('#reg_num_documento').val();
        var dia = $('#reg_dia_nac').val();
        var mes = $('#reg_mes_nac').val();
        var ano = $('#reg_ano_nac').val();
        var genero = $('#reg_sexo').val();
        var pais = $('#reg_pais').val();
        var departamento = $('#reg_departamento').val();
        var ciudad = $('#reg_ciudad').val();
        var direccion = $('#reg_direccion').val();
        var correo = $('#reg_correo').val();
        var celular = $('#reg_celular').val();
        var barrio = $('#reg_barrio').val();

        var selectedtema = false;
        $('.ax-checkbox.ax-temas').each(function() {
            if (this.checked) {
               selectedtema = true;
            }
        });

        var urlvid = '';
        if ($("#reg-submit-teq").length > 0) {
            urlvid = $("#reg_url_vid").val();
        }

        var contactoresidencia = $('#reg_med_fis:checked').val();
        var contactoemail = $('#reg_med_vir:checked').val();
        var terminos = $('#reg_terms:checked').val();
        var politica = $('#reg_policy:checked').val();
        var acceptvideo = $('#reg_video:checked').val();
        var expectativa = $('#reg_expectativa').val();
        var mesg = '';

        if (nombre.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa tu nombre</div>';
        }
        if (apellido.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa tu apellido</div>';
        }
        if (tipodocumento == 0) {
            mesg += '<div class="ax-registro-user">Ingresa tu tipo de documento</div>';
        }
        if (numerodocumento.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa tu n&uacute;mero de documento</div>';
        }
        if (dia == '0') {
            mesg += '<div class="ax-registro-user">Ingresa tu d&iacute;a de nacimiento</div>';
        }
        if (mes == '0') {
            mesg += '<div class="ax-registro-user">Ingresa el mes de tu nacimiento</div>';
        }
        if (ano == '0') {
            mesg += '<div class="ax-registro-user">Ingresa el a&ntilde;o de tu nacimiento</div>';
        }

        if (genero == '0') {
            mesg += '<div class="ax-registro-user">Ingresa tu g&eacute;nero</div>';
        }

        if (celular.trim() == '') {
            mesg += '<div class="ax-registro-user">Ingresa tu n&uacute;mero celular</div>';
        }

        if (!terminos) {
            mesg += '<div class="ax-registro-user">Por favor acepta los t&eacute;rminos y condiciones</div>';
        }
        if (!politica) {
            mesg += '<div class="ax-registro-user">Por favor acepta la pol&iacute;tica de privacidad</div>';
        }

        if (mesg != '') {
            $('#axmessage').empty();
            $('#axmessage').append(mesg);
            window.scrollTo(0, 250);
        } else {
            var fechanacimiento = ano+'-'+mes+'-'+dia;
            var temas = '';
            var productos = '';
            var conprodutos = 0;
            var contemas = 0;
            var contacresi = 'S';
            var contavirt = 'S';
            var terms = 'N';
            var policy = 'N';
            var accvideo = 'N';
            var nameciudad = $('#reg_ciudad option[value='+$('#reg_ciudad').val()+']').text();
            var namedepar = $('#reg_departamento option[value='+$('#reg_departamento').val()+']').text();
            var namepais = $('#reg_pais option[value='+$('#reg_pais').val()+']').text();

            $('.ax-checkbox.ax-productos').each(function() {
                if (this.checked) {
                    if (conprodutos == 0) {
                        productos += this.value;
                    } else {
                        productos += ','+this.value;
                    }
                    conprodutos++;
                }
            });

            $('.ax-checkbox.ax-temas').each(function() {
                if (this.checked) {
                    if (contemas == 0) {
                        temas += this.value;
                    } else {
                        temas += ','+this.value;
                    }
                    contemas++;
                }
            });

            if (contactoresidencia) {
                contacresi = 'N';
            }

            if (contactoemail) {
                contavirt = 'N';
            }

            if (terminos) {
                terms = 'S';
            }

            if (politica) {
                policy = 'S';
            }

            if (acceptvideo) {
                accvideo = 'S';
            }

            var data = null;

            data = {
                datauserteq: {
                    nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                    tipodocumento: tipodocumento, numdocumento: numerodocumento, anon: ano, mesn: mes, dian: dia, genero: genero,
                    pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio,
                    correo: correo, celular: celular, temas: temas, contactoresidencia: contacresi,
                    contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa, acceptvideo: accvideo,productos: productos
                }
            };

            $("#msgregister").empty();
            $("#redirectregistro").empty();

            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
                data: data,
                success: function(jsonData) {

                    if (jsonData.redirect) {
                        if (jsonData.res == 'errordb') {
                            $("#msgregister").append("Error al registrar el usuario, por favor int&eacute;ntelo m&aacute;s tarde.");
                        } else {
                            $("#msgregister").append(jsonData.msg);
                        }

                        $("#redirectregistro").append('<a href="'+jsonData.redirect+'" class="btn btn-primary">Continuar</a>');
                        $("#mdlregistro").modal("show");
                    } else {
                        if (jsonData.res != '') {
                            if (jsonData.res == 'errordb') {
                                $("#msgregister").append("Error al registrar el usuario, por favor int&eacute;ntelo m&aacute;s tarde.");
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            }
                            else if (jsonData.res == 'is35') {
                                $("#msgregister").append('Lo sentimos, este sitio es para mujeres mayores de 35 a&ntilde;os, por ese motivo a&uacute;n no puedes hacer parte de nuestra comunidad, esperamos contar contigo m&aacute;s adelante.');
                                /*$("#redirectregistro").append('<a class="btn btn-default" href="'+siteurl+'">Cerrar</a>'); */
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            }
                            else if (jsonData.res == 'ismen') {
                                $("#msgregister").append(jsonData.msg);
                                /*$("#redirectregistro").append('<a class="btn btn-default" href="'+siteurl+'">Cerrar</a>');  */
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            }
                            else if (jsonData.res == 'identificacion') {
                                $("#msgregister").append(jsonData.msg+'. Te invitamos a que actualices tus datos.');
                                /*$("#redirectregistro").append('<a class="btn btn-default" href="'+siteurl+'">Cerrar</a>'); */
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                            }
                            else {
                                $("#msgregister").append(jsonData.msg);
                                $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                                ax.cleanRegister();
                            }
                            $("#mdlregistro").modal("show");
                        } else {
                            console.log(jsonData);
                            $("#msgregister").append('Error al registrar, por favor int&eacute;ntelo m&aacute;s tarde.');
                            $("#redirectregistro").append('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
                        }
                    }
                },
                error: function(jsonData)
                {

                    console.log(jsonData);
                }
            });
        }
    },
    cleanRegister: function () {
        $("#axmessage").empty();
        $('#reg_nombre').val('');
        $('#reg_segnombre').val('');
        $('#reg_apellido').val('');
        $('#reg_segapellido').val('');

        $('#reg_tipo_documento').removeAttr('selected');
        $('#reg_tipo_documento option[value=0]').prop('selected', 'selected');

        $('#reg_num_documento').val('');
        $('#reg_dia_nac').removeAttr('selected');
        $('#reg_mes_nac').removeAttr('selected');
        $('#reg_ano_nac').removeAttr('selected');
        $('#reg_sexo').removeAttr('selected');
        $('#reg_pais').removeAttr('selected');
        $('#reg_departamento').removeAttr('selected');
        $('#reg_ciudad').removeAttr('selected');

        $('#reg_dia_nac option[value=0]').prop('selected', 'selected');
        $('#reg_mes_nac option[value=0]').prop('selected', 'selected');
        $('#reg_ano_nac option[value=0]').prop('selected', 'selected');
        $('#reg_sexo option[value=0]').prop('selected', 'selected');
        $('#reg_pais option[value=0]').prop('selected', 'selected');
        $('#reg_departamento option[value=0]').prop('selected', 'selected');
        $('#reg_ciudad option[value=0]').prop('selected', 'selected');
        $('#reg_direccion').val('');
        $('#reg_correo').val('');
        $('#reg_celular').val('');
        $('#reg_barrio').val('');

        $('.ax-checkbox.ax-temas').each(function() {
            $(this).prop('checked', false);
        });

        $('#reg_med_fis').prop('checked', false);
        $('#reg_med_vir').prop('checked', false);
        $('#reg_terms').prop('checked', false);
        $('#reg_policy').prop('checked', false);
        $('#reg_video').prop('checked', false);
    },
    getInterest: function() {
        $.ajax({
            type: 'GET',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data:{ intereses: 'intereses' },
            beforeSend: function () {
                $('#int_group').empty();
                $('#int_group').append('<img class="ax-preload" src="/wp-content/themes/masvivas/img/ripple.svg"/>');
            },
            success: function(jsonData) {
                //console.log(jsonData);
                $('#int_group').empty();

                $.each(jsonData,function() {
                    $('#int_group').append('<div class="checkbox"><label class="custom-control custom-checkbox"><input type="checkbox" class="ax-checkbox ax-temas custom-control-input" value="'+this.name+'"><span class="custom-control-indicator"></span>'+this.name+'</label>');
                });
            },
            error: function(jsonData) {
                console.log(jsonData);
                $('#int_group').empty();
                $('#int_group').append('<p>No hay temas disponibles</p>');
            }
        });
    },
    getProducts: function(){
        $.ajax({
            type: 'GET',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: {  getProducts: 'getProducts' },
            success: function(jsonData) {

                //console.log(jsonData);
                $('#prod-group').empty();
                if($.isEmptyObject(jsonData)){
                    $('#prod-group').append('<div><p>No hay productos disponibles.</p><div>');
                }else{
                    $.each(jsonData,function() {
                        var producto = '<div class="checkbox"><label class="custom-control custom-checkbox"><input type="checkbox" class="ax-checkbox ax-productos custom-control-input" value="'+this.id+'"><span class="custom-control-indicator"></span>'+this.name+'</label></div>';
                        $('#prod-group').append(producto);
                    });
                }
            },
            error: function(jsonData) {
                $('#prod-group').append('<div><p>No hay productos disponibles.</p><div>');
                console.log(jsonData);
            }
        });
    },
    getTypesDocument: function() {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: { tipodoc: 1 },
            beforeSend: function () {
                $('#reg_tipo_documento').empty();
                $('#reg_tipo_documento').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData) {
                $('#reg_tipo_documento').empty();
                $('#reg_tipo_documento').removeAttr('disabled');
                $('#reg_tipo_documento').append('<option value="0" selected="selected">Tipo de documento</option>');

                if (jsonData.length == 0) {
                    $('#reg_tipo_documento').empty();
                    $('#reg_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
                }

                $.each(jsonData,function() {
                    $('#reg_tipo_documento').append('<option value="'+this.id+'">'+this.documento+'</option>');
                });
            },
            error: function(jsonData) {
                console.log(jsonData);
                $('#reg_tipo_documento').empty();
                $('#reg_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
            }
        });
    },
    getTypesDocumentUpd: function() {
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: { tipodoc: 1 },
            beforeSend: function () {
                $('#upd_tipo_documento').empty();
                $('#upd_tipo_documento').append('<option value="-1" selected="selected" disabled>Cargando...</option>');
            },
            success: function(jsonData) {
                $('#upd_tipo_documento').empty();
                $('#upd_tipo_documento').removeAttr('disabled');
                $('#upd_tipo_documento').append('<option value="0" selected="selected">Tipo de documento</option>');

                if (jsonData.length == 0) {
                    $('#upd_tipo_documento').empty();
                    $('#upd_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
                }

                $.each(jsonData,function() {
                    $('#upd_tipo_documento').append('<option value="'+this.id+'">'+this.documento+'</option>');
                });
            },
            error: function(jsonData) {
                console.log(jsonData);
                $('#upd_tipo_documento').empty();
                $('#upd_tipo_documento').append('<option value="-1" selected="selected">No hay tipos disponibles</option>');
            }
        });
    },
    getUserInfo: function(udata) {

        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: { getuserinfo: udata },
            success: function(jsonData) {

                $('#ax-preload').empty();


                if (jsonData['datauser'].correo != '') {

                    $('#upd_nombre').val(jsonData['datauser'].nombre1);
                    $('#upd_segnombre').val(jsonData['datauser'].nombre2);
                    $('#upd_apellido').val(jsonData['datauser'].apellido1);
                    $('#upd_segapellido').val(jsonData['datauser'].apellido2);
                    $('#upd_tipo_documento option[value="'+jsonData['datauser'].tipoidentificacion+'"]').prop("selected", true);
                    $('#upd_num_documento').val(jsonData['datauser'].identificacion);
                    $('#upd_num_documento').attr("disabled", "disabled");
                    $('#upd_fecha').val(jsonData['datauser'].fechanacimiento);
                    $('#upd_sexo').val(jsonData['datauser'].idsexo);
                    $('#upd_pais option').val(jsonData['datauser'].idpais);

                    $('#upd_departamento').val(jsonData['datauser'].iddepartamento);
                    valor = jsonData['datauser'].iddepartamento;
                    $.get(templateDir+'/inc/service/service.php?departamento='+valor, function(data){

                        if (data.length > 0) {
                            var ciudades = $.parseJSON(data);
                            var elementos = "<option value=''>Selecciona tu ciudad</option>";
                            for (var i = 0; i < ciudades.length; i++) {
                                elementos += '<option value="'+ciudades[i]["id"]+'">'+ciudades[i]['nombre']+'</option>';
                            }
                            $('#upd_ciudad').empty();
                            $('#upd_ciudad').append(elementos);
                            $('#upd_ciudad').val(jsonData['datauser'].idciudad);
                        }
                    });

                    $('#upd_direccion').val(jsonData['datauser'].direccion);
                    $('#upd_email').val(jsonData['datauser'].correo);
                    $('#upd_celular').val(jsonData['datauser'].celular);
                    $('#upd_barrio').val(jsonData['datauser'].barrio);

                    /*if(jsonData.productosData) {
                        var productos = jsonData.productosData;
                        for (i = 0; i < productos.length; i++) {
                            $('#prod-group :input[value="'+productos[i].idproducto+'"]').prop("checked", true);
                        }
                    }*/

                    if(jsonData.productosData) {
                        var productos = jsonData.productosData;
                        for (i = 0; i < productos.length; i++) {
                            $('#prod-group :input[value="'+productos[i]+'"]').prop("checked", true);
                        }
                    }

                    if (jsonData.temasinteres) {
                        var temasusr = jsonData.temasinteres;
                        for (i = 0; i < temasusr.length; i++) {
                            $('#int_group :input[value="'+temasusr[i]+'"]').prop("checked", true);
                        }
                    }

                    if (jsonData['datauser'].esusuario == 'N') {
                        $('#user_radio2').prop("checked", true);
                    }else {
                        $('#user_radio1').prop("checked", true);
                    }
                    if (jsonData['datauser'].contactoemail == 'S') {
                        $('#upd_med_vir').prop("checked", true);
                    }
                    if (jsonData['datauser'].contactoresidencia == 'S') {
                        $('#upd_med_fis').prop("checked", true);
                    }
                    if (jsonData['datauser'].terminosycondiciones == 'S') {
                        $('#upd_terms').prop("checked", true);
                    }
                    if (jsonData['datauser'].politicaprivacidad == 'S') {
                        $('#upd_policy').prop("checked", true);
                    }
                    /*if ($('#upd_med_vir:checked').val() == 'on' && $('#upd_med_fis:checked').val() == 'on') {
                        $('#upd_med_fis').prop('checked', false);
                    }*/

                }
            },
            error: function(jsonData) {
                console.log(jsonData);
            }
        });
    },
    validateStep1: function(reg){
        if(reg)
            sel = "#reg_";
        else
            sel = "#upd_";

        var nombre = $(sel+'nombre');
        var apellido = $(sel+'apellido');
        var tipodocumento = $(sel+'tipo_documento');
        var numerodocumento = $(sel+'num_documento');
        var fecha = $(sel+'fecha');
        var genero = $(sel+'sexo');
        var pais = $(sel+'pais');
        var departamento = $(sel+'departamento');
        var ciudad = $(sel+'ciudad');
        var direccion = $(sel+'direccion');
        var correo = (reg ? $(sel+'correo') : $(sel+'email'));
        var celular = $(sel+'celular');
        var barrio = $(sel+'barrio');
        $('body').find(".ax-registro-user").remove();
        $(".form-group").removeClass("has-error");
        var error = 0;

        if (nombre.val().trim() == '') {
            nombre.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu nombre</span>');
            error++;
        }else{
            nombre.parent().find('.ax-registro-user').remove();
        }
        if (apellido.val().trim() == '') {
            apellido.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu apellido</span>');
            error++;
        }else{
            apellido.parent().find('.ax-registro-user').remove();
        }
        /*if (!reg && $('#upd_segapellido').val().trim() == '') {
            $('#upd_segapellido').parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu segundo apellido</span>');
            error++;
        }else{
            $('#upd_segapellido').parent().find('.ax-registro-user').remove();
        }*/
        if (tipodocumento.val() == 0) {
            tipodocumento.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu tipo de documento</span>');
            error++;
        }else{
            tipodocumento.parent().find('.ax-registro-user').remove();
        }
        if (numerodocumento.val().trim() == '') {
            numerodocumento.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu n&uacute;mero de documento</span>');
            error++;
        }else{
            numerodocumento.parent().find('.ax-registro-user').remove();
        }
        if (fecha.val() == '') {
            fecha.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tus datos</span>');
            error++;
        }else{
            fecha.parent().find('.ax-registro-user').remove();
        }
        if (genero.val() == '0') {
            genero.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu g&eacute;nero</span>');
            error++;
        }else{
            genero.parent().find('.ax-registro-user').remove();
        }
        if (pais.val() == '0') {
            pais.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu pa&iacute;s de origen</span>');
            error++;
        }else{
            pais.parent().find('.ax-registro-user').remove();
        }
        if (departamento.val() == '0') {
            departamento.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu departamento</span>');
            error++;
        }else{
            departamento.parent().find('.ax-registro-user').remove();
        }
        if (ciudad.val() == '0') {
            ciudad.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu ciudad</span>');
            error++;
        }else{
            ciudad.parent().find('.ax-registro-user').remove();
        }
        if (direccion.val().trim() == '') {
            direccion.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu direcci&oacute;n</span>');
            error++;
        }else{
            direccion.parent().find('.ax-registro-user').remove();
        }
        if (!ax.validateEmail(correo.val().trim())) {
            correo.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa un correo electr&oacute;nico v&aacute;lido</span>');
            error++;
        }else{
            correo.parent().find('.ax-registro-user').remove();
        }

        if (celular.val().trim() == '') {
            celular.parent().addClass("has-error").append('<span class="ax-registro-user">Ingresa tu n&uacute;mero celular</span>');
            error++;
        }else{
            celular.parent().find('.ax-registro-user').remove();
        }

        if(error < 1 && reg){

            ax.register();

        }

    },
    updateUserInfo: function() {

        var nombre = $('#upd_nombre').val();
        var segnombre = $('#upd_segnombre').val();
        var apellido = $('#upd_apellido').val();
        var segapellido = $('#upd_segapellido').val();
        var tipodocumento = $('#upd_tipo_documento').val();
        var numerodocumento = $('#upd_num_documento').val();
        var fecha = $('#upd_fecha').val();
        var genero = $('#upd_sexo').val();
        var pais = $('#upd_pais').val();
        var departamento = $('#upd_departamento').val();
        var ciudad = $('#upd_ciudad').val();
        var direccion = $('#upd_direccion').val();
        var correo = $('#upd_email').val();
        var celular = $('#upd_celular').val();
        var barrio = $('#upd_barrio').val();
        var productos = "";
        var conproductos = 0;
        var esusuaria = $("#user_radio1").is(':checked') ? $("#user_radio1").val() : $("#user_radio2").val();
        contacto = true;
        var selectedtema = false;
        $('.ax-checkbox.ax-temas').each(function() {
            if (this.checked) {
               selectedtema = true;
           }
        });

        $('.ax-checkbox.ax-productos').each(function() {
            if (this.checked) {
                if (conproductos == 0) {
                   productos += this.value;
                } else {
                    productos += ','+this.value;
                }
                conproductos++;
            }
        });

        var urlvid = '';
        if ($("#reg-submit-teq").length > 0) {
            urlvid = $("#upd_url_vid").val();
        }

        contacresi = $('#upd_med_fis').is(':checked') ? 'S' : 'N';
        contavirt = $('#upd_med_vir').is(':checked')  ? 'S' : 'N';
        terms = $('#upd_terms').is(':checked')  ? 'S' : 'N';
        policy = $('#upd_policy').is(':checked')  ? 'S' : 'N';

        if(contacresi === 'N' && contavirt === 'N'){
            $('#ax-preload').empty();
            $('#ax-preload').append("<p>Por favor selecciona alg&uacute;n medio de contacto</p>");
            contacto = false;
        }else if (contacresi === 'S') {
            contacto = true;
        }else if (contavirt === 'S') {
            contacto = true;
        }

        if (!selectedtema) {
            $("#int_group").find('.ax-registro-user').remove();
            $("#int_group").append('<span class="ax-registro-user">Por favor selecciona al menos 1 inter&eacute;s</span>');
        }else{
            $("#int_group").find('.ax-registro-user').remove();
        }

        if (terms === 'N') {
            $(".ax-terminoscond #terminos").find('.ax-registro-user').remove();
            $(".ax-terminoscond #terminos").append('<span class="ax-registro-user">Por favor acepta los T&eacute;rminos y condiciones</span>');
        }else{
            $(".ax-terminoscond #terminos").find('.ax-registro-user').remove();
        }
        if (policy === 'N') {
            $(".ax-terminoscond #politica").find('.ax-registro-user').remove();
            $(".ax-terminoscond #politica").append('<span class="ax-registro-user">Por favor acepta la Pol&iacute;tica de privacidad</span>');
        }else{
            $(".ax-terminoscond #politica").find('.ax-registro-user').remove();
        }
        if (policy === 'N' || terms === 'N' || !selectedtema || !contacto) {
            $('#ax-preload').empty();
            $('#ax-preload').append("<p>Comprueba todos los datos</p>");
            return;
        } else {
            var temas = '';
            var contemas = 0;
            var nameciudad = $('#upd_ciudad option[value='+$('#upd_ciudad').val()+']').text();
            var namedepar = $('#upd_departamento option[value='+$('#upd_departamento').val()+']').text();
            var namepais = $('#upd_pais option[value='+$('#upd_pais').val()+']').text();
            $('.ax-checkbox.ax-temas').each(function() {
                if (this.checked) {
                    if (contemas == 0) {
                        temas += this.value;
                    } else {
                        temas += ','+this.value;
                    }
                    contemas++;
                }
            });

            var expectativa = $('#upd_expectativa').val();
            var data = null;

            if ($("#reg-submit-teq").length > 0) {
                data = {
                    datausertequpd: {
                        nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                        tipodocumento: tipodocumento, numdocumento: numerodocumento, fechan: fecha, pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio, correo: correo,
                        celular: celular, temas: temas,urlvid: urlvid, contactoresidencia: contacresi,
                        contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa,productos: productos, esusuario: esusuaria
                    }
                };
            } else {
                data = {
                    datauserupd: {
                        nombre: nombre, segnombre: segnombre, apellido: apellido, segapellido: segapellido,
                        tipodocumento: tipodocumento, numdocumento: numerodocumento, fechan: fecha, genero: genero,
                        pais: pais, namepais: namepais, departamento: departamento, namedepartamento: namedepar, ciudad: ciudad, nameciudad: nameciudad, direccion: direccion, barrio: barrio,
                        correo: correo, celular: celular, temas: temas, contactoresidencia: contacresi,
                        contactoemail: contavirt, terminos: terms, politica: policy, expectativa: expectativa,productos: productos, esusuario: esusuaria
                    }
                };
            }

            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: data,
                success: function(jsonData) {
                    if (jsonData.res != '') {
                        $('#msgupdate').empty();
                        $('#msgupdate').append('<p>'+jsonData.msg+'</p>');
                        $('#mdlupdate').modal('show');
                        //$('#ax-preload').empty();
                        //$('#ax-preload').append('<p>'+jsonData.msg+'</p>');
                    } else {
                        if (jsonData.redirect != '') {
                            $('#msgupdate').empty();
                            $('#msgupdate').append('<p>'+jsonData.msg+'</p>');
                            $('#mdlupdate').modal('show');
                            //$('#ax-preload').empty();
                            //$('#ax-preload').append('<p>'+jsonData.msg+'</p>');
                        } else {
                            if (jsonData.msg.length > 0) {
                                $('#msgupdate').empty();
                                $('#msgupdate').append('<p>'+jsonData.msg+'</p>');
                                $('#mdlupdate').modal('show');
                                //$('#ax-preload').empty();
                                //$('#ax-preload').append('<p>'+jsonData.msg+'</p>');
                            } else {
                                console.log(jsonData);
                            }
                        }
                    }
                },
                error: function(jsonData) {

                    console.log(jsonData);
                }
            });
        }
    },
    getComments: function () {
        $.ajax({
            type: 'GET',
            dataType: "json",
            url:templateDir+'/inc/soap/ax-soap.php',
            data: {allcomments:{idpost: idcurrentpost}},
            success: function(jsonData) {

                if (jsonData.html && jsonData.html != '') {
                    $('#ax-all-comments').empty();
                    $('#ax-all-comments').append(jsonData.html);
                } else {
                    $('#ax-all-comments').append('<div class="ax-aviso"><p>A&uacute;n no se han publicado comentarios.<br>¡S&eacute; el primero en hacerlo!</p></div>');
                }
            },
            error: function(jsonData) {
                console.log(jsonData);
            }
        });
    },
    getCommentsExpectativa: function () {
        $.ajax({
            type: 'GET',
            dataType: "json",
            url:templateDir+'/inc/soap/ax-soap.php',
            data: {allcommentsExpectativa:{idpost: idcurrentpost}},
            success: function(jsonData) {

                if (jsonData.html && jsonData.html != '') {
                    $('#ax-all-comments').empty();
                    $('#ax-all-comments').append(jsonData.html);
                } else {
                    $('#ax-all-comments').append('<div class="ax-aviso"><p>A&uacute;n no se han publicado comentarios.<br>¡S&eacute; el primero en hacerlo!</p></div>');
                }
            },
            error: function(jsonData) {
                console.log(jsonData);
            }
        });
    },
    submitComment: function(textbox){
        var content = $(textbox).val();
        var page_type = $('#page_type').val();
        var parent = $(textbox).attr('parentid');
        // var filename = $("#my_image_upload").val();
        if (content.trim() != '' || $("#my_image_upload_comment_p").val() != '') {

            var data = new FormData();
            data.append('my_image_upload_comment_p', $("#my_image_upload_comment_p")[0].files[0]);
            data.append('content',content);
            data.append('page_type',page_type);
            data.append('parent',parent);
            data.append('inputcomment', textbox);
            data.append('pagecomment', window.location.href);
            data.append('messageimage', '#msgparcomment');
            data.append('imagecomment', $("#my_image_upload_comment_p").val());

            $.ajax({
                type: 'POST',
                url:templateDir+'/inc/soap/ax-soap.php',
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(jsonData) {
                    $("#my_image_upload_comment_p").val("");

                    var obj = JSON.parse(jsonData);

                    if (obj.nombreuser) {
                        $('#msgparcomment').empty();
                        $('#msgparcomment').append('<p>'+obj.msg+'</p>');
                    } else {
                        if (obj.msg != '') {
                            if (obj.msg == 'sesion') {
                                $('#msgparcomment').empty();
                                $('#msgparcomment').append('<p>Por favor <a href="javascript:void(0)" onclick="$('+"'"+'#mdlsolicitar'+"'"+').modal('+"'"+'show'+"'"+');"><strong>inicia sesi&oacute;n</strong></a> o <a href="'+obj.pageregister+'"><strong>reg&iacute;strate</strong></a> para comentar</p>');
                            } else {
                                //var el = $("#reg_subircomentario").emojioneArea();
                                //el[0].emojioneArea.setText('');
                                $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                                $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                                $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                                $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
                                $("#my_image_upload_comment_p").val('');

                                $('#msgparcomment').empty();
                                $('#msgparcomment').append('<p>'+obj.msg+'</p>');
                                if($(textbox).hasClass("ax-expectativa")){
                                    $('#reg_subircomentario').val('');
                                    ax.getCommentsExpectativa();
                                }
                                else
                                    ax.getComments();
                            }

                        } else {
                            console.log(obj);
                        }
                    }
                },
                error: function(jsonData) {
                    console.log(obj);
                }
            });

        } else {

            $('#msgparcomment').empty();
            $('#msgparcomment').append('<p>Por favor ingresa un comentario.</p>');
        }
    },
    submitSubComment: function(textbox,imgu,idcomment){
        var content = $(textbox).val();
        var page_type = $('#page_type').val();
        var parent = $(textbox).attr('parentid');

        // var filename = $("#my_image_upload").val();

        if (content.trim() != '' || $("#my_image_upload_comment"+idcomment).val() != '') {
            var data = new FormData();
            data.append('my_image_upload_comment', imgu[0].files[0]);

            data.append('content',content);
            data.append('page_type',page_type);
            data.append('parent',parent);
            data.append('inputcomment', textbox);
            data.append('pagecomment', window.location.href);
            data.append('messageimage', '#msgsubcomment'+idcomment);
            data.append('imagecomment', $("#my_image_upload_comment"+idcomment).val());

            $.ajax({
                type: 'POST',
                url:templateDir+'/inc/soap/ax-soap.php',
                data: data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(jsonData) {

                    var obj = JSON.parse(jsonData);

                    if (obj.nombreuser) {
                        $('#msgsubcomment'+idcomment).empty();
                        $('#msgsubcomment'+idcomment).append('<p>'+obj.msg+'</p>');
                    } else {
                        if (obj.msg != '') {
                            if (obj.msg == 'sesion') {
                                $('#msgsubcomment'+idcomment).empty();
                                $('#msgsubcomment'+idcomment).append('<p>Por favor <a href="javascript:void(0)" onclick="$('+"'"+'#mdlsolicitar'+"'"+').modal('+"'"+'show'+"'"+');"><strong>inicia sesi&oacute;n</strong></a> o <a href="'+obj.pageregister+'"><strong>reg&iacute;strate</strong></a> para comentar</p>');
                            } else {
                                //var el = $('#reg_subirsubcomentarios'+idcomment).emojioneArea();
                                //el[0].emojioneArea.setText('');
                                $("#spanb"+idcomment+" i.fa").removeClass("fa-check");
                                $("#spanb"+idcomment+" i.fa").addClass("fa-camera");
                                $("#spanb"+idcomment+" i.fa").css("color","#b2b2b2");
                                $("#spanb"+idcomment).css("background-color","#fff");

                                $('#msgparcomment').empty();
                                $('#msgparcomment').append('<p>'+obj.msg+'</p>');
                                if($(textbox).hasClass("ax-expectativa"))
                                    ax.getCommentsExpectativa();
                                else
                                    ax.getComments();
                            }
                        } else {
                            console.log(jsonData);
                        }
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        } else {

            $('#msgsubcomment'+idcomment).empty();
            $('#msgsubcomment'+idcomment).append('<p>Por favor ingresa un comentario.</p>');
        }
    },
    shareFacebook: function (titulo, comentario, imagen) {
        data = {};
        data.method = 'share';
        data.title = titulo;
        data.quote = siteurl;
        data.description = comentario;
        data.href = window.location.href;

        if (imagen != '') {
            data.picture = imagen;
        } else {
            data.picture = 'http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/11093539/bg-share-facebook.jpg';
        }

        FB.ui(data, function(response){
            if (response && !response.error_code) {
                if (typeof response != 'undefined'){
                    console.log(response);
                }
            }
        });
    },
    findShop: function () {
        var deparment = $('#reg_departamento').val();
        var city = $('#reg_ciudad').val();

        if (deparment > 0 && city > 0) {
            $('#wpsl-search-input').val('');
            $('#wpsl-search-input').val($('#reg_departamento option:selected').text()+', '+$('#reg_ciudad option:selected').text());
            $('#wpsl-search-btn').click();
        } else {
            alert('Selecciona departamento y ciudad');
        }
    },
    registerProduct: function (idpro) {
        if (typeof(idpro)==='undefined') idpro = 0;

        var product = [];

        if (idpro != 0) {
            product.push = idpro;
        }

        $('input[type=checkbox]:checked').each(function() {
            product.push(this.value);
        });

        if (product.length > 0) {
            $("#pixelsS").empty();
            $("#pixelsS").append('<!-- Google Code for Ligths Conversiones Conversion Page --> <script type="text/javascript">/* <![CDATA[ */var google_conversion_id = 960862839;var google_conversion_language = "en";var google_conversion_format = "3";var google_conversion_color = "ffffff";var google_conversion_label = "gN5wCOC6l2oQ97SWygM"; var google_remarketing_only = false;/* ]]> */</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script><noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/960862839/?label=gN5wCOC6l2oQ97SWygM&guid=ON&script=0"/></div></noscript>');

            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: {registerprod: {idprod: product}},
                success: function(jsonData) {

                    if (jsonData.res == 'sesion') {
                        $("#mdlsolicitar").modal("show");
                    } else {

                        $('#msgresponse').html("");
                        $('#msgresponse').html(jsonData.msg);
                        $('#mdlresponse').modal('show');
                        //$("#msgmuestra").empty();
                        //$("#msgmuestra").append(jsonData.msg);
                    }
                },
                error: function(jsonData) {
                    console.log(jsonData);
                }
            });
        } else {
            $('#msgresponse').html("");
            $('#msgresponse').html('Por favor selecciona un producto');
            $('#mdlresponse').modal('show');
            //$("#msgmuestra").empty();
            //$("#msgmuestra").append('Por favor selecciona un producto');
        }
    },
    newsLetter: function () {
        $("#msgsuscrip").empty();

        var email = $("#txtEmail").val();
        var charter = $("#txtCharter").val();
        var terms = $("#chkterminos:checked").val();

        if (email.trim() == '' || charter.trim() == '') {
            $("#msgsuscrip").append('* Por favor completa todos los campos del formulario.');
        } else if (!ax.validateEmail(email)) {
            $("#msgsuscrip").append('* Por favor ingresa un correo v&aacute;lido.');
        } else if (!terms) {
            $("#msgsuscrip").append('* Por favor acepta los t&eacute;rminos y condiciones.');
        } else {
            var data = {correo: email, identificacion: charter};
            $.ajax({
                type: 'POST',
                url: templateDir+'/inc/soap/ax-soap.php',
                dataType : "json",
                data: {newsletter: data},
                success: function(jsonData) {

                    $('#msgregistro').empty();

                    if (jsonData.registered) {
                        $('#msgregistro').append('¡Tu registro ha sido exitoso! Pr&oacute;ximamente te enviaremos informaci&oacute;n de tu inter&eacute;s.');
                        $("#mdlsuscripcion").modal("show");
                        $("#txtEmail").val("");
                        $("#txtCharter").val("");
                        $("#chkterminos").prop('checked',false);
                    } else if (jsonData.sesion == true) {
                        $('#msgsuscrip').append('Por favor <a href="javascript:void(0)" onclick="$('+"'"+'#mdllogin'+"'"+').modal('+"'"+'show'+"'"+');"><strong>inicia sesi&oacute;n</strong></a> o <a href="'+jsonData.pageregister+'"><strong>reg&iacute;strate</strong></a> para suscribirte.');
                    } else if (jsonData.alreadyexist) {
                        $('#msgregistro').append('Tu inscripci&oacute;n a los boletines ya hab&iacute;a sido realizada. Tu informaci&oacute;n ya se encuentra en nuestra base de datos.');
                        $("#mdlsuscripcion").modal("show");
                        $("#txtEmail").val("");
                        $("#txtCharter").val("");
                        $("#chkterminos").prop('checked',false);
                    } else {
                        console.log(jsonData);
                    }
                },
                error: function(jsonData)
                {

                    console.log(jsonData);
                }
            });
        }
    },
    cerrarSesion: function () {
        $("#frmCerrar").submit();
    },
    toggleVideo: function(el) {
        var video = $('.video-toggle.ax-toggle-video');
        var videosLink = {
            'aleja': "http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2017/07/18142352/aleja.mp4",
            'caro': "http://familia.streaming.s3.amazonaws.com/video-caro-mujeres-de-tiempo-completo.mp4",
            'vero': "http://familia.streaming.s3.amazonaws.com/video-vero-mujeres-de-tiempo-completo.mp4",
        };
        $(".ax-angle").fadeOut();
        video.slideUp(500,function(){
            id = $(el).parent().attr('id');
            $(".ax-toggle-video").removeClass('ax-aleja ax-caro ax-vero');
            switch(id){
                case 'aleja':
                video.find('.ax-promo-title').text('Alejandra');
                video.find('.ax-video-text').text('Ella es Aleja, la aventurera, le aburre la rutina, le gusta experimentar, conocer nuevos lugares y costumbres, cree que la maternidad y la vida de hogar no es para ella, valora las mujeres que piensan de esa manera pero considera que hay cosas m&aacute;s importantes.');
                document.getElementById('videoExpectativa').src = videosLink.aleja;
                $(".ax-toggle-video").addClass('ax-aleja');
                break;
                case 'vero':
                video.find('.ax-promo-title').text('Ver&oacute;nica');
                video.find('.ax-video-text').text('Ella es Vero una mam&aacute; que ha dedicado sus &uacute;ltimos a&ntilde;os a una vida de teteros, de cuentos en la noche,  de pa&ntilde;ales y de pel&iacute;culas infantiles.  Gan&oacute; tres Cannes Lions como publicista pero ser mam&aacute; ha sido su verdadera vocaci&oacute;n.');
                document.getElementById('videoExpectativa').src = videosLink.vero;
                $(".ax-toggle-video").addClass('ax-vero');
                break;
                case 'caro':
                video.find('.ax-promo-title').text('Carolina');
                video.find('.ax-video-text').text('Ella es Caro, la profesional, ama su trabajo y se esfuerza para que todo salga bien, su estilo de vida es vertiginoso, r&aacute;pido y, aunque tambi&eacute;n es mam&aacute; no se imagina su vida siendo ama de casa. ');
                document.getElementById('videoExpectativa').src = videosLink.caro;
                $(".ax-toggle-video").addClass('ax-caro');
                break;
            }
            $(el).find(".ax-angle").fadeIn();
        });
        video.slideDown(500);
        document.getElementById('videoExpectativa').load();
        $('#VideoModal').on('shown.bs.modal',function(){
            document.getElementById('videoExpectativa').play();
        });

        $('#VideoModal').on('hide.bs.modal',function(){
            document.getElementById('videoExpectativa').pause();
        });
    },
    toggleEpisodios: function(el, id) {
        video = {};
        $(".ax-expandDesc .ax-descipcion-episodios").slideUp(500);
        setTimeout(function(){
            $('.ax-expandDesc').empty();
            video = $('.ax-descipcion-episodios').clone();
            el = $(el);
            el.parents('.row').find('.ax-expandDesc').append(video);
            video.find('.ax-promo-title').text(ax.episodios[id].title);
            video.find('.ax-video-text').text(ax.episodios[id].description);
            $('.ax-expandDesc').css({'background':'url('+ax.episodios[id].imagen+') no-repeat'});
            console.log(ax.episodios[id].url);
            //$('#videoExpectativa').html(ax.episodios[id].url);
            //document.getElementById('videoExpectativa').load();
            $('.iconPlay').attr('data-id',id);
            el.find(".ax-angle").fadeIn();
            $(".ax-angle").fadeOut();
            $(".ax-expandDesc .ax-descipcion-episodios").slideDown(500);
        },500)
    },
    openLastEpisode: function() {
        var bodyModal = $("#VimeoModal .modal-body");
        bodyModal.empty();
        bodyModal.append(ax.episodios[ax.first].url);
        $("#VimeoModal").modal("show");
    },
    recuperarPass: function(){
        var data = {
            forgotPassword: 1,
            mail: $("#rec_email_mdl").val()
        }
        $.ajax({
            type: 'POST',
            url: templateDir+'/inc/soap/ax-soap.php',
            dataType : "json",
            data: data,
            success: function(jsonData) {
                $('#axmsgrecuperar_mdl').empty();
                if (!jsonData.error) {
                    $('#axmsgrecuperar_mdl').append('Revisa tu bandeja de entrada.');
                } else {
                    $('#axmsgrecuperar_mdl').append('Este correo no se encuentra en nuestra base de datos, ingresa el mail con el que te registraste: masvivas@ejemplo.com');
                }
            },
            error: function(jsonData) {
                console.log(jsonData);
            }
        });
    },
    first :""
}

$(document).ajaxStart(function(){
    $(".ax-back-opa").css("display", "block");
});
$( document ).ajaxStop(function() {
    $(".ax-back-opa").css("display", "none");
});