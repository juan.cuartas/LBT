<?php
/**
 * The Footer widget areas
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
if ( is_active_sidebar( 'first-footer-widget-area' ) || is_active_sidebar( 'second-footer-widget-area' ) || is_active_sidebar( 'third-footer-widget-area' ) || is_active_sidebar( 'fourth-footer-widget-area' ) ) : ?>
<div class="container">
    <div class="ax-footer-logo">
        <div class="row">
            <div class="ax-footer-cont">
                <div class="col-md-4">
                    <div class="row">
                        <h2><img class="img-responsive" alt="Lights by TENA" title="Lights by TENA" src="/wp-content/themes/lights/img/ax-logo-bn.png"></h2>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ax-menu-footer">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer1')); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ax-menu-footer">
                        <h4>Nuestros productos:</h4>
                        <?php wp_nav_menu( array( 'theme_location' => 'footer2')); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="ax-menu-footer">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer3')); ?>
                    </div>
                </div>
            </div>
            <div class="ax-footertel">
                <div class="col-md-5 col-sm-4"></div>
                <div class="col-md-5 col-sm-5">
                    <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
                </div>
                <div class="col-md-2 col-sm-3">
                    <?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
                </div>
            </div>
            <div class="ax-footer">
                <div class="col-md-3">
                    <div class="row">
                        <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
                </div>
                <div class="col-md-3">
                    <div class="ax-iconosredes">
                       <?php dynamic_sidebar( 'social-widget-area' ); ?>
                   </div>
                </div>
            </div>
        </div><!-- /.row -->
    </div>
</div><!-- /.containereste -->
<?php endif ; ?>
