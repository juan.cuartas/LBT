<?php
/**
 * Bootstrap Canvas WP functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, bootstrapcanvaswp_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'bootstrapcanvaswp_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

/*
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 900;

/* Tell WordPress to run bootstrapcanvaswp_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'bootstrapcanvaswp_setup' );

if ( ! function_exists( 'bootstrapcanvaswp_setup' ) ):
/**
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override bootstrapcanvaswp_setup() in a child theme, add your own bootstrapcanvaswp_setup to your child theme's
 * functions.php file.
 *
 * @uses add_theme_support()        To add support for post thumbnails, custom headers and backgrounds, and automatic feed links.
 * @uses register_nav_menus()       To add support for navigation menus.
 * @uses add_editor_style()         To style the visual editor.
 * @uses load_theme_textdomain()    For translation/localization support.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size()  To set a custom post thumbnail size.
 *
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_setup() {
    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style( 'editor-style.css' );

    // Post Format support.
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

    // This theme uses post thumbnails
    add_theme_support( 'post-thumbnails' );

    // Add default posts and comments RSS feed links to head
    add_theme_support( 'automatic-feed-links' );

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory
     */
    load_theme_textdomain( 'bootstrapcanvaswp', get_template_directory() . '/languages' );

    // Register Custom Navigation Walker
    require_once('inc/wp_bootstrap_navwalker.php');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'bootstrapcanvaswp' ),
    ) );



    register_nav_menus( array(
        'footer1' => __( 'Footer 1', 'bootstrapcanvaswp' ),
    ) );
    register_nav_menus( array(
        'footer2' => __( 'Footer 2', 'bootstrapcanvaswp' ),
    ) );
    register_nav_menus( array(
        'footer3' => __( 'Footer 3', 'bootstrapcanvaswp' ),
    ) );

    register_nav_menus( array(
        'topmenu' => __( 'Top Menu', 'bootstrapcanvaswp' ),
    ) );

    // This theme allows users to set a custom background.
    $args = array(
        // Let WordPress know what our default background color is.
        'default-color' => 'fff',
    );
    add_theme_support( 'custom-background', $args );

    // The custom header business starts here.

    $args = array(
        // The height and width of our custom header.
        'width'         => '980',
        'height'        => '170',
        // Support flexible widths and heights.
        'flex-height'    => true,
        'flex-width'    => true,
        // Let WordPress know what our default text color is.
        'default-text-color'     => '333',
    );
    add_theme_support( 'custom-header', $args );

    // This feature allows themes to add document title tag to HTML <head>.
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'bootstrapcanvaswp_setup' );
endif;

/**
 * Enqueue scripts and styles for front-end.
 *
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_scripts() {

    wp_enqueue_style( 'bootstrap.min-css', get_template_directory_uri() . '/css/bootstrap.min.css',  false);
    wp_enqueue_style( 'ax-main.media', get_template_directory_uri() . '/css/ax-main.media.css',  false);
    wp_enqueue_style( 'jPages-css', get_template_directory_uri() . '/css/jPages.css',  false);
    wp_enqueue_style( 'flipclock-css', get_template_directory_uri() . '/css/flipclock.css',  false);

    wp_enqueue_style( 'blog-css', get_template_directory_uri() . '/css/blog.css', false );
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css',  false);
    wp_enqueue_style( 'font-awesome.min.css', get_template_directory_uri() . '/css/font-awesome.min.css',  false );
    wp_enqueue_style( 'Normalize', get_template_directory_uri() . '/css/normalize.css' ,  false);

    wp_enqueue_style( 'main', get_template_directory_uri() . '/css/ax-main.css' ,  false);

    if ( is_rtl() ) {
        wp_enqueue_style( 'blog-rtl-css', get_template_directory_uri() . '/css/blog-rtl.css' , true);
        wp_enqueue_style( 'bootstrap-rtl-css', get_template_directory_uri() . '/css/bootstrap-rtl.css', false );
    }

    wp_enqueue_style( 'style-css', get_stylesheet_uri() );


}
add_action( 'wp_enqueue_scripts', 'bootstrapcanvaswp_scripts' );

function move_scripts_from_head_to_footer() {
    remove_action( 'wp_head', 'wp_print_scripts' );
    remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
    remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );

    add_action( 'wp_footer', 'wp_print_scripts', 5);
    add_action( 'wp_footer', 'wp_enqueue_scripts', 5);
    add_action( 'wp_footer', 'wp_print_head_scripts', 5);
}
add_action('wp_enqueue_scripts', 'move_scripts_from_head_to_footer');



/**
 * Register widgetized areas, including main sidebar and three widget-ready columns in the footer.
 *
 * To override bootstrapcanvaswp_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Bootstrap Canvas WP 1.0
 *
 * @uses register_sidebar()
 */
function bootstrapcanvaswp_widgets_init() {
    // Area 1, located at the top of the sidebar.
    register_sidebar( array(
        'name' => __( 'Primary Widget Area', 'bootstrapcanvaswp' ),
        'id' => 'primary-widget-area',
        'description' => __( 'Add widgets here to appear in your sidebar.', 'bootstrapcanvaswp' ),
        'before_widget' => '<div id="%1$s" class="sidebar-module widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ) );

    // Area 2, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'First Footer Widget Area', 'bootstrapcanvaswp' ),
        'id'            => 'first-footer-widget-area',
        'description' => __( 'An optional widget area for your site footer.', 'bootstrapcanvaswp' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    // Area 3, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area', 'bootstrapcanvaswp' ),
        'id'            => 'second-footer-widget-area',
        'description' => __( 'An optional widget area for your site footer.', 'bootstrapcanvaswp' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    // Area 4, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Third Footer Widget Area', 'bootstrapcanvaswp' ),
        'id'            => 'third-footer-widget-area',
        'description' => __( 'An optional widget area for your site footer.', 'bootstrapcanvaswp' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    // Area 5, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Fourth Footer Widget Area', 'bootstrapcanvaswp' ),
        'id'            => 'fourth-footer-widget-area',
        'description' => __( 'An optional widget area for your site footer.', 'bootstrapcanvaswp' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    // Area 5, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Social', 'bootstrapcanvaswp' ),
        'id'            => 'social-widget-area',
        'description' => __( 'An optional widget area for your site footer.', 'bootstrapcanvaswp' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );


}
add_action( 'widgets_init', 'bootstrapcanvaswp_widgets_init' );

/**
 * Use get_the_excerpt() to print an excerpt by specifying a maximium number of characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Bootstrap Canvas WP 1.0
 *
 * @param int $charlength The number of excerpt characters.
 * @return int The filtered number of excerpt characters.
 */
function the_excerpt_max_charlength($charlength) {
    $excerpt = get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
            echo mb_substr( $subex, 0, $excut );
        } else {
            echo $subex;
        }
        echo '[...]';
    } else {
        echo $excerpt;
    }
}

/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Bootstrap Canvas WP 1.0
 */
class Bootstrap_Canvas_WP_Customize {
    /**
    * This hooks into 'customize_register' (available as of WP 3.4) and allows
    * you to add new sections and controls to the Theme Customize screen.
    *
    * Note: To enable instant preview, we have to actually write a bit of custom
    * javascript. See live_preview() for more.
    *
    * @see add_action('customize_register',$func)
    * @param \WP_Customize_Manager $wp_customize
    * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
    * @since Bootstrap Canvas WP 1.0
    */
    public static function register ( $wp_customize ) {
        //1. Define a new section (if desired) to the Theme Customizer
        $wp_customize->add_section( 'title_tagline',
                                   array(
                                       'title' => __( 'Site Title & Tagline', 'bootstrapcanvaswp' ), //Visible title of section
                                       'priority' => 1, //Determines what order this appears in
                                       'capability' => 'edit_theme_options', //Capability needed to tweak
                                       'description' => __('', 'bootstrapcanvaswp'), //Descriptive tooltip
                                   )
                                  );

        //1. Define a new section (if desired) to the Theme Customizer
        $wp_customize->add_section( 'bootstrapcanvaswp_copyright',
                                   array(
                                       'title' => __( 'Copyright', 'bootstrapcanvaswp' ), //Visible title of section
                                       'priority' => 2, //Determines what order this appears in
                                       'capability' => 'edit_theme_options', //Capability needed to tweak
                                       'description' => __('', 'bootstrapcanvaswp'), //Descriptive tooltip
                                   )
                                  );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'copyrighttext', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_text_field',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Control( //Instantiate the text control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_copyrighttext', //Set a unique ID for the control
            array(
                'label' => __( 'Copyright', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'bootstrapcanvaswp_copyright', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'copyrighttext', //Which setting to load and manipulate (serialized is okay)
                'priority' => 1, //Determines the order this control appears in for the specified section
                'type' => 'text',
            )
        ) );

        //1. Define a new section (if desired) to the Theme Customizer
        $wp_customize->add_section( 'bootstrapcanvaswp_fonts',
                                   array(
                                       'title' => __( 'Fonts', 'bootstrapcanvaswp' ), //Visible title of section
                                       'priority' => 3, //Determines what order this appears in
                                       'capability' => 'edit_theme_options', //Capability needed to tweak
                                       'description' => __('', 'bootstrapcanvaswp'), //Descriptive tooltip
                                   )
                                  );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'body_fontfamily', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => 'georgia, serif', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'bootstrapcanvaswp_sanitize_font_selection',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Control( //Instantiate the text control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_body_fontfamily', //Set a unique ID for the control
            array(
                'label' => __( 'Text Font', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'bootstrapcanvaswp_fonts', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'body_fontfamily', //Which setting to load and manipulate (serialized is okay)
                'priority' => 1, //Determines the order this control appears in for the specified section
                'type'     => 'select',
                'choices'  => array(
                    'arial, helvetica, sans-serif'                     => 'Arial',
                    'arial black, gadget, sans-serif'                  => 'Arial Black',
                    'comic sans ms, cursive, sans-serif'               => 'Comic Sans MS',
                    'courier new, courier, monospace'                  => 'Courier New',
                    'georgia, serif'                                   => 'Georgia',
                    'impact, charcoal, sans-serif'                     => 'Impact',
                    'lucida console, monaco, monospace'                => 'Lucida Console',
                    'lucida sans unicode, lucida grande, sans-serif'   => 'Lucida Sans Unicode',
                    'palatino linotype, book antiqua, palatino, serif' => 'Palatino Linotype',
                    'tahoma, geneva, sans-serif'                       => 'Tahoma',
                    'times new roman, times, serif'                    => 'Times New Roman',
                    'trebuchet ms, helvetica, sans-serif'              => 'Trebuchet MS',
                    'verdana, geneva, sans-serif'                      => 'Verdana',
                )
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'headings_fontfamily', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => 'arial, helvetica, sans-serif', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'bootstrapcanvaswp_sanitize_font_selection',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Control( //Instantiate the text control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_headings_fontfamily', //Set a unique ID for the control
            array(
                'label' => __( 'Headings Font', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'bootstrapcanvaswp_fonts', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'headings_fontfamily', //Which setting to load and manipulate (serialized is okay)
                'priority' => 2, //Determines the order this control appears in for the specified section
                'type'     => 'select',
                'choices'  => array(
                    'arial, helvetica, sans-serif'                     => 'Arial',
                    'arial black, gadget, sans-serif'                  => 'Arial Black',
                    'comic sans ms, cursive, sans-serif'               => 'Comic Sans MS',
                    'courier new, courier, monospace'                  => 'Courier New',
                    'georgia, serif'                                   => 'Georgia',
                    'impact, charcoal, sans-serif'                     => 'Impact',
                    'lucida console, monaco, monospace'                => 'Lucida Console',
                    'lucida sans unicode, lucida grande, sans-serif'   => 'Lucida Sans Unicode',
                    'palatino linotype, book antiqua, palatino, serif' => 'Palatino Linotype',
                    'tahoma, geneva, sans-serif'                       => 'Tahoma',
                    'times new roman, times, serif'                    => 'Times New Roman',
                    'trebuchet ms, helvetica, sans-serif'              => 'Trebuchet MS',
                    'verdana, geneva, sans-serif'                      => 'Verdana',
                )
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'menu_fontfamily', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => 'georgia, serif', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'bootstrapcanvaswp_sanitize_font_selection',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Control( //Instantiate the text control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_menu_fontfamily', //Set a unique ID for the control
            array(
                'label' => __( 'Menu Font', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'bootstrapcanvaswp_fonts', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'menu_fontfamily', //Which setting to load and manipulate (serialized is okay)
                'priority' => 3, //Determines the order this control appears in for the specified section
                'type'     => 'select',
                'choices'  => array(
                    'arial, helvetica, sans-serif'                     => 'Arial',
                    'arial black, gadget, sans-serif'                  => 'Arial Black',
                    'comic sans ms, cursive, sans-serif'               => 'Comic Sans MS',
                    'courier new, courier, monospace'                  => 'Courier New',
                    'georgia, serif'                                   => 'Georgia',
                    'impact, charcoal, sans-serif'                     => 'Impact',
                    'lucida console, monaco, monospace'                => 'Lucida Console',
                    'lucida sans unicode, lucida grande, sans-serif'   => 'Lucida Sans Unicode',
                    'palatino linotype, book antiqua, palatino, serif' => 'Palatino Linotype',
                    'tahoma, geneva, sans-serif'                       => 'Tahoma',
                    'times new roman, times, serif'                    => 'Times New Roman',
                    'trebuchet ms, helvetica, sans-serif'              => 'Trebuchet MS',
                    'verdana, geneva, sans-serif'                      => 'Verdana',
                )
            )
        ) );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'header_textcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Header Text Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'header_textcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 1, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'body_textcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#555', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_body_textcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Text Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'body_textcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 2, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'link_textcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#428bca', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_link_textcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'link_textcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 3, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'hover_textcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#23527c', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_hover_textcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Hover Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'hover_textcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 4, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'headings_textcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#333', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_headings_textcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Headings Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'headings_textcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 5, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'primary_menucolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#428bca', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_primary_menucolor', //Set a unique ID for the control
            array(
                'label' => __( 'Primary Menu Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'primary_menucolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 6, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'primary_linkcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#cdddeb', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_primary_linkcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Primary Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'primary_linkcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 7, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'primary_hovercolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#fff', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_primary_hovercolor', //Set a unique ID for the control
            array(
                'label' => __( 'Primary Hover Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'primary_hovercolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 8, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'primary_activecolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#fff', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_primary_activecolor', //Set a unique ID for the control
            array(
                'label' => __( 'Primary Active Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'primary_activecolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 9, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'primary_activebackground', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#428bca', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_primary_activebackground', //Set a unique ID for the control
            array(
                'label' => __( 'Primary Active Background Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'primary_activebackground', //Which setting to load and manipulate (serialized is okay)
                'priority' => 10, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'dropdown_menucolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#fff', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_dropdown_menucolor', //Set a unique ID for the control
            array(
                'label' => __( 'Dropdown Menu Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'dropdown_menucolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 11, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'dropdown_linkcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#333', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_dropdown_linkcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Dropdown Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'dropdown_linkcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 12, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'dropdown_hovercolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#333', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_dropdown_hovercolor', //Set a unique ID for the control
            array(
                'label' => __( 'Dropdown Hover Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'dropdown_hovercolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 13, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'dropdown_hoverbackground', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#f5f5f5', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_dropdown_hoverbackground', //Set a unique ID for the control
            array(
                'label' => __( 'Dropdown Hover Background Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'dropdown_hoverbackground', //Which setting to load and manipulate (serialized is okay)
                'priority' => 14, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'dropdown_activecolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#fff', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_dropdown_activecolor', //Set a unique ID for the control
            array(
                'label' => __( 'Dropdown Active Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'dropdown_activecolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 15, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'dropdown_activebackground', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#080808', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_dropdown_activebackground', //Set a unique ID for the control
            array(
                'label' => __( 'Dropdown Active Background Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'dropdown_activebackground', //Which setting to load and manipulate (serialized is okay)
                'priority' => 16, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'footer_textcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#999', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_footer_textcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Footer Text Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'footer_textcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 17, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'footer_linkcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#428bca', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_footer_linkcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Footer Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'footer_linkcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 18, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'footer_hovercolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#23527c', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_footer_hovercolor', //Set a unique ID for the control
            array(
                'label' => __( 'Footer Hover Link Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'footer_hovercolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 19, //Determines the order this control appears in for the specified section
            )
        ) );

        //2. Register new settings to the WP database...
        $wp_customize->add_setting( 'footer_backgroundcolor', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                                   array(
                                       'default' => '#f9f9f9', //Default setting/value to save
                                       'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                                       'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                                       'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                                       'sanitize_callback' => 'sanitize_hex_color',
                                   )
                                  );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_footer_backgroundcolor', //Set a unique ID for the control
            array(
                'label' => __( 'Footer Background Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'footer_backgroundcolor', //Which setting to load and manipulate (serialized is okay)
                'priority' => 20, //Determines the order this control appears in for the specified section
            )
        ) );

        //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
        $wp_customize->add_control( new WP_Customize_Color_Control( //Instantiate the color control class
            $wp_customize, //Pass the $wp_customize object (required)
            'bootstrapcanvaswp_background_color', //Set a unique ID for the control
            array(
                'label' => __( 'Background Color', 'bootstrapcanvaswp' ), //Admin-visible name of the control
                'section' => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                'settings' => 'background_color', //Which setting to load and manipulate (serialized is okay)
                'priority' => 21, //Determines the order this control appears in for the specified section
            )
        ) );

        //4. We can also change built-in settings by modifying properties. For instance, let's make some stuff use live preview JS...
        $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
        $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
        $wp_customize->add_setting( 'display_header_text' , array( 'default' => true, 'sanitize_callback' => 'bootstrapcanvaswp_sanitize_checkbox' ) );
        $wp_customize->get_setting( 'display_header_text' )->transport = 'postMessage';
        $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
        $wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
    }

    /**
    * This will output the custom WordPress settings to the live theme's WP head.
    *
    * Used by hook: 'wp_head'
    *
    * @see add_action('wp_head',$func)
    * @since Bootstrap Canvas WP 1.0
    */
    public static function header_output() {
    ?>
    <!--Customizer CSS-->
    <style type="text/css">
        <?php self::generate_css('.blog-title, .blog-description', 'color', 'header_textcolor', '#'); ?>
        <?php self::generate_css('body', 'background-color', 'background_color', '#'); ?>
        <?php self::generate_css('.blog-nav .active', 'color', 'background_color', '#'); ?>
        <?php self::generate_css('body', 'font-family', 'body_fontfamily'); ?>
        <?php self::generate_css('body', 'color', 'body_textcolor'); ?>
        <?php self::generate_css('a', 'color', 'link_textcolor'); ?>
        <?php self::generate_css('a:hover, a:focus', 'color', 'hover_textcolor'); ?>
        <?php self::generate_css('h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6', 'font-family', 'headings_fontfamily'); ?>
        <?php self::generate_css('h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6', 'color', 'headings_textcolor'); ?>
        <?php self::generate_css('.navbar-inverse', 'font-family', 'menu_fontfamily'); ?>
        <?php self::generate_css('.navbar-inverse', 'background-color', 'primary_menucolor'); ?>
        <?php self::generate_css('.navbar-inverse .navbar-brand, .navbar-inverse .navbar-nav > li > a', 'color', 'primary_linkcolor'); ?>
        <?php self::generate_css('.navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus', 'color', 'primary_hovercolor'); ?>
        <?php self::generate_css('.navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus', 'color', 'primary_activecolor'); ?>
        <?php self::generate_css('.navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus, .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus', 'background-color', 'primary_activebackground'); ?>
        <?php self::generate_css('.dropdown-menu', 'background-color', 'dropdown_menucolor'); ?>
        <?php self::generate_css('.dropdown-menu > li > a, .navbar-inverse .navbar-nav .open .dropdown-menu > li > a', 'color', 'dropdown_linkcolor'); ?>
        <?php self::generate_css('.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:focus', 'color', 'dropdown_hovercolor'); ?>
        <?php self::generate_css('.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:focus', 'background-color', 'dropdown_hoverbackground'); ?>
        <?php self::generate_css('.dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:focus', 'color', 'dropdown_activecolor'); ?>
        <?php self::generate_css('.dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:focus', 'background-color', 'dropdown_activebackground'); ?>
        <?php self::generate_css('.blog-footer', 'color', 'footer_textcolor'); ?>
        <?php self::generate_css('.blog-footer a', 'color', 'footer_linkcolor'); ?>
        <?php self::generate_css('.blog-footer a:hover, .blog-footer a:focus', 'color', 'footer_hovercolor'); ?>
        <?php self::generate_css('.blog-footer', 'background-color', 'footer_backgroundcolor'); ?>
    </style>
    <!--/Customizer CSS-->
    <?php
    }

    /**
    * This outputs the javascript needed to automate the live settings preview.
    * Also keep in mind that this function isn't necessary unless your settings
    * are using 'transport'=>'postMessage' instead of the default 'transport'
    * => 'refresh'
    *
    * Used by hook: 'customize_preview_init'
    *
    * @see add_action('customize_preview_init',$func)
    * @since Bootstrap Canvas WP 1.0
    */
    public static function live_preview() {
        wp_enqueue_script(
            'bootstrapcanvaswp-themecustomizer', // Give the script a unique ID
            get_template_directory_uri() . '/js/theme-customizer.js', // Define the path to the JS file
            array(  'jquery', 'customize-preview' ), // Define dependencies
            '', // Define a version (optional)
            true // Specify whether to put in footer (leave this true)
        );
    }

    /**
     * This will generate a line of CSS for use in header output. If the setting
     * ($mod_name) has no defined value, the CSS will not be output.
     *
     * @uses get_theme_mod()
     * @param string $selector CSS selector
     * @param string $style The name of the CSS *property* to modify
     * @param string $mod_name The name of the 'theme_mod' option to fetch
     * @param string $prefix Optional. Anything that needs to be output before the CSS property
     * @param string $postfix Optional. Anything that needs to be output after the CSS property
     * @param bool $echo Optional. Whether to print directly to the page (default: true).
     * @return string Returns a single line of CSS with selectors and a property.
     * @since Bootstrap Canvas WP 1.0
     */
    public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
        $return = '';
        $mod = get_theme_mod($mod_name);
        if ( ! empty( $mod ) ) {
            $return = sprintf('%s { %s:%s; }',
                              $selector,
                              $style,
                              $prefix.$mod.$postfix
                             );
            if ( $echo ) {
                echo $return;
            }
        }
        return $return;
    }
}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'Bootstrap_Canvas_WP_Customize' , 'register' ) );

// Output custom CSS to live site
add_action( 'wp_head' , array( 'Bootstrap_Canvas_WP_Customize' , 'header_output' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'Bootstrap_Canvas_WP_Customize' , 'live_preview' ) );

/**
 * Sanitize Customizer Font Selections
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_sanitize_font_selection( $input ) {
    $valid = array(
        'arial, helvetica, sans-serif'                     => 'Arial',
        'arial black, gadget, sans-serif'                  => 'Arial Black',
        'comic sans ms, cursive, sans-serif'               => 'Comic Sans MS',
        'courier new, courier, monospace'                  => 'Courier New',
        'georgia, serif'                                   => 'Georgia',
        'impact, charcoal, sans-serif'                     => 'Impact',
        'lucida console, monaco, monospace'                => 'Lucida Console',
        'lucida sans unicode, lucida grande, sans-serif'   => 'Lucida Sans Unicode',
        'palatino linotype, book antiqua, palatino, serif' => 'Palatino Linotype',
        'tahoma, geneva, sans-serif'                       => 'Tahoma',
        'times new roman, times, serif'                    => 'Times New Roman',
        'trebuchet ms, helvetica, sans-serif'              => 'Trebuchet MS',
        'verdana, geneva, sans-serif'                      => 'Verdana',
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

/**
 * Sanitize Customizer Checkbox
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

if ( ! function_exists( 'bootstrapcanvaswp_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own bootstrapcanvaswp_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
    case 'pingback' :
    case 'trackback' :
    // Display trackbacks differently than normal comments.
?> vcx
<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
    <p><?php _e( 'Pingback:', 'bootstrapcanvaswp' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'bootstrapcanvaswp' ), '<span class="comment-meta edit-link"><span class="glyphicon glyphicon-pencil"></span> ', '</span>' ); ?></p>
    <?php
    break;
    default :
    // Proceed with normal comments.
    global $post;
    ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
    <article id="comment-<?php comment_ID(); ?>" class="comment">
        <header class="comment-meta comment-author vcard">
            <?php
    echo get_avatar( $comment, 44 );
    printf( ' <cite><b class="fn">%1$s</b> %2$s</cite>',
           get_comment_author_link(),
           // If current post author is also comment author, make it known visually.
           ( $comment->user_id === $post->post_author ) ? '<span>' . __( 'Post author', 'bootstrapcanvaswp' ) . '</span>' : ''
          );
    printf( '<span class="glyphicon glyphicon-calendar"></span> <a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
           esc_url( get_comment_link( $comment->comment_ID ) ),
           get_comment_time( 'c' ),
           /* translators: 1: date, 2: time */
           sprintf( __( '%1$s at %2$s', 'bootstrapcanvaswp' ), get_comment_date(), get_comment_time() )
          );
            ?>
        </header><!-- .comment-meta -->

        <?php if ( '0' == $comment->comment_approved ) : ?>
        <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'bootstrapcanvaswp' ); ?></p>
        <?php endif; ?>

        <section class="comment-content comment">
            <?php comment_text(); ?>
            <?php edit_comment_link( __( 'Edit', 'bootstrapcanvaswp' ), '<p class="comment-meta edit-link"><span class="glyphicon glyphicon-pencil"></span> ', '</p>' ); ?>
        </section><!-- .comment-content -->

        <div class="reply">
            <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'bootstrapcanvaswp' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </div><!-- .reply -->
        <hr />
    </article><!-- #comment-## -->
    <?php
    break;
    endswitch; // end comment_type check
}
endif;




function consultar_muestras()
{
    global $wpdb;
    $datos_usuario = wp_get_current_user();

    require_once "inc/soap/nusoap/nusoap.php";
    $client = new nusoap_client(URL_WEB_SERVICE);




    $registro_muestra = $client->call('registroMuestra',array('muestra'=>array('visitorid'=>562247,'idproducto'=>14683)));



    //    echo "<pre>";
    // print_r($registro_muestra);
    // echo "</pre>";


    $producto = $client->call('getProductos');


    foreach ($producto as $pro)
    {


        $datos_pro=array(
            'id_muestra' => utf8_encode($pro['idproducto']),
            'nombre_muestra' => utf8_encode($pro['nombre'])
        );
        $registro=$wpdb->insert('wp_muestras', $datos_pro);
    }


    //  $to = 'jeferson.montoya@arkix.com';
    // $subject = 'solicitud de muestra';
    // $message = 'He solicitado el producto tal';

    // $headers = array(
    //  'From: Mas vivas <masvivas@prueba.com>;',
    //     'BCC: My Other Name <myothername@example.com>;',
    // );

    // wp_mail( $to, $subject, $message, $headers );




    $consulta = "SELECT * FROM wp_muestras WHERE estado=1" ;
    $resultado = $wpdb->get_results( $consulta );


    // $consulta_usu = "SELECT * FROM wp_pedidos WHERE id_usuario=".$datos_usuario->ID."" ;
    // $resultado_usu = $wpdb->get_results( $consulta_usu );

    $html.= "<div id='mensaje_muestra' style='display:none;'>Tu muestra fue pedida con éxito</div>";

    // if(empty($resultado_usu))
    // {

    $html.= "<form id='frm_solicitar_muestra' method='post'>";


    foreach ($resultado as $key => $value)
    {

        $html.="<div class='ax_muestra ".$value->id_muestra."'>


         <img class='ax_nombre_clase' src='".$value->ruta_img."' width='224' height='168'>


         <input type='radio' id='".$value->id_muestra."' name='muestra' id='muestra' value='".$value->id_muestra."'>


         </div>";
    }



    $html.= "<input type='hidden' value='".$datos_usuario->ID."' id='id_usuario' name='id_usuario'>";

    $html.= "<input type='submit' value='Solicitar muestra' id='btn_smuestra' >";




    $html.= "</form>";



    return $html;

    // }

}

/*add_shortcode('muestra', 'consultar_muestras');*/






add_action('admin_enqueue_scripts', 'upload_script');

function upload_script(){
    wp_enqueue_media ();
    // Esto para cargarlo desde la carpeta js de nuestro plugin, si lo tenemos alojado el otro sitio cambiar url
    wp_enqueue_script( 'uploader',plugins_url( '/simple-table-manager/js/subir_archivos_multimedia.js' ));
}



/*
 * Evitar que los usuarios ingresen al administrador
 */
function themeblvd_disable_admin_bar() {
    if (!current_user_can('edit_posts')) {
        add_filter('show_admin_bar', '__return_false');
    }
}
add_action( 'after_setup_theme', 'themeblvd_disable_admin_bar' );

function themeblvd_redirect_admin(){
    if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
        wp_redirect(site_url());
        exit;
    }
}
add_action('admin_init','themeblvd_redirect_admin');

// cambiar estilo de tiendas
add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl_settings;

    $listing_template = '<li data-store-id="<%= id %>">' . "\r\n";
    $listing_template .= "\t\t" . '<div>' . "\r\n";
    $listing_template .= "\t\t\t" . '<p class="ax-direcciones"><a href="<%= url %>"><%= thumb %></a>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<h4>'. wpsl_store_header_template( 'listing' ) . '</h4>' . "\r\n"; // Check which header format we use
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street-az"><%= address %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n"; // Use the correct address format
    //$listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    $listing_template .= "\t\t\t" . wpsl_more_info_template() . "\r\n"; // Check if we need to show the 'More Info' link and info
    $listing_template .= "\t\t" . '</div>' . "\r\n";

    if ( !$wpsl_settings['hide_distance'] ) {
        $listing_template .= "\t\t" . '<%= distance %> ' . esc_html( $wpsl_settings['distance_unit'] ) . '' . "\r\n";
    }

    //$listing_template .= "\t\t" . '<%= createDirectionUrl() %>' . "\r\n";
    $listing_template .= "\t" . '</li>';

    return $listing_template;
}

add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );

function custom_info_window_template() {

    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window ax-info-window">' . "\r\n";
    $info_window_template .= "\t\t" . '<p class="ax-infow-p">' . "\r\n";
    $info_window_template .= "\t\t\t" .'<h2 class="ax-infow-title">'.  wpsl_store_header_template() .'</h2>'. "\r\n";
    $info_window_template .= "\t\t\t" . '<span class="ax-infow-address"><%= address %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= address2 %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span class="ax-infow-city">' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
    $info_window_template .= "\t\t" . '</p>' . "\r\n";
    $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";

    return $info_window_template;
}

function bavota_breadcrumbs() {
    global $post;
    echo '<nav class="breadcrumb">';
    echo '<a href="'.get_site_url().'">Inicio</a><span class="divider">/</span>';
    if (is_home()){

        $page_for_posts_id = get_option('page_for_posts');
        if ( $page_for_posts_id ) {
            $blog = get_post($page_for_posts_id);
            $id_comunidad = wp_get_post_parent_id($blog->ID);
            $com = get_post($id_comunidad);
            echo '<a class="ax-breadcrumbs" href="'.get_permalink($com).'">'.get_the_title($com).'</a><span class="divider">/</span>';
            echo '<a class="ax-breadcrumbs" href="'.get_permalink($blog).'">'.get_the_title($blog).'</a>';
        }
    }

    if (!is_home()) {

        if (is_category() || is_single()) {
            if (is_category() && !in_category('uncategorized') ) {
                the_category('','<span class="divider">/</span>');
            }
            if (is_single() && in_category('blog')) {
                //the_category('<span class="divider">/</span>','');
                echo '<a class="ax-breadcrumbs" href="'.get_site_url().'/comunidad/blog">Blog</a>';
                the_title('<span class="divider">/</span>','');
            }
            if (is_single() && !in_category('blog')) {
                the_title('','');
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '<a class="ax-breadcrumbs" href="'.get_permalink($ancestor).'">'.get_the_title($ancestor).'</a><span class="divider">/</span>';
                }
                echo $output;
                echo '<a class="ax-breadcrumbs" href="'.get_permalink().'">'.$title.'</a>';
            } else {
                echo '<a class="ax-breadcrumbs" href="'.get_permalink().'">'.get_the_title().'</a>';
            }
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</nav>';
}

if(function_exists('add_image_size')){
    add_image_size('blog_destacado',954,405, true);
    add_image_size('blog_image',700,340, true);
}

add_filter('image_size_names_choose', 'image_sizes_blog');
function image_sizes_blog($sizes) {
    $addsizes = array(
        "blog_destacado" => __( "Versión para usarla en destacado"),
        "blog_image" => __("imagen predeterminada del blog.")
    );
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

// Add custom field to entry of blog
function set_default_meta($post_ID){
    $current_field_value = get_post_meta($post_ID,'pregunta_comentario',true);
    $default_meta = 'Pregunta'; // value
    if ($current_field_value == '' && !wp_is_post_revision($post_ID)){
        add_post_meta($post_ID,'pregunta_comentario',$default_meta,true);
    }
    return $post_ID;
}
add_action('wp_insert_post','set_default_meta');


/**
 *  Replace OG Tags
 */
if(isset($_GET['ref'])){
  add_filter( 'wpseo_opengraph_title', 'change_title' );
  add_filter( 'wpseo_opengraph_desc', 'change_desc' );
  add_filter( 'wpseo_opengraph_url', 'change_url' );
  add_filter( 'wpseo_opengraph_image', 'change_image' );
  add_action( 'wpseo_add_opengraph_images', 'add_images' );

  function change_title( $title) {
    $model = getModel();
      $title = __( 'Me parezco a '.$model['text'], 'change_textdomain' );
      return $title;
  }

  function change_desc( $desc ) {
    $desc = __( 'Descubre tú también a quién de ellas te pareces, ¡haz clic aquí!.', 'change_textdomain');
    return $desc;
  }
  function change_url( $url ) {
    $url = "www.lightsbytena.co/lightson/test/?ref=".$_GET['ref'];
    return $url;
  }
  function change_image( $image ) {
    $model = getModel();
    $image = $model['image'];
    return $image;
  }
  function add_images( $object ) {
    $model = getModel();
    $image = $model['image'];
    $object->add_image( $image );
  }
}


/**
 *  Get Model By param REF
 */
function getModel(){
  switch ($_GET['ref']) {
    case 'aleja':
        $model['image'] = 'http://lightsbytena.co/wp-content/themes/lights/img/img_testaleja.png';
        $model['name'] = "Alejandra Azcarate";
        $model['text'] = '¡Aleja, la aventurera!';
        break;

      case 'vero':
        $model['image'] = 'http://lightsbytena.co/wp-content/themes/lights/img/img_testvero.png';
        $model['name'] = "Veronica Orozco";
        $model['text'] = '¡Vero, la amiga leal!';
        break;

      case 'caro':
        $model['image'] = 'http://lightsbytena.co/wp-content/themes/lights/img/img_testcaro.png';
        $model['name'] = "Carolina Cuervo";
        $model['text'] = '¡Caro, la decidida!';
        break;
    }

    return $model;
}

function add_header_seguridad() {
    header("X-Content-Type-Options: nosniff");
    header("X-Frame-Options: DENY");
    header("X-XSS-Protection: 1; mode=block");
    header("Strict-Transport-Security: max-age=157680000");
    header("X-Permitted-Cross-Domain-Policies: master-only");
    header("X-Content-Security-Policy: allow 'self';");
}
add_action('send_headers', 'add_header_seguridad');

// Removes some links from the header
function remove_headlinks_and_emojis() {
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'rsd_link' );
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'start_post_rel_link' );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'wp_shortlink_wp_head' );
    remove_action( 'wp_head', 'adjacent_posts_rel_link' );
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'parent_post_rel_link' );
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action( 'init', 'remove_headlinks_and_emojis' );

// Eliminar las versiones de los parámetros en las URLs
function remove_version_params( $src ){
    $parts = explode( '?ver', $src );
    return $parts[0];
}
add_filter( 'script_loader_src', 'remove_version_params', 15, 1 );
add_filter( 'style_loader_src', 'remove_version_params', 15, 1 );


function wp_get_attachment( $attachment_id ) {

    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}