
<?php

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'año',
        'm' => 'mes',
        'w' => 'semana',
        'd' => 'día',
        'h' => 'hora',
        'i' => 'minuto',
        's' => 'segundo',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? 'Hace ' . implode(', ', $string)  : 'Justo ahora';
}

session_start();

get_header();

$post_id = get_the_ID();

?>

<div class="ax-blog ax-blog-interna">
 	<div class="container">
        <div class="col-md-9 ax-articulo-interna">
            <?php if ( have_posts() ) : the_post(); ?>
              <section>
                <figure>
                <?php if (has_post_thumbnail()) {
                    $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id( $post->ID ), '' );
                ?>
                 <img src="<?php echo $imagen_cabecera['src']; ?>" alt="<?php echo $imagen_cabecera['alt']; ?>" title="<?php echo $imagen_cabecera['title']; ?>" class="img-responsive wp-post-image" sizes="(max-width: 954px) 100vw, 954px" width="954" height="464">
                <?php } ?>
                </figure>
                <div class="ax-content">
                    <h1><?php the_title();?></h1>
                    <div class="ax-info-date">
                        <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j / F / Y'); ?></time>
                        <?php echo DISPLAY_ULTIMATE_PLUS(); ?>
                    </div>

                    <?php the_content(); ?>
                </div>
            </section>
            <?php else : ?>
              <p><?php _e('Ups!, esta entrada no existe.'); ?></p>
            <?php endif; ?>

            <div class="ax-comunidad-comentarios">
                <form id="form_register" name="form_register" name="registerform" method="post">
                    <h3><?php echo get_post_custom_values('pregunta_comentario')[0]; ?></h3>
                    <div class="form-group ">
                      <div class="col-md-12">
                        <div class="ax-subircomentarios">
                            <textarea id="reg_subircomentario" type="text" name="reg_subircomentario" class="ax-input" parentid="" placeholder="Escribe tu comentario" required></textarea>
                            <input type="hidden" id="page_type" value="<?= $post_id ?>">
                            <span class="btn btn-default btn-file">
                               <i class="fa fa-camera" aria-hidden="true"></i><input type="file" name=" " id="my_image_upload_comment_p" accept="image/x-png, image/gif, image/jpeg" multiple="false"  />
                            </span>
                            <div id="msgparcomment"></div>
                        </div>
                        <div id="ax-load"></div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group" style="text-align:center;">
                            <input id="reg-submit" type="button" class="ax-button" value="Publicar">
                         </div>
                      </div>
                    </div>
                </form>
            </div>
             <div id="ax-all-comments" class="col-md-12"></div>
        </div>

        <div class="col-md-3 ax-sidebar-cont">
            <div class="entry-post-nav group">
            </div>
            <div class="ax-first-post">
                <?php
                $the_query = new WP_Query( array(
                    'post_type' => 'post',
                    'posts_per_page' => 1,
                    'post__not_in' => array($post_id),
                    'orderby'        => 'rand',
                ) );

                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) { ?>
                            <?php $the_query->the_post(); ?>
                                <article>
                                    <figure>
                                        <?php if (has_post_thumbnail()) {
                                            $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id( $post->ID ), '' );
                                        ?>
                                        <img class="img-responsive center-block" alt="500x500" src="<?php echo $imagen_cabecera['src']; ?>" alt="<?php echo $imagen_cabecera['alt']; ?>" title="<?php echo $imagen_cabecera['title']; ?>">
                                        <?php } ?>
                                    </figure>
                                     <div class="item">
                                      <h2><?php the_title(); ?></h2>
                                      <a href="<?php the_permalink(); ?>">Conoce más...</a>
                                    </div>
                                  </article>
                        <?php }
                    }
                    wp_reset_postdata();
                ?>
            </div>
            <div class="ax-sidebar-comunidad">
                <?php

            $pagec = get_page_by_title('comunidad');

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_date',
                'hierarchical' => 1,
                'exclude' => '1097',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => $pagec->ID,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish'
            );

            $comunitypages = get_pages($args);
            $numpost = count($comunitypages);
            $randnum = rand(0,$numpost-1);
            $comunityp = '';

            foreach ($comunitypages as $key => $page) {

                if ($key == $randnum) {
                    $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id($page->ID) );
                    $comunityp .= '<div class="ax-comunidad ax-comunidad-blog">';
                    $comunityp .= '<img class="img-responsive center-block" alt="500x500" src="'.$imagen_cabecera['src'].'" alt="'.$imagen_cabecera['alt'].'" title="'.$imagen_cabecera['title'].'">
                    </div>
                        <div class="item">
                            <h2>'.$page->post_title.'</h2>
                            <a class="boton" target="_blank" href="'.get_page_link($page->ID).'" target="blank">Conoce más</a>
                        </div>';
                }
            }
            echo $comunityp;

            ?>
            </div>
            <a href="/solicitar-muestra/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/muestra-gratis.png" alt="enlace muestra gratis" title="Muestra gratis"></a>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlsolicitar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="row">
        <div class="modal-content inicio">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Iniciar sesión</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="email" class="form-control ax-input" id="reg_email_muestra" placeholder="Ingresa tu correo">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control ax-input" id="reg_numdocumento_muestra" placeholder="Ingresa tu identificación">
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <button id="actualizar" type="button" onclick="ax.updateUserData(true)" class="btn btn-default ax-button">Actualiza tus datos</button>
                    </div>
                    <div class="col-md-6">
                      <button id="regcontinuar" type="button" onclick="ax.loginMuestra(true)" class="btn btn-default ax-button">Ingresa</button>
                    </div>
                  </div>
                </div>
                <div id="axmsgregister_muestra"></div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="ax-back-opa">
    <div class="ax-preload-content">
        <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
    </div>
</div>

<script type="text/javascript">
    var idcurrentpost = "<?php echo get_the_ID(); ?>";

    $(document).ready(function() {
        ax.getComments();

        $("iframe[src*='youtube']").each(function () {
            $(this).attr('src', $(this).attr('src')+'?version=3&autohide=0&autoplay=0');
        });

        $("#my_image_upload_comment_p").change(function () {
            if (this.value != '') {
                var size = (this.files[0].size / (1024*1024)).toFixed(2);

                if (size > 2) {
                    $('#msgparcomment').empty();
                    $('#msgparcomment').append('<p>No se admiten imágenes con un peso superior a 2mb</p>');
                    this.value = '';
                    $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                    $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                    $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                    $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
                } else {
                    $("#form_register .ax-subircomentarios i.fa").removeClass('fa-camera');
                    $("#form_register .ax-subircomentarios i.fa").addClass('fa-check');
                    $("#form_register .ax-subircomentarios i.fa").css('color','#fff');
                    $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#ad004b');
                    $("#msgparcomment").empty();
                }
            } else {
                $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
            }
        });
        //$("#reg_subircomentario").emojioneArea();

        <?php
            if (isset($_SESSION['comment']) && isset($_SESSION['inputcomment'])) {
                echo '$("'.$_SESSION['inputcomment'].'").val("'.$_SESSION['comment'].'");';
            }
        ?>
    });

    $(".subcomment").keyup(function(event){
    if(event.keyCode == 13){
        ax.submitSubComment($(this));
    }
});


$('#reg-submit').on('click', function(){
   ax.submitComment("#reg_subircomentario");
});

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1130891536975805',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<?php get_footer(); ?>