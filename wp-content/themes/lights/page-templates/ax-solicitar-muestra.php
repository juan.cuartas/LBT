<?php
/*
Template Name: Solicitar muestra TENA
*/
session_start();
global $wpdb;
$datos_usuario = wp_get_current_user();
require_once(__DIR__.'/../inc/soap/nusoap/nusoap.php');
$client = new nusoap_client(URL_WEB_SERVICE);
//$registro_muestra = $client->call('registroMuestra',array('muestra'=>array('visitorid'=>562247,'idproducto'=>14683)));
$html = '';
$validate = $wpdb->get_results("SELECT * FROM wp_muestras");
/*$producto = $client->call('getProductos');*/ $producto = $wpdb->get_results("SELECT * FROM wp_productos ORDER BY nombre");

if (!empty($producto)) {
    if (empty($validate)) {
        foreach ($producto as $pro) {
            $datos_pro = [
                'id_muestra' => utf8_encode($pro['idproducto']),
                'nombre_muestra' => utf8_encode($pro['nombre']),
                'estado' => '1'
            ];
            $registro = $wpdb->insert('wp_muestras', $datos_pro);
        }
    } else {
        $currentprod = array();
        foreach ($validate as $crrpro) {
            $crrpro = (array)$crrpro;
            array_push($currentprod,['idproducto' => $crrpro['id_muestra'], 'nombre' => $crrpro['nombre_muestra']]);
        }

        $tprod = count($producto);
        $cprod = count($currentprod);

        if ($tprod > $cprod) {
            foreach ($producto as $serpro) {
                foreach ($currentprod as $curprod) {
                    if (!in_array($serpro['idproducto'], $curprod)) {
                        $datos_pro = [
                            'id_muestra' => utf8_encode($serpro['idproducto']),
                            'nombre_muestra' => utf8_encode($serpro['nombre']),
                            'estado' => '1'
                        ];
                        $wpdb->insert('wp_muestras', $datos_pro);
                    }
                }
            }
        } else if ($cprod > $tprod) {
            $cont = 1;
            foreach ($currentprod as $curprod) {
                $delete = true;

                foreach ($producto as $serpro) {
                    if (in_array($curprod['idproducto'],$serpro)) {
                        $delete = false;
                        break;
                    }
                }

                if ($delete) {
                    $wpdb->query('DELETE FROM wp_muestras WHERE id_muestra = "'.$curprod['idproducto'].'"');
                }
            }
        }
    }
} else {
    $html.= "<div id='mensaje_muestra'>Error al consultar productos</div>";
}

$consulta = "SELECT * FROM wp_muestras WHERE estado=1";
$resultado = $wpdb->get_results($consulta);

if (empty($resultado)) {
    $html.= "<div id='mensaje_muestra'>No hay productos disponibles</div>";
} else {
    $html.= "<div id='mensaje_muestra' style='display:none;'>Tu muestra fue pedida con éxito</div>";
    $html.= "<form id='frm_solicitar_muestra' method='post'>";
    foreach ($resultado as $key => $value) {
        $html .= "<div class='ax_muestra col-md-4 ax-".$value->id_muestra."'>
        <div>";
        if (empty($value->url_muestra)) {
            $html .= "<img class='ax_nombre_clase' src='".$value->ruta_img."' width='auto' height='auto'>";
        } else {
            $html .= "<a href='".$value->url_muestra."' target='_blank'><img class='ax_nombre_clase' src='".$value->ruta_img."' width='auto' height='auto'></a>";
        }
        $html .= "<div class='checkbox'>
        <label class='custom-control custom-checkbox'>
        <input id='".$value->id_muestra."' type='checkbox' name='muestra[]' class='custom-control-input' value='".$value->id_muestra."'>
        <span class='custom-control-indicator'></span>";
        if (empty($value->url_muestra)) {
            $html .= "<p>".$value->nombre_muestra."</p>";
        } else {
            $html .= "<p><a href='".$value->url_muestra."' target='_blank'>".$value->nombre_muestra."</a></p>";
        }
        $html .= "</label></div></div></div>";
    }
    $html .= "<div class='col-md-12 ax-botonmuestra'><input type='hidden' value='".$datos_usuario->ID."' id='id_usuario' name='id_usuario'><input type='button' value='Solicitar' id='btn_smuestra' onclick='ax.registerProduct()' class='ax-button btn btn-default'></div>";
    $html.= "</form>";
}

$allmuestras = $html;
get_header(); ?>

<div id="pixelsS"></div>
<div id="pixelsS2"></div>
<div class="ax-bg-t">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                <?php endwhile;?>
                <?php endif; ?>
            </div>

            <div class="col-md-4"></div>


            <div class="ax-cont-muestras">
                <div class="col-md-12">
                    <h5>Selecciona el producto</h5>
                    <p>Tienes la opción de solicitar una muestra gratis por producto. Si deseas que te enviemos más de un producto debes seleccionar uno por uno y dar clic en el botón solicitar.</p>

                    <?php echo $allmuestras; ?>

                    <div class="col-md-12">
                        <div id="msgmuestra"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlsolicitar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>Para solicitar </p>
                <h2>tu muestra gratis</h2>
                <p>debes iniciar sesión.</p>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="email" class="form-control ax-input" id="reg_email_muestra" placeholder="Ingresa tu correo electrónico">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control ax-input" id="reg_numdocumento_muestra" placeholder="Ingresa tu identificación">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <button id="actualizar" type="button" onclick="ax.updateUserDataM(true)" class="btn btn-default ax-button">Actualiza tus datos</button>
                        </div>
                        <div class="col-md-6">
                            <button id="regcontinuar" type="button" onclick="ax.loginMuestra(true)" class="btn btn-default ax-button">Ingresa</button>
                        </div>
                    </div>
                </div>
                <div id="axmsgregister_muestra"></div>
            </div>
            <div class="modal-footer">
                <h4>¿Aún no haces parte de la <br>comunidad Lights by TENA?</h4>
                <p>Si aún no cuentas con un usuario y una contraseña, regístrate, inicia sesión y pide tu muestra gratis.</p>
                <div class="ax-button"><a href="<?php echo get_site_url();?>/registrate">Regístrate y sé parte</a></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlresponse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="msgresponse" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn ax-button" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="ax-back-opa">
    <div class="ax-preload-content">
        <img class="ax-preload-init" src="/wp-content/themes/masvivas/img/ripple.svg"/>
    </div>
</div>
<?php get_footer(); ?>
<script type="text/javascript">
    $(".ax-back-opa").css("display", "block");

    $( window ).on( "load", function() {
        $(".ax-back-opa").css("display", "none");
    });

    $(document).ready(function() {
        ax.getInterest();
        //ax.getTypesDocument();
        <?php
        if (isset($_SESSION['idprodmuestra']) && isset($_SESSION['idusuario'])) {
            echo "ax.registerProduct(".$_SESSION['idprodmuestra'].");";
            unset($_SESSION['idprodmuestra']);
        }
        ?>
    });
</script>

