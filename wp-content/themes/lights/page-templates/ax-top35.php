<?php


/*
Template Name: Plantilla top 35
*/session_start();

$rankingmed = array();
$rankingbog = array();

$consulta_u_ranking = "SELECT r.id_usuario, count(r.id_usuario) as count, ut.nombre,ut.apellido from wp_referidos r
JOIN wp_users_tena ut ON r.id_usuario = ut.id_usuario
WHERE ut.ciudad = 'Medellin' order by count desc";
$resultado_u_ranking = $wpdb->get_results( $consulta_u_ranking);
if (isset($resultado_u_ranking)) {
 $disab='disabled';
}else{
   $disab='';
}

$valor_referencia = $resultado_u_ranking[0]->count;

if ($valor_referencia > 0) {
    foreach ($resultado_u_ranking as $key => $value){
        $rankingmed[] = array($value->nombre . $value->apellido, intval($value->count * 100 / $valor_referencia));
    }   
}

$consulta_u_ranking = "SELECT r.id_usuario, count(r.id_usuario) as count, ut.nombre,ut.apellido from wp_referidos r
JOIN wp_users_tena ut ON r.id_usuario = ut.id_usuario
WHERE ut.ciudad = 'Bogota' order by count desc";
$resultado_u_ranking = $wpdb->get_results( $consulta_u_ranking);
if (isset($resultado_u_ranking)) {
 $disab='disabled';
}else{
   $disab='';
}

$valor_referencia = $resultado_u_ranking[0]->count;

if ($valor_referencia > 0) {
    foreach ($resultado_u_ranking as $key => $value){
        $rankingbog[] = array($value->nombre . $value->apellido, intval($value->count * 100 / $valor_referencia));
    }
}

get_header(); ?>

<div class="ax-bg-t">
    <div class="ax-cont-top-vivas">
        <div class="container">
            <div class="row">
                <div class="ax-cont-info">
                   <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                        <hgroup>
                           <h2><?php the_title(); ?></h2>
                        </hgroup>
                        <?php the_content(); ?>
                        <?php endwhile;?>
                    <?php endif; ?>
                    <div class="ax-cont-rainting">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#medellin">Medellín</a></li>
                            <li><a data-toggle="tab" href="#bogota">Bogotá</a></li>
                        </ul>
                        <div class="tab-content ax-cont-top">
                            <div id="medellin" class="tab-pane fade in active">

                            <?php
                            foreach($rankingmed as $item){
                                echo $html = '<div class="item clearfix">
                                    <div class="col-md-3"><p>'.$item[0].'</p></div>
                                    <div class="col-md-7">
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="'.$item[1].'"
                                          aria-valuemin="0" aria-valuemax="100" style="width:'.$item[1].'%">
                                            <span class="sr-only">'.$item[1].'% Complete</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-2"><p>'.$item[1].'%</p></div>
                                </div>';
                            }
                            
                            ?>
                            </div>
                            <div id="bogota" class="tab-pane fade">
                            <?php
                            foreach($rankingbog as $item){
                                echo $html = '<div class="item clearfix">
                                    <div class="col-md-3"><p>'.$item[0].'</p></div>
                                    <div class="col-md-7">
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="'.$item[1].'"
                                          aria-valuemin="0" aria-valuemax="100" style="width:'.$item[1].'%">
                                            <span class="sr-only">'.$item[1].'% Complete</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-2"><p>'.$item[1].'%</p></div>
                                </div>';
                            }
                            
                            ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.row -->
        </div><!-- /.containereste -->
        
        <div class="ax-cont-anuncio">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                       <a href="/mujeres-con-experiencias/"><div class="ax-banner-1">
                           <div class="cont-text">
                               <p>Inspírate, anímate a ingresar y conocer más</p>
                                <span>Mujeres con experiencias</span>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <a href="/comunidad/"><div class="ax-banner-2">
                           <div class="cont-text">
                                <p>Descubre por qué muchas mujeres están más vivas que nunca en</p>
                                <span>Nuestra comunidad</span>
                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<?php get_footer(); ?>