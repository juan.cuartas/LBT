<?php

/*
Template Name: Test TENA
*/

get_header(); 
?>

<div class="ax-test">
	<!-- <h2><?php the_title(); ?></h2> -->
	<?php
        $id_page = $post->ID;
        $thumbnail_id = get_post_thumbnail_id($post->ID);
        $thumbnail_image = get_post($thumbnail_id);
        $title = $thumbnail_image->post_title;
        the_post_thumbnail('full', array('title' => $title ,'class' => "img-responsive center-block"));
    ?>
 	<div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
		
		<div class="row">
	      	<div class="col-md-12">
	            <?php the_content(); ?>
	        </div>
	 	</div>
	 	<?php endwhile;?>
    <?php endif; ?>
	</div>
</div>
<?php
    get_footer();
?>

