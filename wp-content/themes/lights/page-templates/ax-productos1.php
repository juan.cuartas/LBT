<?php

/*
Template Name: Protectores LIGHTS TENA
*/

get_header();

?>

    <div class="ax-productos">
        <div class="container">
            <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
            <h2>
                <?php the_title(); ?>
            </h2>
            <div class="row">
                <div class="col-md-6">
                    <?php the_content(); ?>
                </div>

                <div class="col-md-6">
                    <img class="img-responsive" alt="" src="/wp-content/themes/lights/img/ax-producto1.png">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Elige cualquiera de las 3 presentaciones, la que te haga sentir más cómoda y segura:</h3>
                </div>
            </div>
            <?php endwhile;?>
            <?php endif; ?>
        </div>
    </div>

    <div class="ax-productos1">
        <div class="container">
            <div class="col-md-7">
                <h5>Protectores Diarios Normal</h5>
                <img class="img-responsive" alt="" src="/wp-content/themes/lights/img/ax-productos-dnormal.png">
            </div>
            <div class="col-md-5">
                <ul>
                    <li><img alt="" src="/wp-content/themes/lights/img/empaque-indiv.png"><span>Empaque individual</span></li>
                    <li><img alt="" src="/wp-content/themes/lights/img/largo-protec.png"><span>15cm.</span></li>

                    <li><img alt="" src="/wp-content/themes/lights/img/conteno-protec.png"><span>15, 50, 80 unidades.</span></li>
                    <li><img style="width: 60px;" alt="" src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/10/05144137/sello-feelFresh.png"><span>Tecnología Feel Fresh control de olor</span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ax-productos2">
        <div class="container">
            <div class="col-md-7">
                <h5>Protectores Diarios Largos con Alas</h5>
                <img class="img-responsive" alt="" src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/10/05143922/Largos-alas.png">
            </div>
            <div class="col-md-5">
                <ul>
                    <li><img alt="" src="/wp-content/themes/lights/img/empaque-indiv.png"><span>Empaque individual</span></li>
                    <li><img alt="" src="/wp-content/themes/lights/img/largo-protec.png"><span>19cm.</span></li>

                    <li><img alt="" src="/wp-content/themes/lights/img/conteno-protec.png"><span>15, 30, 80 unidades.</span></li>
                    <li><img style="width: 60px;" alt="" src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/10/05144137/sello-feelFresh.png"><span>Tecnología Feel Fresh control de olor</span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ax-productos1">
        <div class="container">
            <div class="col-md-7">
                <h5>Protectores Diarios Extralargos</h5>
                <img class="img-responsive" alt="" src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/10/05143854/Extralargos.png">
                <div class="ax-productos-info-A">
                    <ul>
                        <li>Únicos en el mercado con 21 cm de largo para una extra protección</li>
                        <li>Su tecnología Feel Fresh absorbe rápidamente todo tipo de fluidos y controla el olor y la humedad de tu zona íntima.</li>
                        <li>Con materiales hipoalergénicos que son más suaves con tu piel.</li>
                        <h3>Todos los días con tu ropa interior, usa Protectores Extralargos de la Línea Lights by TENA.</h3>
                        <h3>Complementa tu rutina de higiene con Jabón íntimo + Toallas Húmedas Íntimas.</h3>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <ul>
                    <li><img alt="" src="/wp-content/themes/lights/img/empaque-indiv.png"><span>Empaque individual</span></li>
                    <li><img alt="" src="/wp-content/themes/lights/img/largo-protec.png"><span>21cm.</span></li>
                    <li><img alt="" src="/wp-content/themes/lights/img/conteno-protec.png"><span>15, 50, 80 unidades.</span></li>
                    <li><img style="width: 60px;" alt="" src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/10/05144137/sello-feelFresh.png"><span>Tecnología Feel Fresh control de olor</span></li>
                </ul>
            </div>
        </div>
    </div>
    <?php
    get_footer();
?>