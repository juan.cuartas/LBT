<?php 

/*
Template Name: Donde comprar TENA
*/

get_header(); ?>

<div class="ax-dondecomprar">
    <div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <div class="row">
            <div class="col-md-8 ax-comprar">
                
                <p>¿Quieres usar nuestros productos? Aquí te mostramos todas las tiendas donde puedes cómpralos</p>
                <h4>Para encontrar la tienda más cercana debes:</h4>
                    <ol class="ax-iconosredes">
                    <li><span>1</span> <p>Seleccionar en el primer cuadro el departamento que quieres consultar.</p></li>
                    <li><span>2</span> <p>Seleccionar en el segundo cuadro la ciudad donde deseas consultar los puntos de venta en que están disponibles nuestros productos.</p></li>
                    <li><span>3</span> <p>Luego encontrarás un listado con las tiendas a las que te puedes acercar, con su respectiva dirección.</p></li>
                    <li><span>4</span> <p>Haz clic sobre la palabra mapa para visualizar la ubicación geográfica de los puntos de venta.</p></li>
                    </ol>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <select name="reg_departamento" id="reg_departamento" onchange="ax.getCities(this.value)" class="ax-select" required>
                    <option value="0" selected="selected">Selecciona tu departamento</option>
                </select>
            </div>
            <div class="col-md-5">
                <select name="reg_ciudad" id="reg_ciudad" class="ax-select" required>
                    <option value="0" selected="selected">Selecciona tu ciudad</option>
                </select>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="button" onclick="ax.findShop()" class="ax-button" value="Buscar">
                </div>
            </div>
        </div>


    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <div class="ax-cont-map">
            <?php the_content(); ?>
        </div>
    </div>
</div>    
<?php endwhile;?>
<?php endif; ?>






<?php get_footer(); ?>


<script type="text/javascript">
    var templateDir = "<?php bloginfo('template_directory'); ?>";
    $(document).ready(function() {
        ax.getDepartments("1");
    });
</script>