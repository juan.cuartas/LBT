<?php
/**
 * Template Name: Pagina inicio TENA
 */

    get_header();
?>
	<section class="ax-slide">
        <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
            <?php endwhile;?>
        <?php endif; ?>
    </section>

    <section class="ax-videohome">
      <div class="container">
          <div id="player"></div>
      </div>
    </section>

    <section class="ax-interes col-md-12">
      <div class="row">
        <div class="col-md-6 ax-imageninteres hidden-sm-down"></div>
        <div class="col-md-6 ax-recibeinteres">

        	<div class="col-md-8 col-sm-12">
		         <form class="form-signin">
		            <h2 class="form-signin-heading" id="nw-title"></h2>
		            <p id="nw-text"></p>
		            <div class="form-group">
		              <input type="email" id="txtEmail" class="ax-input form-control" placeholder="Ingresa tu correo electrónico" required="">
		            </div>
		            <div class="form-group">
		              <input type="number" id="txtCharter" class="ax-input form-control" placeholder="Ingresa tu identificación" required="">
		            </div>

                    <div class="checkbox">
                         <label class="custom-control custom-checkbox">
                              <input id="chkterminos" type="checkbox" class="ax-checkbox custom-control-input">
                              <span class="custom-control-indicator"></span>
                              <a href="terminos-y-condiciones/" target="_blank">Acepto los términos y condiciones</a>
                        </label>
                    </div>
		            <button id="btnSuscripcion" class="ax-button btn btn-lg btn-block" type="button">Enviar</button>
		            <p class="ax-aviso" id="msgsuscrip"></p>
		            <div id="preload"></div>
		          </form>
        	</div>
        	<div class="col-md-4 col-sm-0"></div>
        </div>
      </div>
    </section>

    <section class="ax-muestrasgratis">
      <div class="container">
        <div id="solicitar-muestra" class="col-md-6 ax-solicitar-muestra">
        </div>
        <div class="col-md-6 ax-zonaintima">
            <div id="com-image"></div>
	        <div class="row">
	          <div class="ax-zonaintima-text">
	            <h2 id="com-title"></h2>
	            <h2 id="com-text"></h2>
	            <button id="com-link" type="button" class="btn btn-default ax-button"></button>
	          </div>
	        </div>
        </div>
      </div>
    </section>

<div class="modal fade ax-notificacion" id="mdlsuscripcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="msgregistro" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <div id="redirectregistro">
                	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

<script type="text/javascript">

    $('#btnSuscripcion').click(function() {
        /*$('#mdlsuscripcion').modal('show');*/
       ax.newsLetter();
    });

    <?php if (isset($_GET['upd']) && $_GET['upd'] == 1)
        echo "$('#mdllogin').modal('show');";
    ?>
    var VIDEOHOME = $("#VIDEOHOME").attr('data-video');
    var VIDEOIMAGE = $("#VIDEOHOME").attr('data-video-image');
    var NEWSLETTER = $("#NEWSLETTER");
    var MUESTRA = $("#MUESTRA");
    var COMUNIDAD = $("#COMUNIDAD");

    if (VIDEOHOME) {
        jwplayer("player").setup({
            file: VIDEOHOME,
            width: '100%',
            height: '100%',
            aspectratio: '16:9',
            autostart: 'false',
            stretching: 'fill',
            image: VIDEOIMAGE,
            ga: '{}',
            repeat:'true',
            primary: 'html5',
            abouttext: 'Arkix',
            aboutlink: 'http://www.masvivas.com',
            fallback: 'false',
            skin: {
                   name: "stormtrooper",
                   active: "#d2005b",
                   inactive: "white",
                   background: "#ead4d8"
                }
        });
    }

    if (NEWSLETTER) {
        $("#nw-title").append(NEWSLETTER.attr('data-title'));
        $("#nw-text").append(NEWSLETTER.attr('data-text'));
    }

    if (MUESTRA) {
        $("#solicitar-muestra").append('<a href="'+MUESTRA.attr('data-link')+'"><img class="img-responsive" alt="" src="'+MUESTRA.attr('data-image')+'"></a>');
    }

    if (COMUNIDAD) {
        $("#com-title").append(COMUNIDAD.attr('data-title'));
        $("#com-image").append('<img src="'+COMUNIDAD.attr('data-image')+'" alt="">');
        $("#com-text").append(COMUNIDAD.attr('data-text'));
        $("#com-link").append(COMUNIDAD.attr('data-button-text'));
        $("#com-link").click(function () {
            window.location.href = COMUNIDAD.attr('data-link');
        });
    }
</script>
