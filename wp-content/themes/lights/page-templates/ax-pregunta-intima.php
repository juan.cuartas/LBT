<?php 

/*
Template Name: Pregunta íntima con Eva Tena
*/

get_header(); ?>

<div class="ax-preguntaintima">
    <div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <div class="row">
            <div class="col-md-12">
                <?php the_content(); ?>
            </div>
        </div>
        <?php endwhile;?>
    <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>