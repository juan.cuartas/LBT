<?php
/**
* Template Name: Historias
*
* Description: Bootstrap Canvas WP loves the no-sidebar look as much as
* you do. Use this page template to remove the sidebar from any page.
*
* Tip: to remove the sidebar from all posts and pages simply remove
* any active widgets from the Main Sidebar area, and the sidebar will
* disappear everywhere.
*
* @package Bootstrap Canvas WP
* @since Bootstrap Canvas WP 1.0
*/

get_header(); ?>

<div class="ax-bg-t">
    <div class="ax-cont-histories">
        <div class="container">
            <div class="row">
                <div class="ax-cont-historias">
                    <div class="col-md-1"></div>
                    <div class="col-md-6 col-sm-7">
                        <div class="row">
                            <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                                <hgroup>
                                    <h2><?php the_title(); ?></h2>
                                </hgroup>
                                <p><?php the_content(); ?></p>
                            <?php endwhile;?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>

            <div class="col-md-12">
                <div class="ax-imagenes-textos">
                    <hgroup>
                        <h3>Imágenes y textos</h3>
                    </hgroup>

                    <div class="ax-cont-gallery">
                        <ul id="ax-retos">

                            <?php

                            $consulta_reto = "SELECT * FROM wp_retos WHERE estado_reto=1 and rg_tequila = 0 ORDER BY id_reto DESC" ;
                            $resultado_reto = $wpdb->get_results($consulta_reto );

                            foreach ($resultado_reto as $key => $value) {

                                $consulta_usuario = "SELECT * FROM wp_users_tena WHERE id_usuario=".$value->id_usuario."" ;
                                $resultado_usuario = $wpdb->get_results($consulta_usuario );

                                $aleatorio= rand(1,3);

                                if($value->descripcion !='' && $value->url_archivo !='') {
                                    echo  $html='<li class="ax-item ax-image"><a href="javascript:void(0);"><img src="'.$value->url_archivo.'" alt="">
                                    <div class="ax-autor">
                                    <div>
                                    <h4>'.$resultado_usuario[0]->nombre.'</h4>
                                    <span>'.$resultado_usuario[0]->ciudad.'</span>
                                    <p>'.$value->descripcion.'</p>
                                    </div>
                                    </div>
                                    </a></li>';
                                } else {

                                    if($value->descripcion =='') {
                                        echo  $html='<li class="ax-item ax-image"><a href="javascript:void(0);"><img src="'.$value->url_archivo.'" alt="">
                                        <div class="ax-autor">
                                        <div>
                                        <h4>'.$resultado_usuario[0]->nombre.'</h4>
                                        <span>'.$resultado_usuario[0]->ciudad.'</span>
                                        <p>'.$value->descripcion.'</p>
                                        </div>
                                        </div>
                                        </a></li>';
                                    }

                                    if($value->url_archivo =='') {
                                        echo  $html='<li class="ax-item ax-texture-'.$aleatorio.'"><a href="javascript:void(0);"><p>'.$value->descripcion.'</p>
                                        <div class="ax-autor">
                                        <div>
                                        <h4>'.$resultado_usuario[0]->nombre.'</h4>
                                        <span>'.$resultado_usuario[0]->ciudad.'</span>
                                        </div>
                                        </div>
                                        </a></li>';
                                    }
                                }
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <div id="holder"></div>
    </div><!-- /.containereste -->

    <div class="ax-cont-videos">
        <hgroup>
            <h3>Videos</h3>
        </hgroup>
        <div class="ax-videos">
            <div class="container">
                <div class="ax-cont-video-pp">
                    <div class="ax-cont-video">

                        <div id="player2"></div>
<!--  <video controls autoplay preload="none">
<source src="https://www.youtube.com/watch?v=-jFzlT3P8FM" type="video/mp4">
<source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210201/alejandraogg.ogg" type="video/ogg">
<source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210240/alejandrawebm.webm" type="video/webm">
Your browser does not support HTML5 video.
</video>  -->
</div>
</div>
</div>
</div>
<div class="container">
    <div class="ax-cont-info-video">
        <div class="ax-cont-description">
            <p id="textVid">Alejandra Azcárate</p>
        </div>
        <div class="ax-cont-gallery">
            <div class="ax-controls-video">




                <div class="ax-lista-videos">
                    <ul id="ax-vid">

                        <?php
                        $consulta_reto_teq = "SELECT * FROM wp_retos WHERE estado_reto=1 and rg_tequila=1" ;
                        $resultado_reto_teq = $wpdb->get_results($consulta_reto_teq );
                        $html = '<li class="ax-item"><div class="ax-item-video ax-item-video-active">
                        <video poster="/wp-content/themes/lights/img/posters/mas-vivas-que-nunca.jpg" autoplay preload="none" onclick="changeVideo('."'".'https://www.youtube.com/watch?v=-jFzlT3P8FM'."'".','."'".'Alejandra Azcárate'."'".')">
                        <source src="https://www.youtube.com/watch?v=-jFzlT3P8FM" type="video/mp4">
                        </video> </div> </li>';
                        foreach ($resultado_reto_teq as $key => $value)
                        {
                            $html .= '<li class="ax-item "><div class="ax-item-video">
                            <video autoplay poster="/wp-content/themes/lights/img/posters/poster-'.$value->id_reto.'.jpg" preload="none" onclick="changeVideo('."'".$value->url_archivo."'".','."'".$value->descripcion."'".')">
                            <source src="'.$value->url_archivo.'" type="video/mp4">

                            </video> </div> </li>';

                        }

                        echo $html;
                        ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div id="holder-videos"></div>
</div>
</div>
</div>
</div>

<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/video1.js"></script>
<script>
//capture width de imagen
var widthImgExperience = $('#ax-retos .ax-item a').width();
$('.ax-autor').css('width', widthImgExperience);

$(document).ready(function() {

    $(".ax-item-video video").each(function() {
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        if (isIE) {
            this.addEventListener('loadedmetadata',function(){
                $(".ax-item-video video").each(function() {
                    this.currentTime=10;
                    this.pause();
                });
            }, false );
        } else {
            this.currentTime=10;
            this.pause();
        }
    });

    $(".ax-item-video").each(function() {

        $(this).bind('click', function () {
            $(".ax-item-video").each(function() {
                $(this).removeClass('ax-item-video-active');
            });

            $(this).addClass('ax-item-video-active');
        });
    });

    $(document).load(function() {
        $('#ax-retos').find('li').each(function(){
            $(this).find('img').each(function(){
                if($(this).width() < $(this).height()){
                    $(this).css({height: 'auto', width: '100%'});
                }else {
                    $(this).css({height: 280, width: 'auto'});
                }
            });
        });

        //capture width de imagen
        $('#ax-retos').find('li').each(function(){
            var widthImgExperience = $(this).width();
            var heightImgExperience = $(this).height();
            $(this).find('.ax-autor').css({width: widthImgExperience, height: heightImgExperience})
        });
    });
});

$("#holder a").click(function() {
    $('#ax-retos').find('li').each(function(){
        $(this).find('img').each(function(){
            if($(this).width() < $(this).height()){
                $(this).css({height: 'auto', width: '100%'});
            }else {
                $(this).css({height: 280, width: 'auto'});
            }
        });
    });

    //capture width de imagen
    $('#ax-retos').find('li').each(function(){
        var widthImgExperience = $(this).width();
        var heightImgExperience = $(this).height();
        $(this).find('.ax-autor').css({width: widthImgExperience, height: heightImgExperience})
    });
});
</script>