<?php
/**
 * Template Name: Iniciar Sesión
 *
 * Description: Bootstrap Canvas WP loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
if(!isset($_GET['id']) && isset($_SESSION['idusuario'])) {
    wp_redirect(home_url());
}

get_header(); ?>

<div class="ax-bg-t">
    <div class="ax-iniciar-sesion">
        <div class="container-custom">
            <div class="row">
                <div class="iniciar-sesion">
                    <div class="col-lg-8 col-md-7 col-sm-6">
                        <div class="content-sesion">
                            <?php
                            $id_page = $post->ID;
                            $thumbnail_id = get_post_thumbnail_id($post->ID);
                            $thumbnail_image = get_post($thumbnail_id);
                            $title = $thumbnail_image->post_title;
                            the_post_thumbnail('full', array('title' => $title ,'class' => "img-responsive center-block"));
                            ?>
                            <div class="ax-imagenes-textos">
                                <h3 class="modal-title">Mujeres de Tiempo Completo</h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-6">
                    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
                        <!-- <hgroup>
                            <h2><?php the_title(); ?></h2>
                        </hgroup> -->
                    <?php endwhile;?>
                    <?php endif; ?>
                    <?php if(!isset($_SESSION['idusuario'])): ?>
                    <div class="main-modal">
                        <div class="modal-content iniciar-sesion--modal">
                            <div class="modal-header">
                                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                <h4 class="modal-title">¿Ya viste nuestra serie web? Ingresa ahora</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="usr">Usuario</label>
                                    <input type="email" class="form-control ax-input" id="reg_email_ligthson" placeholder="Ingresa tu correo" required="true" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Contraseña</label>
                                    <input type="number" class="form-control ax-input" id="reg_numdocumento_ligthson" placeholder="Ingresa tu identificación" required="true" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <button id="regcontinuar_ligthson" type="button" onclick="ax.ingresarLogin(this)" class="btn btn-default ax-button">Inicia sesión</button>
                                        </div>
                                        <div class="col-md-12">
                                            <!--<button id="regactualizar_ligthson" type="button" onclick="ax.updateUserLogin(false,this)" class="btn btn-default ax-button">¿Quieres actualizar tus datos?</button>-->
                                        </div>
                                        <div class="col-md-12">
                                            <a href="javascript:void(0);" class="ax-recuperar-pass ax-text-rp"  onclick="$( '.modal' ).modal('hide');$('#recuperar').modal('show');">Recuperar contraseña</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="axmsgregister_ligthson"></div>
                            </div>
                        </div>
                        <div class="modal-footer ax-modal-L-on">
                            <p>Si aún no te has registrado,<a href="/registrate"> haz clic aquí</a> y disfruta cada capítulo de esta historia.</p>
                        </div>
                    </div>
                    <?php endif; ?>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.containereste -->
    </div>
</div>

<?php

if($_GET['id'] < 4){
  switch ($_GET['id']) {
    case '1':
    $shortName = "aleja";
    $videoUrl = "http://familia.streaming.s3.amazonaws.com/video-aleja-mujeres-de-tiempo-completo.mp4";
    $name = "Aleja";
    $text = '¡Aleja, la aventurera!';
    break;

    case '2':
    $shortName = "vero";
    $videoUrl = "http://familia.streaming.s3.amazonaws.com/video-vero-mujeres-de-tiempo-completo.mp4";
    $name = "Vero";
    $text = '¡Vero, la amiga leal!';
    break;

    case '3':
    $shortName = "caro";
    $videoUrl = "http://familia.streaming.s3.amazonaws.com/video-caro-mujeres-de-tiempo-completo.mp4";
    $name = "Caro";
    $text = '¡Caro, la decidida!';
    break;
    default:
    echo "<script>var showModal = false;</script>";
    break;
}
}
?>
<!-- Modal Sesion-->
<div id="myModalSesion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
      <h4 class="modal-title">Te pareces a <strong><?php print $name; ?></strong> descubre por qué</h4>
      <!-- Modal content-->
      <div class="modal-content inicio sesion-video">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cerrar</span></button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">
            <video controls autoplay controlsList="nodownload" id="video1" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
              <source src="<?php print $videoUrl; ?>" type="video/mp4">
                  Your browser doesn't support HTML5 video tag.
              </video>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
    </div>
    <div class="col-md-4 btn-modal">
      <a href="#!" data-dismiss="modal" aria-label="Close"><h4 class="modal-title"><span id="btn_registrate">Regístrate</span></h4></a>
  </div>
  <div class="col-md-8 btn-modal">
      <h4 class="modal-title"><span id="btn_login">Compartir en Facebook</span></h4>
  </div>
</div>
</div>

<!-- Modal compartir-->
<div id="myModalCompartir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <!-- Modal content-->
    <div class="modal-content inicio sesion-video">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cerrar</span></button>
    </div>
    <div class="modal-body">
        <h4>Compartido Correctamente.</h4>
    </div>
</div>
</div>
</div>

<?php get_footer(); ?>
<?php
print "<script>window.model ='".$shortName."'</script>";
?>

<script>
  $(window).load(function(){
    <?php
    if (isset($_SESSION['idusuario']))
      echo 'var isLogged = true;';
  ?>
  $('.page-template-ax-iniciar-sesion .ax-back-opa').hide();
  if(typeof showModal == 'undefined'){
      $('#myModalSesion').modal('show');
  }
  $('#myModalSesion').on('hide.bs.modal',function(){
    document.getElementById('video1').pause();
    if(typeof isLogged != 'undefined' && isLogged)
      window.location.href= '/lightson';
});

        window.user; // Informacion del usuario

        /***** Conexion con Facebook *****/
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
          // Init App
          FB.init({
            appId : '1377547122361431',
            cookie : true,
            version: 'v2.10'
        });
          // Get login status
          FB.getLoginStatus(function(response) {

          });
      });

        /***** Accion al boton login *****/
        $("#myModalSesion").on( "click", "#btn_login", function() {
          login();
      });


        /***** Abrir ventana de login o aceptacion de permisos ******/
        function login(){
          FB.login(function(response) {
              console.log("copartir");
            if (response.authResponse) { //Logueado correctamente
              shareFacebook();
            } else { // Login cancelado, no dio permisos
              $("#myModalSesion #btn_login").fadeIn();
          }
      },{scope: 'email,publish_actions'});
      }

      /***** Accion al compartir en Facebook *****/
      function shareFacebook(){
          $("body").prepend('<div id="sharing"><p>Compartiendo...</p></div>')
          FB.api('/me/?fields=id,name,email', function(response){
            window.user = response.name;
            if (!response || response.error) {
              alert("Ha ocurrido un error. Por favor intenta más tarde.")
          } else {
            FB.api('/me/feed', 'post', {
                link:'http://www.lightsbytena.co/lightson/test/?ref='+window.model
            }, function(response){
                if (!response || response.error) {
                    alert("Ha ocurrido un error compartiendo link. Por favor intentalo de nuevo.")
                } else {
                  $('#myModalSesion').modal('hide');
                  $('#myModalCompartir').modal('show');
                  $("#sharing").remove();
              }
          });
        }
    });
      };
  });
</script>
<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/video1.js"></script> -->