<?php
/*
Template Name: Comunidad TENA
*/
get_header();
?>
<div class="ax-comunidad">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-8">
				<h2><?php echo get_the_title(); ?></h2>
			    <?php echo get_post()->post_content; ?>
		    </div>
		    <div class="col-md-4"></div>
	 	</div>
	</div>
</div>
<?php
$args = array(
	'sort_order' => 'desc',
	'sort_column' => 'post_date',
	'child_of' => get_the_ID(),
	'post_parent' => 36,
	'post_type' => 'page',
	'post_status' => 'publish',
    'paged' => get_query_var( 'paged' ),
    'posts_per_page' => 10,
);

$comunitypages = new WP_Query($args);

$comunityp = '';
$cont = 1;
if ( $comunitypages->have_posts() ) {
    while ( $comunitypages->have_posts() ) {
        $comunitypages->the_post();

        if ($post->post_name != 'blog' && has_post_thumbnail()) {
            $imagen_cabecera = wp_get_attachment( get_post_thumbnail_id( $post->ID ), '' );
            if (($cont)%2 == 0){
                $comunityp .= '<div class="ax-comunidad ax-comunidad-2">';
            } else {
                $comunityp .= '<div class="ax-comunidad ax-comunidad-3">';
            }
            $comunityp .= '
            <div class="container" data-id="'.$post->post_parent.'">
                <div class="row">';
                if (($cont)%2 == 0) {
                    $comunityp .= '<div class="col-md-6 pull-right">';
                } else {
                    $comunityp .= '<div class="col-md-6">';
                }
            $comunityp .= '<img class="img-responsive center-block" src="'.$imagen_cabecera['src'].'" alt="'.$imagen_cabecera['alt'].'" title="'.$imagen_cabecera['title'].'">
                        </div>
                        <div class="col-md-6">
                            <div class="item">
                                '.$post->post_content.'
                                <a class="boton" target="_blank" href="'.get_page_link($post->ID).'" target="blank">Conoce más</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            $cont++;
        }
    }
}

?>
<div class="ax-comunidad-8">
    <h2>En esta sección compartiremos juntas experiencias de mujeres 35+ y contenidos que nos harán sentir más vivas que nunca.</h2>
</div>
<div id="comunity-pages" class="paginador-general">
    <?php echo $comunityp; ?>
        <div class="container">
            <?php
            $big = 999999999; // need an unlikely integer

            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $comunitypages->max_num_pages
            ) );

            wp_reset_postdata();
            ?>
        </div>

</div>

<?php
get_footer();
?>

