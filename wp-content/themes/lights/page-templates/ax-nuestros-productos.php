<?php

/*
Template Name: Nuestros productos TENA
*/

get_header();

?>

<div class="ax-nuestrosproductos">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12">
				<h2><?php echo get_the_title(); ?></h2>
			    <?php echo get_post()->post_content; ?>
		    </div>
	 	</div>
	</div>

</div>

<?php 
$args = array(
	'sort_order' => 'asc',
	'sort_column' => 'post_date',
	'hierarchical' => 1,
	'exclude' => '',
	'include' => '',
	'meta_key' => '',
	'meta_value' => '',
	'authors' => '',
	'child_of' => get_the_ID(),
	'parent' => -1,
	'exclude_tree' => '',
	'number' => '',
	'offset' => 0,
	'post_type' => 'page',
	'post_status' => 'publish'
); 

$comunitypages = get_pages($args);
$comunityp = '';

foreach ($comunitypages as $key => $page) {
    
    $post_thumbnail_id = get_post_thumbnail_id($page->ID);
    $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
    
    if (($key+1)%2 == 0){
        $comunityp .= '<div class="ax-comunidad ax-comunidad-2">';
    } else {
        $comunityp .= '<div class="ax-comunidad ax-comunidad-3">';
    }
    
    $comunityp .= '
        <div class="container">
            <div class="row ax-linea">';
    
            if (($key+1)%2 == 0) {
                $comunityp .= '<div class="col-md-6 ax-productosnues pull-right">';
            } else {
                $comunityp .= '<div class="col-md-6 ax-productosnues">';
            }
    
    $comunityp .= '<a href="'.get_page_link($page->ID).'"><img class="img-responsive center-block" alt="500x500" src="'.$post_thumbnail_url.'"></a>
                </div>
                <div class="col-md-6 ax-productosnues">
                    <div class="item">
                        '.$page->post_content.'
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <a class="boton" target="_blank" href="'.get_page_link($page->ID).'" target="blank">Conoce más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}

?>

<!--<div id="comunity-pages"><?php /*echo $comunityp;*/ ?></div>-->

<?php
    get_footer();
?>