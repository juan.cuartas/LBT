<?php

/*
Template Name: Comunidad Interna TENA
*/

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'año',
        'm' => 'mes',
        'w' => 'semana',
        'd' => 'día',
        'h' => 'hora',
        'i' => 'minuto',
        's' => 'segundo',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? 'Hace ' . implode(', ', $string)  : 'Justo ahora';
}

session_start();
get_header();

$post_id = get_the_ID();
$comments = get_comments(array('post_id' => $post_id, 'status' => 'approve', 'parent' => 0 ));

?>
<div class="ax-comunidad">

    <div class="" style="background-image: url(<?php echo get_post_meta($post_id, 'banner')[0]['guid']; ?>); background-size: cover;
    padding: 6% 0; color: #fff; font-family: 'text', sans-serif; font-weight: 300; background-repeat: no-repeat;">
      <div class="container">
         <div class="row">
            <div class="col-md-9">
             <?php echo get_post()->post_content; ?>
         </div>
         <div class="col-md-3"></div>
     </div>
 </div>

</div>

<div class="ax-comunidad-comentarios">
    <div class="container">
        <form id="form_register" name="form_register" name="registerform" method="post">

            <h3><?php echo get_post_custom_values('pregunta_comentario')[0]; ?></h3>

            <div class="form-group ">
                <div class="col-md-8">
                    <div class="ax-subircomentarios">


                        <textarea id="reg_subircomentario" type="text" name="reg_subircomentario" class="ax-input" parentid="" placeholder="Escribe tu comentario" required></textarea>
                        <input type="hidden" id="page_type" value="<?= $post_id ?>">
                        <span class="btn btn-default btn-file">
                            <i class="fa fa-camera" aria-hidden="true"></i><input type="file" name=" " id="my_image_upload_comment_p" accept="image/x-png, image/gif, image/jpeg" multiple="false"  />
                        </span>
                        <div id="msgparcomment"></div>
                    </div>
                    <div id="ax-load"></div>
                </div>

                <div class="col-md-4">
                    <div class="form-group" style="text-align:center;">
                        <input id="reg-submit" type="button" class="ax-button" value="Enviar">
                    </div>
                </div>

            </div>

        </form>
    </div>
</div>

<div class="ax-comunidad-usercomentarios">
    <div class="container">
        <div id="ax-all-comments" class="col-md-12"></div>
        <div class="col-md-5 hidden ">
            <div class="ax-usuarioinvita">
                <img class="img-responsive center-block" alt="500x500" src="/wp-content/themes/masvivas/img/ax-sobre-comunidad.png">
                <h3>Invita</h3>
                <h2>a tus <span>amigas</span></h2>
                <p>Comparte con otras mujeres como tú, por qué estas Más viva que nunca. Ten presente que entre más amigas invites para que participen, más posiciones subirás en el Top 35+ y así tendrás más oportunidades de ganar una entrada para que disfrutes del evento que hemos preparado para ti.</p>


                <form id='frm_invitado' method='post' novalidate>
                    <label>Nombre</label>
                    <input type="text" id="nombre_invitado" class="ax-input" data-validate="text" name="nombre_invitado" placeholder="Ingresa el nombre">

                    <label>Apellido</label>
                    <input type="text" id="apellido_invitado" class="ax-input" data-validate="text" name="apellido_invitado" placeholder="Ingresa el apellido">

                    <label>Correo</label>
                    <input type="email" id="email_invitado" class="ax-input" data-validate="email" name="email_invitado" placeholder="Ingresa el correo">


                    <div class="ax-botoninv">
                        <div class="col-sm-6">
                            <div class="row">
                                <input type="button" id="agregar-otro"  class="ax-button" name="agregar-otro" value="Agregar otra amiga" disabled class="ax-button">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <input type="submit" id="enviar-invitado"  class="ax-button" name="enviar-invitado" value="Invitar" class="ax-button">
                            </div>
                        </div>
                    </div>
                </form>

                <div id="mensaje_exitoso_invitado" style="display:none;">Has invitado a tus amigas exitosamente</div>

                <div class="ax-comunidadconoce">
                    <a href="/mujeres-con-experiencias/">
                        <div class="col-xs-4">
                            <div class="row">
                                <img class="img-responsive center-block" alt="500x500" src="/wp-content/themes/masvivas/img/ax-comunidaddescubre.jpg">
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <p>Mujeres con experiencias</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-12 ax-comunidadvideo">
                    <div class="row">
                        <div id="player2"></div>
                        <!--<video width="100%" controls autoplay>
                              <source src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2016/08/12152432/mas-vivas-que-nunca.mp4" type="video/mp4">
                              <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210201/alejandraogg.ogg" type="video/ogg">
                              <source src="http://s3.amazonaws.com/familia.famiclic/lbt/uploads/2016/08/07210240/alejandrawebm.webm" type="video/webm">
                              Your browser does not support HTML5 video.
                        </video>-->
                    </div>
                </div>
            </div><!---->
        </div>
    </div>
</div>

<div class="modal fade" id="mdlsolicitar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="row">
            <div class="modal-content inicio">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Iniciar sesión</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="email" class="form-control ax-input" id="reg_email_muestra" placeholder="Ingresa tu correo">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control ax-input" id="reg_numdocumento_muestra" placeholder="Ingresa tu identificación">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <button id="actualizar" type="button" onclick="ax.updateUserData(true)" class="btn btn-default ax-button">Actualiza tus datos</button>
                            </div>
                            <div class="col-md-6">
                                <button id="regcontinuar" type="button" onclick="ax.loginMuestra(true)" class="btn btn-default ax-button">Ingresa</button>
                            </div>
                        </div>
                    </div>
                    <div id="axmsgregister_muestra"></div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
get_footer();
?>
<script type="text/javascript">
    var idcurrentpost = "<?php echo get_the_ID(); ?>";
    console.log(idcurrentpost);
    $(document).ready(function() {
        ax.getComments();
        $("#my_image_upload_comment_p").change(function () {
            if (this.value != '') {
                var size = (this.files[0].size / (1024*1024)).toFixed(2);

                if (size > 2) {
                    $('#msgparcomment').empty();
                    $('#msgparcomment').append('<p>No se admiten imágenes con un peso superior a 2mb</p>');
                    this.value = '';
                    $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                    $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                    $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                    $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
                } else {
                    $("#form_register .ax-subircomentarios i.fa").removeClass('fa-camera');
                    $("#form_register .ax-subircomentarios i.fa").addClass('fa-check');
                    $("#form_register .ax-subircomentarios i.fa").css('color','#fff');
                    $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#ad004b');
                    $("#msgparcomment").empty();
                }
            } else {
                $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
            }
        });
        //$("#reg_subircomentario").emojioneArea();

        <?php
        if (isset($_SESSION['comment']) && isset($_SESSION['inputcomment'])) {
            echo '$("'.$_SESSION['inputcomment'].'").val("'.$_SESSION['comment'].'");';
        }
        ?>
    });

    $(".subcomment").keyup(function(event){
        if(event.keyCode == 13){
            ax.submitSubComment($(this));
        }
    });


    $('#reg-submit').on('click', function(){
        ax.submitComment("#reg_subircomentario");
    });
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1130891536975805',
            xfbml      : true,
            version    : 'v2.7'
        });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

    $(".ax-back-opa").css("display", "block");

    $( window ).on( "load", function() {
        $(".ax-back-opa").css("display", "none");
    });
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/video1.js"></script>