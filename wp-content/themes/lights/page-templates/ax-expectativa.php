<?php

/*
Template Name: Plantilla Home LightsOn
*/
global $wpdb;

if (!isset($_SESSION['idusuario'])){
    wp_redirect( '/iniciar-sesion' );
}else{
    session_start();
}

get_header();
$args = array(
    'posts_per_page'   => 1,
    'offset'           => 0,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'pregunta',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'       => '',
    'author_name'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
);
$posts_array = get_posts($args);
$post = $posts_array[0];
$img = get_post_meta($post->ID,'imagen_destacada')[0];

$episodios = array();
$episodios_array = $wpdb->get_results('SELECT * FROM wp_posts where post_type = "serie_web" AND post_status = "publish" order by post_date desc;');
foreach($episodios_array as $key => $ep):
    $episodios[$ep->ID]['imagen'] = get_post_meta($ep->ID,'imagen_destacada')[0]['guid'];
    $episodios[$ep->ID]['url'] = get_post_meta($ep->ID,'url')[0];
    $episodios[$ep->ID]['actrices'] = get_post_meta($ep->ID,'actrices')[0];
    $episodios[$ep->ID]['title'] = $ep->post_title;
    $episodios[$ep->ID]['description'] = $ep->post_content;
    $episodios[$ep->ID]['button_label'] = get_post_meta($ep->ID,'button_label')[0];
endforeach;
$first = key($episodios);

//echo "<pre>"; print_r($episodios); echo "</pre>"; die();
unset($episodios_array);

?>
<div class="ax-wrapper">

    <!-- Contador -->
    <section class="ax-contador <?php echo strip_tags(get_the_content()); ?>">
        <div class="container ax-main-contador">
            <div class="row">
                <div class="col-md-5 col-md-push-7 col-sm-4 col-sm-push-7 ax-main-contador--title"><h2>Estamos a punto de iniciar algo que te va a encantar</h2></div>
                <div class="col-md-3 col-md-pull-5 col-sm-3 col-sm-pull-5 col-xs-3 ax-main-contador--image">
                    <figure class="ax-contador--image"></figure>
                </div>
                <div class="col-md-4 col-md-pull-5 col-sm-5 col-sm-pull-5 col-xs-9 ax-no-padLeft"><div class="clock"></div></div>
                <span id="ax-cerrar-contador" class="ax-cerrar-contador" onClick="$('.ax-contador').slideUp(500)">X</span>
            </div>
        </div>
    </section>

    <!-- Sección promo -->
    <?php if(!empty($episodios)): ?>
        <section class="promo  video-toggle" style="background: url(<?= $episodios[$first]['imagen'] ?>) no-repeat;">
            <div class="container-custom">
                <div class="row">
                    <div class="col-md-6 col-md-push-6 col-sm-12 col-sm-push-6 right">
                        <a href="javascript:void(0)" onclick="ax.openLastEpisode()">
                            <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/ax-play-icon.png" />
                        </a>
                    </div>
                    <div class="col-md-6 col-md-pull-12 col-sm-12 col-sm-pull-0 left">
                        <h2 class="ax-promo-title"><?= $episodios[$first]['title']; ?></h2>
                        <small>2017 temporada 1</small>
                        <p><?= $episodios[$first]['description']; ?></p>
                        <small><strong>Actrices:</strong><?= $episodios[$first]['actrices']; ?></small>
                        <button class="promo-button" onclick="ax.openLastEpisode()" type="button"><i class="fa fa-fw" aria-hidden="true" title=""></i>VER <?= $episodios[$first]['button_label']; ?></button>
                    </div>

                </div>
            </div>
        </section>
    <?php else: ?>
        <section class="promo  video-toggle">
            <div class="container-custom">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h2 class="ax-promo-title">Próximo 10 de septiembre 8:00 p.m.<br>Mujeres de Tiempo Completo</h2>
                        <small>2017 temporada 1</small>
                        <p>Caro, Aleja y Vero, tres amigas, tres universos diferentes. Junto a ellas viajaremos a ese mundo femenino que vive sin tapujos, sin miedo, sin dolor y con el espíritu que tan sólo la libertad de quien puede tomar sus propias decisiones y estar seguro de ellas puede ofrecernos.</p>
                        <small><strong>Actrices:</strong>Alejandra Azcárate, Carolina Cuervo, Verónica Orozco</small>
                        <a class="promo-button" href="javascript:void(0)" onclick="openCap('trailer');"><i class="fa fa-fw" aria-hidden="true" title=""></i>VER TRAILER</a>
                    </div>
                    <div class="col-md-6 col-sm-6">
                    </div>
                </div>
            </div>
        </section>
    <?php endif;?>
    <!-- end - Sección promo -->

    <!-- sección capitulos -->
    <section class="ax-capitulos conoce">
        <div class="container-custom">
            <?php if(sizeof($episodios) > 1):
            $cantEp = count($episodios);
            $cont = 0; ?>
            <div class="row">
                <div class="col-md-12">
                    <p class="ax-conoce-title expectativa-title">VER Todos los capítulos</p>
                </div>
            </div>
            <?php foreach($episodios as $key => $ep): ?>

                <?php if($cont % 2 == 0): ?><div class="row"><?php endif; ?>
                    <div class="col-md-6 col-xs-6 main-ax-conoce">
                        <div class="ax-conoce-item" data-id="<?= $key ?>" style="background: url(<?= $ep['imagen'] ?>) no-repeat;">
                            <a href="javascript:void(0)" onclick="ax.toggleEpisodios(this,<?= $key ?>)">
                                <h2 class="ax-conoce-item--title expectativa-title--item"><?= $ep['title']; ?></h2>
                                <i class="ax-angle fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <?php $cont ++; if(($cantEp == 1 ) || ($cont > 0 && $cont % 2 == 0) || ($cantEp % 2 != 0 && ($cont) == $cantEp)): ?>
                    <div class="ax-expandDesc video-toggle col-md-12"></div>
                <?php endif; ?>
                <?php if($cont > 0 && $cont % 2 == 0): ?></div><?php endif;?>

            <?php endforeach; endif; ?>
        </div>
    </section>

    <section class="memoriasVideos">
        <div class="container-custom clearfix">
            <p class="ax-conoce-title expectativa-title">Conoce más detalles de Mujeres de Tiempo Completo</p>
            <div class="videos-memorias">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/k7UoOVYalG0?rel=0&amp;enablejsapi=1" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="videos-memorias">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/D4KSHSW83Z4?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="videos-memorias">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/B-YUcOqlnh4?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="videos-memorias">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/IuqLXs_zC0w?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="videos-memorias">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/bGxB_JXUtu0?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <!-- Sección conoce -->
    <section class="conoce">
        <div class="container-custom">
            <div class="row">
                <div class="col-md-12">
                    <p class="ax-conoce-title expectativa-title">Conoce a ¡Aleja, la aventurera! ¡Vero, la mamá! ¡Caro, la profesional! ¿Con cuál de ellas te identificas?</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div id="aleja" class="ax-conoce-item aleja">
                        <a href="javascript:void(0)" onclick="ax.toggleVideo(this)"  >
                            <h2 class="ax-conoce-item--title expectativa-title--item">Alejandra</h2>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div id="caro" class="ax-conoce-item caro">
                        <a href="javascript:void(0)" onclick="ax.toggleVideo(this)">
                            <h2 class="ax-conoce-item--title expectativa-title--item">Carolina</h2>
                            <i class="ax-angle fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div id="vero" class="ax-conoce-item vero">
                        <a href="javascript:void(0)" onclick="ax.toggleVideo(this)">
                            <h2 class="ax-conoce-item--title expectativa-title--item">Verónica</h2>
                            <i class="ax-angle fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="video-toggle ax-toggle-video">
        <a id="ax-close-video" href="javascript:void(0)" onclick="$('.ax-toggle-video').slideUp(500)">X</a>
        <div class="container-custom">
            <div class="row">
                <div class="col-md-7 col-md-push-5 col-sm-5 col-sm-push-7">
                    <a href="javascript:void(0)" onclick="$('#VideoModal').modal('show')">
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/ax-play-icon.png" />
                    </a>
                </div>
                <div class="col-md-5 col-md-pull-7 col-sm-7 col-sm-pull-5">
                    <h2 class="ax-promo-title"></h2>
                    <small>2017 temporada 1</small>
                    <p class="ax-video-text"></p>
                </div>
            </div>
        </div>
    </section>
    <!-- end - Sección conoce -->

    <!-- Sección entrevistas -->
    <section class="entrevistas">
        <div class="container-custom">
            <div class="row">
                <div class="col-md-6">
                    <p class="ax-entrevistas-title expectativa-title">Entérate más sobre Mujeres de tiempo completo</p>
                </div>
                <div class="col-md-6">
                    <p class="ax-entrevistas-title expectativa-title">Haz clic para conocer el making off</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="ax-entrevistas-item">

                        <a href="http://www.lightsbytena.co/la-batalla-de-ser-uno-mismo/">

                            <img src="<?php bloginfo('template_url'); ?>/img/entrevista-caro.jpg" class="img-responsive">
                            <h2 class="ax-conoce-item--title expectativa-title--item">Entrevista a Carolina</h2>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class=" video-lightson video-lightson--r">
                        <iframe style="width: 100%; height: 100%;" src="https://www.youtube.com/embed/meUvKe4gBkE?enablejsapi=1" frameborder="0" allowfullscreen></iframe>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end - Sección entrevistas -->

    <!-- Sección pregunta -->
    <section id="preguntas">
        <div class="container-custom">
            <div class="row">
                <div class="col-md-12 ax-texto-pregunta">
                    <p class="expectativa-title">Este espacio es para ti, comparte tu historia con otras mujeres de tiempo completo. ¡Anímate!</p>
                </div>
            </div>
        </div>
        <div class="container-custom main-pregunta">
            <div class="ax-pregunta">
                <div class="ax-pregunta-cont">
                    <div class="col-lg-8 col-md-7 col-sm-12 img-container">
                       <h2 class="ax-pregunta-title"><?php echo $post->post_title ?></h2>
                       <img src="<?php echo $img['guid']; ?>" class="img-responsive" />
                    </div>

                   <div class="col-lg-4 col-md-5 col-sm-12 ax-comment-box">
                        <div class="ax-header-comments">
                            <p><?php echo $post->post_content; ?></p>
                        </div>

                        <div id="ax-all-comments" class="col-md-12"></div>

                        <div class="ax-comunidad-comentarios">
                            <div class="form-group ">
                                <div class="col-md-12">
                                    <div class="ax-subircomentarios col-md-9">
                                        <textarea id="reg_subircomentario" type="text" name="reg_subircomentario" class="ax-input ax-expectativa" parentid="" placeholder="Escribe tu comentario" required></textarea>
                                        <input type="hidden" id="page_type" value="<?= $post->ID ?>">
                                        <span class="btn btn-default btn-file">
                                            <i class="fa fa-camera" aria-hidden="true"></i><input type="file" name=" " id="my_image_upload_comment_p" accept="image/x-png, image/gif, image/jpeg" multiple="false"  />
                                        </span>
                                    </div>
                                    <div class="col-md-3">
                                        <button id="reg-submit" class="ax-button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                    </div>
                                    <div id="msgparcomment"></div>
                                    <div id="ax-load"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

    <!-- Modal Sesion-->
    <div id="VideoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content inicio sesion-video">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cerrar</span></button>
                <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>
                <div class="modal-body">
                    <video controls controlsList="nodownload" id="videoExpectativa" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" preload="metadata">
                        Your browser doesn't support HTML5 video tag.
                    </video>
                </div>

            </div>
        </div>
    </div>

    <div id="VimeoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content inicio sesion-video">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar"><span aria-hidden="true">Cerrar</span></button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>
                <div class="modal-body body-video">
                </div>
                <div class="modal-footer">
                    <p>¿Te gustó este capítulo? Invita a todas tus amigas a registrarse para que puedan verlo y juntas disfruten de esta serie web.</p>
                    <a id="shareBtn" ><span id="btn_login" class="btn-compartir--f" class="fb-share-button">Compartir en Facebook</span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="ax-descipcion-episodios" style="display:none">
        <a id="ax-close-video" href="javascript:void(0)" onclick="$('.ax-descipcion-episodios').slideUp(500);$('.ax-expandDesc').css({'background':'none'})">X</a>
        <div class="container-custom">
            <div class="row">
                <div class="col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 right">
                    <a href="javascript:void(0)" class="iconPlay" onclick="openCap($(this))">
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/ax-play-icon.png" />
                    </a>
                </div>
                <div class="col-md-5 col-md-pull-12 col-sm-5 col-sm-pull-12 left">
                    <h2 class="ax-promo-title"></h2>
                    <small>2017 temporada 1</small>
                    <p class="ax-video-text"></p>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>

<script type="text/javascript">

    idcurrentpost = "<?php echo get_the_ID(); ?>";

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1130891536975805',
            xfbml      : true,
            version    : 'v2.7'
        });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    document.getElementById('shareBtn').onclick = function() {
        FB.ui({
            method: 'share',
            mobile_iframe: true,
            href: "http://www.lightsbytena.co/iniciar-sesion",
        }, function(response){});
    }
    function openCap(ep){
        if(ep == "trailer"){
            var bodyModal = $("#VimeoModal .body-video");
            bodyModal.empty();
            bodyModal.append('<iframe src="https://www.ustream.tv/embed/recorded/107706724?html5ui&autoplay=true&showtitle=false" style="border: 0 none transparent;"  webkitallowfullscreen allowfullscreen frameborder="no" width="560" height="315"></iframe>');
            $("#VimeoModal").modal("show");
        }else{
            var bodyModal = $("#VimeoModal .body-video");
            bodyModal.empty();
            bodyModal.append(ax.episodios[ep.data('id')].url);
            $("#VimeoModal").modal("show");
        }
    }

    $("#cerrar").click(function() {
        $('html,body').animate({
            scrollTop: $("#preguntas").offset().top
        }, 'slow');
    });

    $(document).ready(function() {


        ax.getCommentsExpectativa();


        ax.episodios = <?= json_encode($episodios); ?>;
        ax.first = <?= (!empty($first) ? $first  : "''"); ?>;

        var videoFrame = $('.video-lightson--r');
        videoFrame.css({'height':videoFrame.parent().prev().height()});
        $("iframe[src*='youtube']").each(function () {
            $(this).attr('src', $(this).attr('src')+'?version=3&autohide=0&autoplay=0');
        });

        $('#VideoModal').on('shown.bs.modal',function(){
            document.getElementById('videoExpectativa').play();
        });

        $('#VideoModal').on('hide.bs.modal',function(){
            document.getElementById('videoExpectativa').pause();
        });

        $('#VimeoModal').on('hide.bs.modal',function(){
            $("#VimeoModal .modal-body").empty();
        });

        $("#my_image_upload_comment_p").change(function () {
            if (this.value != '') {
                var size = (this.files[0].size / (1024*1024)).toFixed(2);

                if (size > 2) {
                    $('#msgparcomment').empty();
                    $('#msgparcomment').append('<p>No se admiten imágenes con un peso superior a 2mb</p>');
                    this.value = '';
                    $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                    $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                    $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                    $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
                } else {
                    $("#form_register .ax-subircomentarios i.fa").removeClass('fa-camera');
                    $("#form_register .ax-subircomentarios i.fa").addClass('fa-check');
                    $("#form_register .ax-subircomentarios i.fa").css('color','#fff');
                    $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#ad004b');
                    $("#msgparcomment").empty();
                }
            } else {
                $("#form_register .ax-subircomentarios i.fa").removeClass('fa-check');
                $("#form_register .ax-subircomentarios i.fa").addClass('fa-camera');
                $("#form_register .ax-subircomentarios i.fa").css('color','#b2b2b2');
                $("#form_register .ax-subircomentarios span.btn-file").css('background-color','#fff');
            }
        });
        //$("#reg_subircomentario").emojioneArea();

        <?php
        if (isset($_SESSION['comment']) && isset($_SESSION['inputcomment'])) {
            echo '$("'.$_SESSION['inputcomment'].'").val("'.$_SESSION['comment'].'");';
        }
        ?>
    });

    $(".subcomment").keyup(function(event){
        if(event.keyCode == 13){
            ax.submitSubComment($(this));
        }
    });

    $('#reg-submit').on('click', function(){
        ax.submitComment("#reg_subircomentario");
    });

    /*iframe youtube*/
    var script = document.createElement('script');
    script.src = '//www.youtube.com/iframe_api';
    script.type = 'text/javascript';
    var head = document.getElementsByTagName('head')[0];
    head.appendChild( script );

    function onYouTubeIframeAPIReady() {
        var iframes = document.getElementsByTagName('iframe');

        for( var i in iframes )
        {
            if( iframes.hasOwnProperty( i ) )
            {
                if( iframes[i].src.indexOf( '?enablejsapi=1' ) != -1 )
                {
                    new YT.Player(iframes[i],
                    {
                        events:
                        {
                            'onStateChange': onPlayerStateChange
                        }
                    });
                }
            }
        }
    }

    function onPlayerStateChange( e ) {
        if( e.data == YT.PlayerState.PLAYING && typeof e.target.onetimeplayed === 'undefined'  ) {
            alert( "PLAYING: " + e.target.getIframe().src );
            e.target.onetimeplayed = 1;
        }
        else if( e.data == -1 ) {
            alert( "CLICKED: " + e.target.getIframe().src );
        }
    }
</script>
