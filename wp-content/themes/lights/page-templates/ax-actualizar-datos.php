<?php

/*
Template Name: Actualización de datos TENA
*/
//die(print_r($_SESSION));
session_start();

if (empty($_SESSION['idusuario'])) {
    wp_redirect(home_url());
}

get_header();


?>

<div class="ax-registro ax-steps ax-steps-actualizar">
    <div class="step-header">
        <div class="container">
            <!-- steps -->
            <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-md-4 step-1-a">
                    <a href="#step-1" class="btn btn-primary">1.</a>
                    <h2>Datos personales</h2>
                </div>
                <div class="stepwizard-step col-md-4 step-2-v">
                    <a href="#step-2" class="btn btn-default">2.</a>
                    <h2>Datos del producto</h2>
                </div>
                <div class="stepwizard-step col-md-4 step-3-c">
                    <a href="#step-3" class="btn btn-default">3.</a>
                    <h2>Temas de interés</h2>
                </div>
            </div>
            </div>
        </div> <!-- end - container -->
    </div>

    <div class="form">
        <form id="form_register" name="form_register" >
            <div>
                <p id="axmessage" class="ax-message"></p>

                <!-- <h3>*Ingresa tus datos</h3> -->
                <section class="setup-content" id="step-1"> <!-- step 1 -->
                    <div class="container">
                        <div class="row">
                            <p id="axmessage_step1" class="ax-message"></p>
                        </div>

                        <div class="row ax-formulario">
                            <?php the_content(); ?>
                            <!-- col-left -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_nombre" type="text" name="upd_nombre" class="ax-input" placeholder="Nombre" required autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <span class="ax-icoreg">*</span> -->
                                    <input id="upd_segnombre" type="text" name="upd_segnombre"  placeholder="Segundo nombre"  class="ax-input" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_apellido" type="text" name="upd_apellido" class="ax-input" placeholder="Apellido"  required autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg"></span>
                                    <input id="upd_segapellido" type="text" name="upd_segapellido" class="ax-input" placeholder="Segundo apellido" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="upd_tipo_documento" id="upd_tipo_documento" class="ax-select" required>
                                        <option value="" selected="selected">Tipo de documento</option>
                                        <option value="C">Cédula de ciudadanía</option>
                                        <option value="E">Cédula de extranjería</option>
                                        <option value="P">Pasaporte</option>
                                        <option value="T">Tarjeta de identidad</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_num_documento" type="text" name="upd_num_documento" class="ax-input" placeholder="Número de documento" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_email" type="email" name="upd_email" class="ax-input" placeholder="Correo electrónico"  required autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_celular" type="text" name="upd_celular" placeholder="Celular"  class="ax-input" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="upd_sexo" id="upd_sexo" class="ax-select" required>
                                        <option value="0" selected="selected">Género</option>
                                        <option value="1">Masculino</option>
                                        <option value="2">Femenino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group datepick">
                                    <span class="ax-icoreg">*</span>
                                    <p>Cuéntanos tu fecha de nacimiento</p>
                                    <div class="clearfix">

                                        <input class="ax-fecha" placeholder="dd/mm/yyyy" type="text" id="upd_fecha" name="upd_fecha" autocomplete="off">
                                        <span class="help-block">Dia/Mes/Año</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="upd_pais" id="upd_pais" class="ax-select" required>
                                        <option value="1" selected="selected">Colombia</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="upd_departamento" id="upd_departamento" class="ax-select" required>
                                        <option value="0" selected="selected">Selecciona tu departamento</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="upd_ciudad" id="upd_ciudad" class="ax-select" required>
                                        <option value="0" selected="selected">Selecciona tu ciudad</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_barrio" type="text" name="upd_barrio" placeholder="Barrio"  class="ax-input" autocomplete="off">
                                </div>
                            </div>
                            <!-- end - col-left -->

                            <!-- col-right -->

                            <!-- end - col-right -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="upd_direccion" type="text" class="ax-input" name="upd_direccion" placeholder="Dirección" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary nextBtn btn-lg center-block"  type="button">Siguiente</button>
                    </div> <!-- end - container -->
                </section> <!-- end - step 1 -->

                <section class="setup-content" id="step-2"><!-- step 2 -->
                    <div class="container">
                        <div class="col-md-9">
                            <p class="title-section">Cuéntanos si eres usuaria de la comunidad Lights y en caso de que aún no tengas alguna relación con nuestra marca te invitamos a definir en qué producto estás interesada.</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label class="custom-control">
                                                <input id="user_radio1" type="radio" name="upd_user_radio" class="custom-control-input form-control" required="" value="S">
                                                <span class="custom-control-indicator"></span>
                                                <h4>Soy usuaria Lights by TENA</h4>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label class="custom-control">
                                                <input id="user_radio2" type="radio" name="upd_user_radio" class="custom-control-input form-control" value="N" required="">
                                                <span class="custom-control-indicator"></span>
                                                <h4>Aún no soy usuaria</h4>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p id="ax-text-user"><span class="ax-icoreg">*</span> Elige el producto con el que más te identificas.</p>
                                </div>
                                <div class="col-md-12">
                                    <div id="prod-group" class="ax-check-group"></div>
                                    <div class="mensaje"></div>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <button class="btn btn-primary prevBtn btn-lg" type="button">Anterior</button>
                                <button class="btn btn-primary nextBtn btn-lg" type="button">Siguiente</button>
                            </div>
                        </div>
                    </div> <!-- end - container -->
                </section><!-- end - step 2 -->

                <section class="setup-content ax-interes" id="step-3"><!-- step 3 -->
                    <div class="container">
                        <div class="row">
                            <p id="axmessage" class="ax-message"></p>
                        </div>
                        <div class="row content">
                            <div class="col-md-10">
                                <p>Para recibir información de tu interés, por favor selecciona al menos una de las siguientes actividades.</p>
                                <div id="int_group" class="ax-check-group"></div>
                                <div class="ax-terminoscond clearfix">
                                    <div class="form-group">
                                        <div class="col-md-6" id="terminos">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <span class="ax-icoreg">*</span>
                                                    <input id="upd_terms" type="checkbox" name="upd_terms" class="ax-checkbox custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <a href="/terminos-y-condiciones" target="_blank"><p>Acepto los términos y condiciones y el tratamiento de la información.</p></a>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6" id="politica">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <span class="ax-icoreg">*</span>
                                                    <input id="upd_policy" type="checkbox" name="upd_policy" class="ax-checkbox custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <a href="/politicas" target="_blank"><p>Acepto la política de protección de información personal.</p></a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <input id="upd_expectativa" type="hidden" value="S" name="upd_expectativa">
                                </div>
                                <div class="ax-mediosmail clearfix">
                                    <h3>Deseas que te contactemos por alguno de estos medios</h3>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <input id="upd_med_fis" type="checkbox" name="upd_med_fis" class="ax-checkbox custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <p>Deseas que te contactemos por medios físicos con los datos que acabas de suministrar.</p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <input id="upd_med_vir" type="checkbox" name="upd_med_vir" class="ax-checkbox custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <p>Deseas que te contactemos por medios virtuales con los datos que acabas de suministrar.</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="text-align:center;">
                            <div id="ax-preload"></div>
                            <button class="btn btn-primary prevBtn btn-lg" type="button">Anterior</button>
                            <input id="reg-submit" type="button" onclick="ax.updateUserInfo()" class="ax-button" value="Actualizar">
                        </div>
                    </div> <!-- end - container -->
                </section><!-- end - step 3 -->
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="mdlupdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="msgupdate" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <a class="btn ax-button" href="/lightson" >Aceptar</a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

<script type="text/javascript">
    var templateDir = "<?php bloginfo('template_directory'); ?>";

    $.validator.addMethod("fechanac",
        function(value, element) {
            return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19)?\d{2}$/);
            //return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19|20)?\d{2}$/);
        },
        "Formato incorrecto!"
    );

    $.validator.addMethod("numbers",
        function(value, element) {
            return value.match(/^[0-9]+$/);
            //return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19|20)?\d{2}$/);
        },
        "Formato incorrecto!"
    );

    formulario = $('#form_register');

    // ---- inicio pasos actualizar datos
    var navListItems = $('div.setup-panel div a'),
    allWells = $('.setup-content'),
    allNextBtn = $('.nextBtn'),
    allPrevBtn = $('.prevBtn');
    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
        $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            navListItems.parent().removeClass('ax-active-steps');
            $item.addClass('btn-primary');
            $item.parent().addClass("ax-active-steps");
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        //curInputs = curStep.find("input[type='text'],input[type='url']"),
        isValid = true;

        formulario.validate({

            rules:{
                upd_nombre:{required:true},
                upd_apellido:{required:true},
                upd_tipo_documento:{required:true},
                upd_num_documento:{required:true, numbers:true, minlength:6, maxlength:12},
                upd_email:{required:true, email: true},
                upd_celular:{required:true, numbers:true, minlength:10, maxlength:10},
                upd_sexo:{required:true},
                upd_fecha:{required:true, fechanac:true},
                upd_pais:{required:true},
                upd_departamento:{required:true},
                upd_ciudad:{required:true},
                upd_direccion:{required:true},

            },messages: {
                upd_nombre:{ required:"Ingresa tu nombre"},
                upd_apellido:{required:"Ingresa tu apellido"},
                upd_tipo_documento:{required:"Selecciona tu tipo de documento"},
                upd_num_documento:{required:"Ingresa tu n&uacute;mero de documento"},
                upd_email:{required:"Ingresa tu correo electr&oacute;nico"},
                upd_celular:{required:"Ingresa tu n&uacute;mero celular"},
                upd_sexo:{required:"Selecciona tu genero"},
                upd_fecha:{required:"Tu fecha de nacimiento"},
                upd_pais:{required:"Selecciona tu pais"},
                upd_departamento:{required:"Selecciona tu departamento"},
                upd_ciudad:{required:"Selecciona tu ciudad"},
                upd_direccion:{required:"Ingresa tu direcci&oacute;n"},
            }

        });

        //$(".form-group").removeClass("has-error");
        /*for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }*/
        //validar paso 2
        if (curStepBtn === 'step-2') {
            cantidad = 0;
            $(curStep).find('#prod-group .ax-checkbox.ax-productos').each(function() {
                if ($(this).is(':checked') ){
                    cantidad++;
                }
            });
            if (cantidad < 1) {
                $(".mensaje").html("");
                $(".mensaje").html('<br><span class="ax-registro-user">Por favor selecciona al menos 1 producto</span>');
                isValid = false;
            }else{
                $(".mensaje").html("");
            }
        }
        if (formulario.valid() == true && isValid == true){
            nextStepWizard.removeAttr('disabled').trigger('click');
        }
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    //---fin pasos para actualizar datos

    ax.getInterest();
    ax.getProducts();
    cargarDepartamentos();

    // funcionamiento
    $(document).ready(function() {

        $(".ax-fecha").mask('00/00/0000');

        $("#user_radio1").click(function(){
            $("#ax-text-user").html("");
            $("#ax-text-user").append('<span class="ax-icoreg">*</span> Elige el producto con el que más te identificas.');
        });

        $("#user_radio2").click(function(){
            $("#ax-text-user").html("");
            $("#ax-text-user").append('<span class="ax-icoreg">*</span> Elige los productos que te gustaría comprar.');
        });

        <?php
        if ($_SESSION['medioscontacto']) {
            echo "$('#axmessage').empty();";
            echo "$('#axmessage').append(".'"'."<div class='ax-registro-user'>Por favor actualiza el medio por el cual deseas que te contactemos.</br>".'"'.");";
        }
        ?>

    });

    function cargarDepartamentos() {
        $.get(templateDir+'/inc/service/service.php?tipo=departamentos', function(data){

            var departamentos = $.parseJSON(data);
            var elementos = "<option selected='selected' value=''>Selecciona tu departamento</option>";
            for (var i = 0; i < departamentos.length; i++) {
                elementos += '<option value="'+departamentos[i]["id"]+'">'+departamentos[i]['nombre']+'</option>';
            }
            $('#upd_departamento').empty();
            $('#upd_departamento').append(elementos);
            obtenerInformacionUsuario();
        });
    }

    function obtenerInformacionUsuario() {
        var udata = {};


        <?php
        if (!empty($_SESSION['idusuario']) && !empty($_SESSION['userdata']['email'])) {
            echo 'udata.idusuario = "'.$_SESSION['idusuario'].'";';
            echo 'udata.correo = "'.$_SESSION['userdata']['email'].'";';
            echo 'udata.numdocumento = "'.$_SESSION['userdata']['identificacion'].'";';
        }else{
            echo 'udata.idusuario = "'.$_SESSION['idusuario'].'";';
            echo 'udata.correo = "'.$_SESSION['userdata']['EMAIL'].'";';
            echo 'udata.numdocumento = "'.$_SESSION['userdata']['IDENTIFICACION'].'";';
        }
        ?>

        if (udata.idusuario != '') {
            ax.getUserInfo(udata);
        }
    }

    // Eventos
    $('#upd_departamento').on('change', function(){

        valor = $(this).val();

        if (valor !== '') {
            $.get(templateDir+'/inc/service/service.php?departamento='+valor, function(data){

                var ciudades = $.parseJSON(data);
                var elementos = "<option selected='selected' value=''>Selecciona tu ciudad</option>";
                for (var i = 0; i < ciudades.length; i++) {
                    elementos += '<option value="'+ciudades[i]["id"]+'">'+ciudades[i]['nombre']+'</option>';
                }
                $('#upd_ciudad').empty();
                $('#upd_ciudad').append(elementos);
            });
        }

    });

    $('#upd_med_fis').click( function () {
        if($(this).prop('checked') == false && $('#upd_med_vir').prop('checked') == false)
            $(this).prop('checked', true);
    });

    $('#upd_med_vir').click( function () {
        if($(this).prop('checked') == false && $('#upd_med_fis').prop('checked') == false)
            $(this).prop('checked', true);
    });

</script>