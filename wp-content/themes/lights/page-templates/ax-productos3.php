<?php

/*
Template Name: Jabón íntimo LIGHTS TENA
*/

get_header();

?>

<div class="ax-productos">
 	<div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
		<h2><?php the_title(); ?></h2>
		<div class="row">
	      	<div class="col-md-8">
	            <?php the_content(); ?>
	        </div>

        	<div class="col-md-4">
        		<img class="img-responsive" alt="" src="/wp-content/themes/lights/img/ax-producto3.png">
        	</div>
	 	</div>
	 	<?php endwhile;?>
    <?php endif; ?>
	</div>
</div>
<?php
    get_footer();
?>

