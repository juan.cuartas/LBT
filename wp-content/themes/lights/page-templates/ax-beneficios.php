<?php

/*
Template Name: Beneficios TENA
*/

get_header();

?>

<div class="ax-beneficios">
 	<div class="container">
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
		<div class="row">
            <h2><?php the_title(); ?></h2>
	      	<div class="col-md-7">

	            <?php the_content(); ?>
	        </div>

        	<div class="col-md-5"></div>
	 	</div>
	 	<?php endwhile;?>
    <?php endif; ?>
	</div>
</div>

<div class="ax-beneficios2">
    <div class="container">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                  <p>Ahora hacen parte de la línea de cuidado íntimo femenino </p>
                  <img src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2017/01/24152425/axbeneficioslogo.png" alt="Lights" class="img-responsive" />
                  <p>la nueva marca para mujeres de 35+ que ofrece una solución integral para el cuidado y la higiene de la zona íntima, con </p>
                  <h4>Protectores Diarios, Jabón íntimo y Toallas Húmedas íntimas </h4>
                  <p>que permiten que la rutina de higiene sea completa y acorde con las necesidades de la mujer de esta edad. </p>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<div class="ax-beneficios4">

        <div class="col-md-12">
             <div class="">
                <img src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2017/01/24153854/axbeneficiosprodct.png" alt="Lights" class="img-responsive ax-benfprod" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h4>Línea de producto especializada en la higiene íntima de las mujeres de</h4><br>
                <img src="http://s3.amazonaws.com/arkix.bucket/lbt/uploads/2017/01/24162344/axbeneficios35.png" alt="Lights" class="img-responsive " />
            </div>
            <div class="col-md-3"></div>
        </div>

</div>

<div class="ax-beneficios3">
        <div class="col-md-6">
            <div class="ax-evolucion">
                <p class="axp1">Con esta </p>
                <p class="axp2">evolución</p>
                <p class="axp3">ahora haces parte de la comunidad excluiva para mujeres  </p>
                <p class="axp4">Más Vivas</p>
                <button id="btnSuscripcion" class="ax-button btn btn-lg btn-block btn-default" type="button">ingresa a la comunidad</button>
            </div>
        </div>
        <div class="col-md-6">
           <div class="ax-evolucion2">
                <a href="/nuestros-productos/protectores-diarios/">
                    <button id="btnSuscripcion" class="ax-button btn btn-lg btn-block btn-default" type="button">Conoce más aquí</button>
                </a>
                <a href="/solicitar-muestra/">
                <button id="btnSuscripcion" class="ax-button btn btn-lg btn-block btn-default" type="button">Solicita tu muestra gratis</button>
                </a>
            </div>
        </div>

</div>


<?php
    get_footer();
?>
<script type="text/javascript">
    $(".ax-back-opa").css("display", "block");

    $( window ).on( "load", function() {
        $(".ax-back-opa").css("display", "none");
    });
</script>
