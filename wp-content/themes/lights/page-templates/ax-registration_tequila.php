<?php

/*
Template Name: Formulario registro TENA
*/
session_start();
get_header();

?>

<div class="ax-registro ax-steps">

    <div class="form">
        <form id="form_register" class="steps-formulario" name="form_register">
            <div>
                <section class="setup-content" id="step-1">
                    <div class="back-posit"></div>
                    <div class="container">

                        <div class="row ax-formulario">
                            <!-- col-left -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="reg_nombre" type="text" name="reg_nombre" class="ax-input" placeholder="Nombre" required autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="reg_apellido" type="text" name="reg_apellido" class="ax-input" placeholder="Apellido"  required autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="reg_tipo_documento" id="reg_tipo_documento" class="ax-select" required>
                                        <option value="" selected="selected">Tipo de documento</option>
                                        <option value="C">Cédula de ciudadanía</option>
                                        <option value="E">Cédula de extranjería</option>
                                        <option value="P">Pasaporte</option>
                                        <option value="T">Tarjeta de identidad</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <?php if (isset($_GET['pss'])) {
                                        echo '<input id="reg_num_documento" type="text" name="reg_num_documento" class="ax-input" placeholder="Número de documento" required autocomplete="off">';
                                    } else {
                                        echo '<input id="reg_num_documento" type="text" name="reg_num_documento" class="ax-input" placeholder="Número de documento" value="'.$_SESSION['numdocreg'].'" required autocomplete="off">';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <?php if (isset($_GET['pss'])) {
                                        echo '<input id="reg_correo" type="email" class="ax-input" placeholder="Correo electrónico" required autocomplete="off">';
                                    } else {
                                        echo '<input id="reg_correo" type="email" class="ax-input" placeholder="Correo electrónico" value="'.$_SESSION['correoreg'].'" required autocomplete="off">';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="reg_celular" type="text" name="reg_celular" placeholder="Celular"  class="ax-input" required autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="reg_sexo" id="reg_sexo" class="ax-select" required>
                                        <option value="" selected="selected">Género</option>
                                        <option value="2">Femenino</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group datepick">
                                    <span class="ax-icoreg">*</span>
                                    <p>Cuéntanos tu fecha de nacimiento</p>
                                    <div class="clearfix">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <input class="ax-fecha" placeholder="dd/mm/yyyy" type="text" id="reg_fecha" name="reg_fecha" required autocomplete="off">
                                        <span class="help-block">Dia/Mes/Año</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="reg_pais" id="reg_pais"  class="ax-select" required>
                                        <option value="1">Colombia</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="reg_departamento" id="reg_departamento" class="ax-select" required>
                                        <option value="" selected="selected">Selecciona tu departamento</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <select name="reg_ciudad" id="reg_ciudad" class="ax-select" required>
                                        <option value="" selected="selected">Selecciona tu ciudad</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="ax-icoreg">*</span>
                                    <input id="reg_direccion" type="text" class="ax-input" name="reg_direccion" placeholder="Dirección"  required autocomplete="off">
                                </div>
                            </div>

                        </div>
                        <div class="row ax-intereses">
                            <div class="col-md-12 content">
                                <div class="ax-terminoscond clearfix">

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <span class="ax-icoreg">*</span>
                                                    <input id="reg_terms" type="checkbox" name="reg_terms" class="ax-checkbox custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <p><a href="/terminos-y-condiciones/" target="_blank">Acepto los términos y condiciones y el tratamiento de la información.</a></p>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <span class="ax-icoreg">*</span>
                                                    <input id="reg_policy" type="checkbox" name="reg_policy" class="ax-checkbox custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <p><a href="/politicas/" target="_blank">Acepto la política de protección de información personal.</a></p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <input id="reg_expectativa" type="hidden" value="N" name="reg_expectativa">
                                    <div id="ax-preload"></div>

                                </div>
                                <div class="ax-mediosmail clearfix">

                                    <h3>Deseas que te contactemos por alguno de estos medios</h3>
                                    <div class="form-group ">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <input id="reg_med_fis" type="checkbox" name="reg_med_fis" class="ax-checkbox custom-control-input medios" >
                                                    <span class="custom-control-indicator"></span>
                                                    <p>Deseas que te contactemos por medios físicos con los datos que acabas de suministrar.</p>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <input id="reg_med_vir" type="checkbox" name="reg_med_vir" class="ax-checkbox custom-control-input medios" >
                                                    <span class="custom-control-indicator"></span>
                                                    <p>Deseas que te contactemos por medios virtuales con los datos que acabas de suministrar.</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="text-align:center;">
                            <!--<button id="reg-submit" type="button"  class="ax-button" onclick="ax.validateStep1(true);" value="Regístrate">Regístrate</button>-->
                            <button class="ax-button" type="submit">Regístrate</button>
                        </div>
                    </div> <!-- end - container -->
                </section> <!-- end - step 1 -->
            </div>
        </form>
    </div>
</div>


<div class="modal fade" id="mdlregistro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="msgregister" class="text-center"></p>
            </div>
            <div class="modal-footer">
                <div id="redirectregistro"></div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
?>
<script type="text/javascript">

    $.validator.addMethod("fechanac",
        function(value, element) {
            return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19)?\d{2}$/);
            //return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19|20)?\d{2}$/);
        },
        "Formato incorrecto!"
    );

    $.validator.addMethod("numbers",
        function(value, element) {
            return value.match(/^[0-9]+$/);
            //return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19|20)?\d{2}$/);
        },
        "Formato incorrecto!"
    );


    $('#form_register').validate({

        rules:{
            reg_nombre:{required:true},
            reg_apellido:{required:true},
            reg_tipo_documento:{required:true},
            reg_num_documento:{required:true, numbers:true, minlength:6, maxlength:12},
            reg_correo:{required:true, email: true},
            reg_celular:{required:true, numbers:true, minlength:10, maxlength:10},
            reg_sexo:{required:true},
            reg_fecha:{required:true, fechanac:true},
            reg_pais:{required:true},
            reg_departamento:{required:true},
            reg_ciudad:{required:true},
            reg_direccion:{required:true},
            reg_terms:{required:true},
            reg_policy:{required:true},
            reg_med_fis:{
                require_from_group:[1, '.medios']
            },
            reg_med_vir:{
                require_from_group:[1, '.medios']
            }
        },messages: {
            reg_nombre:{ required:"Ingresa tu nombre"},
            reg_apellido:{required:"Ingresa tu apellido"},
            reg_tipo_documento:{required:"Selecciona tu tipo de documento"},
            reg_num_documento:{required:"Ingresa tu n&uacute;mero de documento"},
            reg_correo:{required:"Ingresa tu correo electr&oacute;nico"},
            reg_celular:{required:"Ingresa tu n&uacute;mero celular"},
            reg_fecha:{required:"Tu fecha de nacimiento"},
            reg_direccion:{required:"Ingresa tu direcci&oacute;n"},
            reg_pais:{required:"Selecciona tu pais"},
            reg_departamento:{required:"Selecciona tu departamento"},
            reg_ciudad:{required:"Selecciona tu ciudad"},
            reg_sexo:{required:"Selecciona tu genero"},
            reg_med_fis:{
                require_from_group:"Por favor selecciona una de la opciones."
            },
            reg_med_vir:{
                require_from_group:"Por favor selecciona una de la opciones."
            }

        },submitHandler: function(form) {
            //ax.validateStep1(true);
            ax.register();
        }

    });

    /*

     */

    cargarDepartamentos();
    $(".ax-fecha").mask('00/00/0000');

    function cargarDepartamentos(){
        $.get(templateDir+'/inc/service/service.php?tipo=departamentos', function(data){
            var departamentos = $.parseJSON(data);
            var elementos = "<option selected='selected' value=''>Selecciona tu departamento</option>";
            for (var i = 0; i < departamentos.length; i++) {
                elementos += '<option value="'+departamentos[i]["id"]+'">'+departamentos[i]['nombre']+'</option>';
            }
            $('#reg_departamento').empty();
            $('#reg_departamento').append(elementos);
        });
    }

    $('#reg_departamento').on('change', function(){
        valor = $(this).val();
        if (valor !== '') {
            $.get(templateDir+'/inc/service/service.php?departamento='+valor, function(data){
                var ciudades = $.parseJSON(data);
                var elementos = "<option selected='selected' value=''>Selecciona tu ciudad</option>";
                for (var i = 0; i < ciudades.length; i++) {
                    elementos += '<option value="'+ciudades[i]["id"]+'">'+ciudades[i]['nombre']+'</option>';
                }
                $('#reg_ciudad').empty();
                $('#reg_ciudad').append(elementos);
            });
        }
    });

    /*$('#reg_med_fis').click( function () {
        if( $(this).prop('checked') == true && $('#reg_med_vir').prop('checked') == true)
            $(this).prop('checked', false);
    });

    $('#reg_med_vir').click( function () {
        if($(this).prop('checked') == true && $('#reg_med_fis').prop('checked') == true)
            $(this).prop('checked', false);
    });*/
</script>